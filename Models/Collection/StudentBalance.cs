using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class StudentBalance
    {
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }
        [BsonElement("student")]
        public Reference Student { get; set; }

        [BsonElement("group")]
        public GroupReference Group { get; set; }

        [BsonElement("plan")]
        public StudyPlan Plan { get; set; }

        [BsonElement("dueAmount")]
        public double DueAmount { get; set; }

        [BsonElement("currentAmount")]
        public double CurrentAmount { get; set; }
    }

}