using System;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System.Collections.Generic;

namespace skooltrak_api.Models
{
	[BsonIgnoreExtraElements]	
	public class Charge
	{
		public Charge()
		{
			CreateDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("student")]
		public Reference Student { get; set; }

		[BsonElement("description")]
		public string Description { get; set; }

		[BsonElement("starDate")]
		public DateTime StartDate { get; set; }

		[BsonElement("monthlyFee")]
		public Reference MonthlyFee { get; set; }

		[BsonElement("dueDate")]
		public DateTime DueDate { get; set; }

		[BsonElement("amount")]
		public double Amount { get; set; }

		[BsonElement("balance")]
		public double Balance { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("creditNote")]
		public bool CreditNote { get; set; }
		public string Status
		{
			get
			{
				return Balance == 0 ? "Cancelled" :
				(DueDate < DateTime.Now ? "Due" :
				(StartDate < DateTime.Now ? "Active" : "Pending"));
			}
		}

		[BsonElement("paymentDate")]
		public DateTime PaymentDate { get; set; }
	}
}