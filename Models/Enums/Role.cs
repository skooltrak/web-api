namespace skooltrak_api.Models
{
	public enum RoleEnum
	{
		Administrator = 1,
		Teacher = 2,
		Student = 3,
		Parent = 4
	}
}