namespace skooltrak_api.Models
{
	public enum AttendanceEnum
	{
		Present = 1,
		Absent,
		Late
	}
}