namespace skooltrak_api.Models
{
	public enum ActivityEnum
	{
		Announcement,
		Assignment,
		Grade,
		Quiz,
		Content,
		Document
	}
}