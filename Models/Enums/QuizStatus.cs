namespace skooltrak_api.Models
{
	public enum QuizStatusEnum
	{
		Pending,
		Incomplete,
		Completed,
		Revisited
	}
}