using skooltrak_api.Shared;

namespace skooltrak_api.Models
{

	public class AdminAccess
	: Enumeration
	{
		public static AdminAccess Collection = new AdminAccess((int)AdminAccessEnum.Collection, nameof(Collection));
		public static AdminAccess Students = new AdminAccess((int)AdminAccessEnum.Students, nameof(Students));
		public static AdminAccess Surveys = new AdminAccess((int)AdminAccessEnum.Surveys, nameof(Surveys));
		public static AdminAccess Grades = new AdminAccess((int)AdminAccessEnum.Grades, nameof(Grades));
		public static AdminAccess Courses = new AdminAccess((int)AdminAccessEnum.Courses, nameof(Courses));
		public static AdminAccess Teachers = new AdminAccess((int)AdminAccessEnum.Teachers, nameof(Teachers));
		public static AdminAccess Incidents = new AdminAccess((int)AdminAccessEnum.Incidents, nameof(Incidents));
		public static AdminAccess Exams = new AdminAccess((int)AdminAccessEnum.Exams, nameof(Exams));
		public static AdminAccess Security = new AdminAccess((int)AdminAccessEnum.Security, nameof(Security));
		public static AdminAccess Settings = new AdminAccess((int)AdminAccessEnum.Settings, nameof(Settings));
		
		public AdminAccess(int id, string name, string description = "Descripción")
			: base(id, name)
		{
		}
	}
	public enum AdminAccessEnum
	{
		Collection = 1,
		Students = 2,
		Surveys = 3,
		Grades = 4,
		Courses = 5,
		Teachers = 6,
		Incidents = 7,
		Exams = 8,
		Security = 9,
		Settings = 10
	}
}