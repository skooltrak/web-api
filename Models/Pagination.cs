namespace skooltrak_api.Models
{
	public class Pagination
	{
		public string LastId { get; set; }
		public int PageSize { get; set; }
	}
}