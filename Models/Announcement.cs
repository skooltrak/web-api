using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class Announcement
	{
		public Announcement()
		{
			CreatedDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("title")]
		public string Title { get; set; }

		[BsonElement("text")]
		public string Text { get; set; }

		[BsonElement("author")]
		public UserInfo Author { get; set; }

		[BsonElement("createdDate")]
		public DateTime CreatedDate { get; set; }

		[BsonElement("activeSince")]
		public DateTime ActiveSince { get; set; }

		[BsonElement("activeUntil")]
		public DateTime ActiveUntil { get; set; }

	}
}