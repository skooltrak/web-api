using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class Incident
	{
		public Incident()
		{
			CreatedAt = DateTime.Now;
			UpdatedAt = DateTime.Now;
			Checks = new List<IncidentCheck>();
			Updates = new List<IncidentUpdate>();
			Documents = new List<FileInfo>();
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("title")]
		public string Title { get; set; }

		[BsonElement("details")]
		public string Details { get; set; }

		[BsonElement("student")]
		public Student Student { get; set; }

		[BsonElement("course")]
		public CourseReference Course { get; set; }

		[BsonElement("incidentDate")]
		public DateTime IncidentDate { get; set; }

		[BsonElement("documents")]
		public List<FileInfo> Documents { get; set; }

		[BsonElement("checks")]
		public List<IncidentCheck> Checks { get; set; }

		[BsonElement("updates")]
		public List<IncidentUpdate> Updates { get; set; }

		[BsonElement("createdBy")]
		public UserInfo CreatedBy { get; set; }

		[BsonElement("createdAt")]
		public DateTime CreatedAt { get; set; }

		[BsonElement("updatedAt")]
		public DateTime UpdatedAt { get; set; }
	}

	public class IncidentCheck
	{
		[BsonElement("user")]
		public UserInfo User { get; set; }

		[BsonElement("checkedAt")]
		public DateTime CheckedAt { get; set; }
	}

	public class IncidentUpdate
	{
		[BsonElement("user")]
		public UserInfo User { get; set; }

		[BsonElement("details")]
		public string Details { get; set; }

		[BsonElement("action")]
		public DisciplinaryAction Action { get; set; }

		[BsonElement("createdAt")]
		public DateTime CreatedAt { get; set; }
		
	}

	public class DisciplinaryAction
	{
		[BsonId]
		public string Id { get; set; }

		[BsonElement("name")]
		public string Name { get; set; }

		[BsonElement("description")]
		public string Description { get; set; }
	}
}