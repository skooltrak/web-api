using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class Comment
	{
		public Comment()
		{
			CreateDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("object")]
		public Reference Object { get; set; }

		[BsonElement("user")]
		public UserInfo User { get; set; }

		[BsonElement("content")]
		public string Content { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }
	}
}