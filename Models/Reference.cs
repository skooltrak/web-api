using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class Reference
	{
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("name")]
		public string Name { get; set; }
	}

	public class Interval
	{
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
	}

	public class IdObject
	{
		public string Id { get; set; }
	}
}