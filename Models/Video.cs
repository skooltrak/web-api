using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class Video
	{
		public Video() {
			CreatedDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("title")]
		public string Title { get; set; }

		[BsonElement("description")]
		public string Description { get; set; }

		[BsonElement("published")]
		public bool Published { get; set; }

		[BsonElement("courses")]
		public List<CourseReference> Courses { get; set; }

		[BsonElement("assignment")]
		public Reference Assignment { get; set; }

		[BsonElement("tags")]
		public string[] Tags { get; set; }

		[BsonElement("uploadedBy")]
		public UserInfo UploadedBy { get; set; }

		[BsonElement("file")]
		public FileInfo File { get; set; }

		[BsonElement("createdDate")]
		public DateTime CreatedDate { get; set; }
	}
}