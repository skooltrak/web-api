using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class Observation
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("student")]
        public Reference Student { get; set; }

        [BsonElement("year")]
        public int Year { get; set; }

        [BsonElement("period")]
        public Period Period { get; set; }

        [BsonElement("comments")]
        public string Comments { get; set; }

        [BsonElement("secondaryComments")]
        public string secondaryComments { get; set; }

        [BsonElement("createdBy")]
        public UserInfo CreatedBy { get; set; }

        [BsonElement("createdAt")]
        public DateTime CreatedAt { get; set; }
    }

}