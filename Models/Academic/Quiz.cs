using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class Quiz
	{
		public Quiz()
		{
			Questions = new List<Question>();
			CreateDate = DateTime.Now;
			ModificateDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("title")]
		public string Title { get; set; }

		[BsonElement("description")]
		public string Description { get; set; }

		[BsonElement("course")]
		public CourseReference Course { get; set; }

		[BsonElement("createAssignment")]
		public bool CreateAssignment { get; set; }

		[BsonElement("settings")]
		public QuizSettings Settings { get; set; }

		[BsonElement("questions")]
		public List<Question> Questions { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("teacher")]
		public Teacher Teacher { get; set; }

		[BsonElement("modificateDate")]
		public DateTime ModificateDate { get; set; }

		[BsonElement("createUser")]
		public UserInfo CreateUser { get; set; }

	}


	public class QuizSettings
	{
		[BsonElement("isRandom")]
		public bool IsRandom { get; set; }

		[BsonElement("allowContinue")]
		public bool AllowContinue { get; set; }
	}

	public class Question
	{
		public Question()
		{
			Options = new List<QuestionOption>();
		}

		[BsonElement("questionText")]
		public string QuestionText { get; set; }

		[BsonElement("points")]
		public int Points { get; set; }

		[BsonElement("options")]
		public List<QuestionOption> Options { get; set; }
	}

	public class QuestionOption
	{
		[BsonElement("optionText")]
		public string OptionText { get; set; }

		[BsonElement("isCorrect")]
		public bool IsCorrect { get; set; }

	}

}