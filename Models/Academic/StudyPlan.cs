using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class StudyPlan
	{
		public StudyPlan()
		{
			CreateDate = DateTime.Now;
			EnrollCharges = new List<EnrollCharge>();
			Skills = new List<string>();
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("name")]
		public string Name { get; set; }

		[BsonElement("schoolId")]
		public string SchoolId { get; set; }

		[BsonElement("degree")]
		public Reference Degree { get; set; }

		[BsonElement("description")]
		public string Description { get; set; }

		[BsonElement("level")]
		public Level Level { get; set; }

		[BsonElement("active")]
		public bool Active { get; set; }

		[BsonElement("preschool")]
		public bool Preschool { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("modificateDate")]
		public DateTime ModificateDate { get; set; }

		[BsonElement("monthlyCost")]
		public double MonthlyCost { get; set; }

		[BsonElement("enrollCharges")]
		public List<EnrollCharge> EnrollCharges { get; set; }

		[BsonElement("skills")]
		public List<string> Skills { get; set; }

		[BsonElement("hasUser")]
		public bool HasUser { get; set; }
	}


	public class PlanReference {

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("name")]
		public string Name { get; set; }

		[BsonElement("preschool")]
		public bool Preschool { get; set; }

	}

	public class EnrollCharge
	{
		[BsonElement("description")]
		public string Description { get; set; }

		[BsonElement("cost")]
		public double Cost { get; set; }
	}
}