using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using skooltrak_api.Shared;

namespace skooltrak_api.Models
{
	[BsonIgnoreExtraElements]
	public class Course
	{
		public Course()
		{
			Active = true;
			CreateDate = DateTime.Now;
			Buckets = Util.GenerateBuckets();
			Teachers = new List<Reference>();
			IsClass = true;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		public string Name
		{
			get
			{
				return Subject != null ? Subject.ShortName + " - " + Plan.Name : " ";
			}
		}

		[BsonElement("subject")]
		public Subject Subject { get; set; }

		[BsonElement("parentSubject")]
		public Subject ParentSubject { get; set; }

		[BsonElement("plan")]
		public StudyPlan Plan { get; set; }

		[BsonElement("isClass")]
		public bool IsClass { get; set; }

		[BsonElement("teachers")]
		public List<Reference> Teachers { get; set; }

		[BsonElement("weeklyHours")]
		public int WeeklyHours { get; set; }

		[BsonElement("buckets")]
		public List<GradeBucket> Buckets { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("currentPeriod")]
		public Period CurrentPeriod { get; set; }

		[BsonElement("modificateDate")]
		public DateTime ModificateDate { get; set; }

		[BsonElement("relatedCourse")]
		public CourseReference RelatedCourse { get; set; }

		[BsonIgnore]
		public double CurrentScore { get; set; }

		[BsonElement("icon")]
		public string Icon { get; set; }

		[BsonElement("color")]
		public string Color { get; set; }

		[BsonElement("active")]
		public bool Active { get; set; }

	}

	public class CourseReference
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("name")]
		public string Name { get; set; }

		[BsonElement("subject")]
		public Subject Subject { get; set; }

		[BsonElement("parentSubject")]
		public Subject ParentSubject { get; set; }

		[BsonElement("color")]
		public string Color { get; set; }

		[BsonElement("icon")]
		public string Icon { get; set; }
	}

}