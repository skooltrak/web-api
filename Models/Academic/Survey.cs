using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class Survey
	{
		public Survey()
		{
			CreateDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("title")]
		public string Title { get; set; }

		[BsonElement("description")]
		public string Description { get; set; }

		[BsonElement("questions")]
		public List<SurveyQuestion> Questions { get; set; }

		[BsonElement("beginDate")]
		public DateTime BeginDate { get; set; }

		[BsonElement("endDate")]
		public DateTime EndDate { get; set; }

		[BsonElement("allUsers")]
		public bool AllUsers { get; set; }

		[BsonElement("groups")]
		public List<ClassGroup> Groups { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("createUser")]
		public UserInfo CreateUser { get; set; }
	}

	public class SurveyQuestion
	{
		[BsonElement("questionText")]
		public string QuestionText { get; set; }

		[BsonElement("options")]
		public List<SurveyOption> Options { get; set; }
	}

	public class SurveyOption
	{

		[BsonElement("answerText")]
		public string AnswerText { get; set; }

		[BsonElement("count")]
		public int Count { get; set; }
	}

	public class UserSurvey
	{
		public UserSurvey()
		{
			CreateDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("user")]
		public UserInfo User { get; set; }

		[BsonElement("survey")]
		public Reference Survey { get; set; }

		[BsonElement("questions")]
		public List<UserQuestion> Questions { get; set; }

		[BsonIgnore]
		public Student Student { get; set; }

		[BsonIgnore]
		public GroupReference Group { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }
	}

	public class UserQuestion
	{
		[BsonElement("question")]
		public string QuestionText { get; set; }

		[BsonElement("answerIndex")]
		public int AnswerIndex { get; set; }

		[BsonIgnore]
		public string OptionText { get; set; }
	}


}