using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class ExamResult
	{
		public ExamResult()
		{
			CreateDate = DateTime.Now;
			Status = (int)QuizStatusEnum.Pending;
			Answers = new List<ExamAnswer>();
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("assignation")]
		public Reference Assignation { get; set; }

		[BsonElement("answers")]
		public List<ExamAnswer> Answers { get; set; }

		[BsonElement("course")]
		public CourseReference Course { get; set; }

		[BsonElement("exam")]
		public Reference Exam { get; set; }

		[BsonElement("student")]
		public Reference Student { get; set; }

		[BsonElement("totalPoints")]
		public double TotalPoints { get; set; }

		[BsonElement("points")]
		public double Points { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("modificateDate")]
		public DateTime ModificateDate { get; set; }

		[BsonElement("startDate")]
		public DateTime StartDate { get; set; }

		[BsonElement("endDate")]
		public DateTime EndDate { get; set; }

		[BsonElement("minutes")]
		public int Minutes { get; set; }

		[BsonElement("status")]
		public int Status { get; set; }
	}

	public class ExamAnswer
	{
		[BsonElement("question")]
		public ExamQuestion Question { get; set; }

		[BsonElement("points")]
		public double Points { get; set; }

		[BsonElement("responseText")]
		public string ResponseText { get; set; }

		[BsonElement("responseNumber")]
		public double? ResponseNumber { get; set; }

		[BsonElement("responseBoolean")]
		public bool ResponseBoolean { get; set; }

		[BsonElement("comments")]
		public string Comments { get; set; }

		[BsonElement("selection")]
		public QuestionOption Selection { get; set; }

		[BsonElement("matchList")]
		public List<MatchOption> MatchList { get; set; }
	}

}