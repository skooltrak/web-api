using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class EvaluationArea
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("plan")]
        public Reference Plan { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("color")]
        public string Color { get; set; }

        [BsonElement("icon")]
        public string Icon { get; set; }

        [BsonElement("items")]
        public List<EvaluationItem> Items { get; set; }
    }
    public class ReferenceArea
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("color")]
        public string Color { get; set; }

        [BsonElement("icon")]
        public string Icon { get; set; }
    }

    public class EvaluationItem
    {

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }
    }
}