using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class NotasFin
    {
        public NotasFin()
        {
            CreateDate = DateTime.Now;
            ModificateDate = DateTime.Now;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("schoolId")]
        public string SchoolId { get; set; }

        [BsonElement("level")]
        public Level Level { get; set; }

		[BsonElement("group")]
        public string Group { get; set; }

		[BsonElement("periodo")]
        public string Periodo { get; set; }

		[BsonElement("materia")]
        public string Materia { get; set; }

        [BsonElement("student")]
        public string Student { get; set; }

		[BsonElement("nota")]
        public double Nota { get; set; }

		[BsonElement("reclamo")]
        public double Reclamo { get; set; }

		[BsonElement("teacher")]
        public string Teacher { get; set; }

		
		[BsonElement("observacion")]
        public string Observacion { get; set; }


        [BsonElement("createDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime CreateDate { get; set; }

        [BsonElement("modificateDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime ModificateDate { get; set; }

		[BsonElement("createUser")]
        public string CreateUser { get; set; }

		[BsonElement("updateUser")]
        public string updateUser { get; set; }

		[BsonElement("createTerminal")]
        public string CreateTerminal { get; set; }

		[BsonElement("updateTerminal")]
        public string updateTerminal { get; set; }

    }
}