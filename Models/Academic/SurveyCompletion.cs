using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class SurveyCompletion
	{

		public SurveyCompletion()
		{
			CreateDate = DateTime.Now;
			Answers = new List<SurveyAnswer>();
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("user")]
		public UserInfo User { get; set; }

		[BsonElement("survey")]
		public Reference Survey { get; set; }

		[BsonElement("answers")]
		public List<SurveyAnswer> Answers { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }
	}

	public class SurveyAnswer
	{
		[BsonElement("question")]
		public SurveyQuestion Question { get; set; }

		[BsonElement("selected")]
		public SurveyOption Selected { get; set; }
	}
}