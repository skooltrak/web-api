using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class ExamAssignation
    {
        public ExamAssignation()
        {
            CreateDate = DateTime.Now;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("exam")]
        public Exam Exam { get; set; }

        [BsonElement("minutes")]
        public int Minutes { get; set; }

        [BsonElement("course")]
        public CourseReference Course { get; set; }

        [BsonElement("startDate")]
        public DateTime StartDate { get; set; }

        [BsonElement("endDate")]
        public DateTime EndDate { get; set; }

        [BsonElement("grade")]
        public Reference Grade { get; set; }

        [BsonElement("group")]
        public ClassGroup Group { get; set; }

        [BsonElement("teacher")]
        public Teacher Teacher { get; set; }

        [BsonElement("createDate")]
        public DateTime CreateDate { get; set; }

        [BsonElement("createUser")]
        public UserInfo CreateUser { get; set; }

    }
}