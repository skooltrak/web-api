using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class TipoNotas
    {
        public TipoNotas()
        {
            CreateDate = DateTime.Now;
            ModificateDate = DateTime.Now;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }


		[BsonElement("group")]
        public string Group { get; set; }

		
		[BsonElement("materia")]
        public string Materia { get; set; }


		[BsonElement("porcentaje")]
        public string Porcentaje { get; set; }

        [BsonElement("createDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime CreateDate { get; set; }

        [BsonElement("modificateDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime ModificateDate { get; set; }

		[BsonElement("createUser")]
        public string CreateUser { get; set; }

		[BsonElement("updateUser")]
        public string updateUser { get; set; }

		[BsonElement("createTerminal")]
        public string CreateTerminal { get; set; }

		[BsonElement("updateTerminal")]
        public string updateTerminal { get; set; }

    }
}