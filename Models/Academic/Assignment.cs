using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class Assignment
	{
		public Assignment()
		{
			CreateDate = DateTime.Now;
			ModificateDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("title")]
		public string Title { get; set; }

		[BsonElement("type")]
		public AssignmentType Type { get; set; }

		[BsonElement("description")]
		public string Description { get; set; }

		[BsonElement("uploadFile")]
		public bool UploadFile { get; set; }

		[BsonElement("uploadVideo")]
		public bool UploadVideo { get; set; }

		[BsonElement("hasForum")]
		public bool HasForum { get; set; }

		[BsonElement("course")]
		public CourseReference Course { get; set; }

		[BsonElement("group")]
		public Reference Group { get; set; }

		[BsonElement("files")]
		public bool Files { get; set; }

		[BsonElement("startDate")]
		public DateTime StartDate { get; set; }

		[BsonElement("dueDate")]
		public DateTime? DueDate { get; set; }

		[BsonElement("date")]
		public DateTime Date { get; set; }

		[BsonElement("teacher")]
		public Reference Teacher { get; set; }

		[BsonElement("createUser")]
		public UserInfo CreateUser { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("contentBlocks")]
		public List<ContentBlock> ContentBlocks { get; set; }

		[BsonIgnore]
		public List<Document> Documents { get; set; }

		[BsonElement("modificateDate")]
		public DateTime ModificateDate { get; set; }

	}

	public class SupportMaterial
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public FileInfo File { get; set; }
	}


	public class AssignmentComment
	{
		public AssignmentComment()
		{
			CreateDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("user")]
		public UserInfo User { get; set; }

		[BsonElement("assignmentId")]
		public string AssignmentId { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }
	}

}