using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class Perido
    {
        public Perido()
        {
            CreateDate = DateTime.Now;
            ModificateDate = DateTime.Now;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("schoolId")]
        public string SchoolId { get; set; }

        [BsonElement("level")]
        public Level Level { get; set; }

		[BsonElement("group")]
        public string Group { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

		[BsonElement("Inicio")]
        public DateTime Inicio { get; set; }

		[BsonElement("fin")]
        public DateTime Fin { get; set; }

		[BsonElement("status")]
        public string Status { get; set; }

        [BsonElement("createDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime CreateDate { get; set; }

        [BsonElement("modificateDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime ModificateDate { get; set; }

		[BsonElement("createUser")]
        public string CreateUser { get; set; }

		[BsonElement("updateUser")]
        public string updateUser { get; set; }

		[BsonElement("createTerminal")]
        public string CreateTerminal { get; set; }

		[BsonElement("updateTerminal")]
        public string updateTerminal { get; set; }

    }
}