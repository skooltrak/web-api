using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class Attendance
	{
		public Attendance()
		{
			Students = new List<StudentAttendance>();
			CreateDate = DateTime.Now;
			ModificateDate = DateTime.Now;
			Year = DateTime.Now.Year;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("date")]
		public DateTime Date { get; set; }
		
		[BsonElement("period")]
		public Period Period { get; set; }

		[BsonElement("course")]
		public CourseReference Course { get; set; }

		[BsonElement("group")]
		public Reference Group { get; set; }

		[BsonElement("teacher")]
		public Reference Teacher { get; set; }

		[BsonElement("createUser")]
		public UserInfo CreateUser { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("year")]
		public int Year { get; set; }

		[BsonElement("modificateDate")]
		public DateTime ModificateDate { get; set; }

		[BsonElement("students")]
		public List<StudentAttendance> Students { get; set; }
	}

	public class StudentAttendance
	{
		[BsonElement("id")]
		public string Id { get; set; }

		[BsonElement("name")]
		public string Name { get; set; }

		[BsonElement("status")]
		public int Status { get; set; }
	}
}