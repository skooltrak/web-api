using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	[BsonIgnoreExtraElements]
	public class Exam
	{
		public Exam()
		{
			Questions = new List<ExamQuestion>();
			CreateDate = DateTime.Now;
			ModificateDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("title")]
		public string Title { get; set; }

		[BsonElement("description")]
		public string Description { get; set; }

		[BsonElement("course")]
		public CourseReference Course { get; set; }

		[BsonElement("teacher")]
		public Teacher Teacher { get; set; }

		[BsonElement("questions")]
		public List<ExamQuestion> Questions { get; set; }

		[BsonElement("createUser")]
		public UserInfo CreateUser { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("modificateDate")]
		public DateTime ModificateDate { get; set; }

		[BsonElement("documents")]
		public List<FileInfo> Documents { get; set; }

	}

	public class ExamReference
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("title")]
		public string Title { get; set; }

		[BsonElement("description")]
		public string Description { get; set; }

		[BsonElement("course")]
		public CourseReference Course { get; set; }

		[BsonElement("teacher")]
		public Teacher Teacher { get; set; }

		[BsonElement("createUser")]
		public UserInfo CreateUser { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("modificateDate")]
		public DateTime ModificateDate { get; set; }
	}

	[BsonIgnoreExtraElements]
	public class ExamQuestion
	{
		public ExamQuestion()
		{
			Options = new List<QuestionOption>();
			MatchList = new List<MatchOption>();
		}

		[BsonElement("questionText")]
		public string QuestionText { get; set; }

		[BsonElement("points")]
		public double Points { get; set; }

		[BsonElement("type")]
		public QuestionType Type { get; set; }

		[BsonElement("matchList")]
		public List<MatchOption> MatchList { get; set; }

		[BsonElement("options")]
		public List<QuestionOption> Options { get; set; }

		[BsonElement("trueFalse")]
		public bool TrueFalse { get; set; }

		[BsonElement("numberAnswer")]
		public double NumberAnswer { get; set; }

		[BsonElement("")]
		public string TextAnswer { get; set; }
	}

	public class QuestionType
	{
		[BsonElement("name")]
		public string Name { get; set; }

		[BsonElement("code")]
		public int Code { get; set; }
	}

	public class MatchOption
	{

		[BsonElement("index")]
		public int Index { get; set; }

		[BsonElement("optionText")]
		public string OptionText { get; set; }

		[BsonElement("correctMatch")]
		public string CorrectMatch { get; set; }

		[BsonElement("selectedMatch")]
		public List<string> SelectedMatch { get; set; }
	}

	public enum QuestionEnum
	{
		TrueFalse = 0,
		Selection = 1,
		Text = 2,
		LongText = 3,
		Number = 4,
		Match = 5
	}
}