using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class StudentSkill
	{
		public StudentSkill()
		{
			Periods = new List<SkillPeriod>(3);
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("student")]
		public Reference Student { get; set; }

		[BsonElement("skill")]
		public Skill Skill { get; set; }

		[BsonElement("year")]
		public int Year { get; set; }

		[BsonElement("periods")]
		public List<SkillPeriod> Periods { get; set; }
	}

	public class SkillPeriod
	{
		[BsonElement("period")]
		public Period Period { get; set; }

		[BsonElement("value")]
		public string Value { get; set; }
	}
}