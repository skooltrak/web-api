using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class QuizResult
	{
		public QuizResult()
		{
			CreateDate = DateTime.Now;
			Status = (int)QuizStatusEnum.Pending;
			Answers = new List<QuizAnswer>();
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("assignation")]
		public Reference Assignation { get; set; }

		[BsonElement("course")]
		public CourseReference Course { get; set; }

		[BsonElement("quiz")]
		public Reference Quiz { get; set; }

		[BsonElement("student")]
		public Reference Student { get; set; }

		[BsonElement("answers")]
		public List<QuizAnswer> Answers { get; set; }

		[BsonElement("totalPoints")]
		public double TotalPoints { get; set; }

		[BsonElement("points")]
		public double Points { get; set; }

		[BsonIgnore]
		public double Grade
		{
			get
			{
				return (Points / TotalPoints) * 5;
			}
		}

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("modificateDate")]
		public DateTime ModificateDate { get; set; }

		[BsonElement("startDate")]
		public DateTime StartDate { get; set; }

		[BsonElement("endDate")]
		public DateTime EndDate { get; set; }

		[BsonElement("status")]
		public int Status { get; set; }
	}

	public class QuizAnswer
	{
		[BsonElement("question")]
		public Question Question { get; set; }

		[BsonElement("selected")]
		public QuestionOption Selected { get; set; }
	}
}