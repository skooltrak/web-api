using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class TeacherClass
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("day")]
        public DayClass Day { get; set; }

        [BsonElement("group")]
        public ClassGroup Group { get; set; }

        [BsonElement("class")]
        public ClassHour Class { get; set; }
    }

    public class DayClass
    {
        [BsonElement("day")]
        public int Day { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }
    }

    public class TeacherDayClass
    {
        public DayClass Day { get; set; }
        public List<TeacherClass> Classes { get; set; }
    }

}