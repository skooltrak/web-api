using System;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace skooltrak_api.Models
{
	public class PaymentDay
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("title")]
		public string Title { get; set; }

		[BsonElement("startDate")]
		public DateTime StartDate { get; set; }

		[BsonElement("dueDate")]
		public DateTime DueDate { get; set; }

	}
}