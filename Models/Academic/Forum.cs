using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class Forum
	{
		public Forum()
		{
			CreateDate = DateTime.Now;
			Posts = 0;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("name")]
		public string Name { get; set; }

		[BsonElement("course")]
		public CourseReference Course { get; set; }

		[BsonElement("assignment")]
		public Reference Assignment { get; set; }

		[BsonElement("description")]
		public string Description { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("createBy")]
		public UserInfo CreatedBy { get; set; }

		[BsonElement("lastPost")]
		public DateTime LastPost { get; set; }

		[BsonElement("posts")]
		public int Posts { get; set; }
	}

	public class ForumPost
	{
		public ForumPost()
		{
			CreateDate = DateTime.Now;
			Type = "message";
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("file")]
		public Document File { get; set; }

		[BsonElement("type")]
		public string Type { get; set; }

		[BsonElement("forum")]
		public Reference Forum { get; set; }

		[BsonElement("content")]
		public string Content { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("createdBy")]
		public UserInfo CreatedBy { get; set; }
	}
}