using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class QuizAssignation
	{

		public QuizAssignation()
		{
			CreateDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("title")]
		public string Title { get; set; }

		[BsonElement("quiz")]
		public Quiz Quiz { get; set; }

		[BsonElement("duration")]
		public int Duration { get; set; }

		[BsonElement("showResult")]
		public bool ShowResult { get; set; }

		[BsonElement("course")]
		public CourseReference Course { get; set; }

		[BsonElement("startDate")]
		public DateTime StartDate { get; set; }

		[BsonElement("endDate")]
		public DateTime EndDate { get; set; }

		[BsonElement("group")]
		public ClassGroup Group { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("createUser")]
		public UserInfo CreateUser { get; set; }
	}

	
}