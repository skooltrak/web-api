using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	[BsonIgnoreExtraElements]
	public class Subject
	{
		public Subject()
		{
			CreateDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("name")]
		public string Name { get; set; }

		[BsonElement("shortName")]
		public string ShortName { get; set; }

		[BsonElement("parent")]
		public Reference Parent { get; set; }

		[BsonElement("code")]
		public string Code { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("modificateDate")]
		public DateTime ModificateDate { get; set; }


	}


	public class ParentSubject
	{

		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }
		public string Name { get; set; }
		public double Score { get; set; }
		public List<Course> Courses { get; set; }
	}
}