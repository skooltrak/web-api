using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
  public class ArchiveGrade
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }

    [BsonElement("student")]
    public Reference Student { get; set; }

    [BsonElement("year")]
    public int Year { get; set; }

    [BsonElement("plan")]
    public StudyPlan Plan { get; set; }

    [BsonElement("group")]
    public GroupReference Group { get; set; }

    [BsonElement("school")]
    public Reference School { get; set; }

    [BsonElement("period")]
    public Period Period { get; set; }

    [BsonElement("score")]
    public double Score { get; set; }

    [BsonElement("grades")]
    public IEnumerable<GradeRedux> Grades { get; set; }
  }

  public class GradeRedux
  {
    [BsonElement("grade")]
    public Reference Grade { get; set; }

    [BsonElement("course")]
    public CourseReference Course { get; set; }

    [BsonElement("bucket")]
    public GradeBucket Bucket { get; set; }

    [BsonElement("comments")]
    public string Comments { get; set; }

    [BsonElement("score")]
    public double Score { get; set; }

    [BsonElement("createDate")]
    public DateTime CreateDate { get; set; }

    [BsonElement("modificateDate")]
    public DateTime ModificateDate { get; set; }
  }
}