using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class StudentGrade
	{
		public StudentGrade()
		{
			CreateDate = DateTime.Now;
			ModificateDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("grade")]
		public Reference Grade { get; set; }

		[BsonElement("bucket")]
		public GradeBucket Bucket { get; set; }

		[BsonElement("period")]
		public Period Period { get; set; }

		[BsonElement("group")]
		public GroupReference Group { get; set; }

		[BsonElement("course")]
		public CourseReference Course { get; set; }

		[BsonElement("student")]
		public Reference Student { get; set; }

		[BsonElement("comments")]
		public string Comments { get; set; }

		[BsonElement("score")]
		public double Score { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("modificateDate")]
		public DateTime ModificateDate { get; set; }

		[BsonElement("createUser")]
		public UserInfo CreateUser { get; set; }
	}
}
