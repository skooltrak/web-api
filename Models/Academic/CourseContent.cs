using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class CourseContent
	{

		public CourseContent()
		{
			CreateDate = DateTime.Now;
			ModificateDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("title")]
		public string Title { get; set; }

		[BsonElement("body")]
		public string Body { get; set; }

		[BsonElement("documents")]
		public List<Document> Documents { get; set; }

		[BsonElement("course")]
		public CourseReference Course { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("modificateDate")]
		public DateTime ModificateDate { get; set; }

		[BsonElement("createUser")]
		public UserInfo CreateUser { get; set; }
	}
}