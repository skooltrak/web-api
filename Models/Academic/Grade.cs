using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class Grade
    {
        public Grade()
        {
            CreateDate = DateTime.Now;
            ModificateDate = DateTime.Now;
            Groups = new List<GradeGroup>();
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("bucket")]
        public GradeBucket Bucket { get; set; }

        [BsonElement("teacher")]
        public Reference Teacher { get; set; }

        [BsonElement("course")]
        public CourseReference Course { get; set; }

        [BsonElement("date")]
        public DateTime Date { get; set; }

        [BsonElement("groups")]
        public List<GradeGroup> Groups { get; set; }

        [BsonElement("students")]
        public List<GradeStudent> StudentsGrades { get; set; }

        [BsonElement("published")]
        public Boolean Published { get; set; }

        [BsonElement("closed")]
        public Boolean Closed { get; set; }

        [BsonElement("createDate")]
        public DateTime CreateDate { get; set; }

        [BsonElement("period")]
        public Period Period { get; set; }

        [BsonElement("modificateDate")]
        public DateTime ModificateDate { get; set; }

        [BsonElement("createUser")]
        public UserInfo CreateUser { get; set; }

    }


    public class GradeSummary
    {
        public GradeSummary()
        {
            Courses = new List<CourseGrade>();
        }
        public string StudentId { get; set; }
        public List<CourseGrade> Courses { get; set; }
    }

    public class GradeGroup
    {
        public GradeGroup()
        {
            Students = new List<GradeStudent>();
        }

        [BsonElement("group")]
        public GroupReference Group { get; set; }

        [BsonElement("students")]
        public List<GradeStudent> Students { get; set; }
    }

    public class CourseGrade
    {
        public CourseGrade()
        {
            Grades = new List<GradeItem>();
            Attendance = new List<CourseAttendance>();
        }
        public Reference Course { get; set; }
        public List<GradeItem> Grades { get; set; }
        public List<CourseGrade> Children { get; set; }
        public List<CourseAttendance> Attendance { get; set; }
    }

    public class CourseAttendance
    {
        public int Tardies { get; set; }
        public int Absences { get; set; }
    }

    public class GradeItem
    {
        public Period Period { get; set; }
        public double Score { get; set; }
    }

    public class GradeStudent
    {
        public GradeStudent()
        {
            Included = true;
        }

        [BsonElement("student")]
        public Reference Student { get; set; }

        [BsonElement("group")]
        public GroupReference Group { get; set; }

        [BsonElement("comments")]
        public string Comments { get; set; }

        [BsonElement("score")]
        public double Score { get; set; }

        [BsonElement("included")]
        public bool Included { get; set; }
    }
}