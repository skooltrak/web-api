using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
  public class Credit
  {

    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }

    [BsonElement("type")]
    public string Type { get; set; }

    [BsonElement("student")]
    public string Student { get; set; }

    [BsonElement("documentId")]
    public string DocumentId { get; set; }

    [BsonElement("year")]
    public int Year { get; set; }

    [BsonElement("level")]
    public string Level { get; set; }

    [BsonElement("degree")]
    public string Degree { get; set; }

    [BsonElement("period")]
    public int Period { get; set; }

    [BsonElement("subject")]
    public string Subject { get; set; }

    [BsonElement("grade")]
    public double Grade { get; set; }

    [BsonElement("childSubject")]
    public bool ChildSubject { get; set; }

    [BsonElement("parentSubject")]
    public string ParentSubject { get; set; }
  }
}