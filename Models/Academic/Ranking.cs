using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class Ranking
    {

        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("groups")]
        public ClassGroup Group { get; set; }

        [BsonElement("plan")]
        public StudyPlan Plan { get; set; }

        [BsonElement("student")]
        public StudentSummary Student { get; set; }

        [BsonElement("period")]
        public Period Period { get; set; }

        [BsonElement("grade")]
        public double Grade { get; set; }
    }
}