using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	[BsonIgnoreExtraElements]
    public class Degree
    {
        public Degree()
        {
            Active = true;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        [BsonRequired]
        public string Name { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("active")]
        public bool Active { get; set; }

		[BsonElement("createDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime CreateDate { get; set; }

        [BsonElement("modificateDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime ModificateDate { get; set; }

		[BsonElement("createUser")]
        public string CreateUser { get; set; }

		[BsonElement("updateUser")]
        public string updateUser { get; set; }

		[BsonElement("createTerminal")]
        public string CreateTerminal { get; set; }

		[BsonElement("updateTerminal")]
        public string updateTerminal { get; set; }

		
    }
}