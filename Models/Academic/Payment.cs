using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class Payment
	{
		public Payment()
		{
			CreateDate = DateTime.Now;
			Applications = new List<PaymentApplication>();
			CreditNote = false;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }
		
		[BsonElement("creditNote")]
		public bool CreditNote { get; set; }

		[BsonElement("student")]
		public Reference Student { get; set; }

		[BsonElement("description")]
		public string Description { get; set; }

		[BsonElement("amount")]
		public double Amount { get; set; }

		[BsonElement("referenceNumber")]
		public string ReferenceNumber { get; set; }

		[BsonElement("method")]
		public string Method { get; set; }

		[BsonElement("paymentDate")]
		public DateTime PaymentDate { get; set; }

		[BsonElement("applications")]
		public List<PaymentApplication> Applications { get; set; }

		[BsonElement("createUser")]
		public UserInfo CreateUser { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }
	}

	public class PaymentApplication
	{
		[BsonElement("charge")]
		public Charge Charge { get; set; }

		[BsonElement("amount")]
		public double Amount { get; set; }
	}
}