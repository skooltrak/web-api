using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class Level
    {
        [BsonElement("id")]
        public int Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("ordinal")]
        public string Ordinal { get; set; }
    }
}