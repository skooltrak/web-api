using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class Period
	{

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("sort")]
		public int Sort { get; set; }

		[BsonElement("name")]
		public string Name { get; set; }

		[BsonElement("startDate")]
		public DateTime StartDate { get; set; }

		[BsonElement("endDate")]
		public DateTime EndDate { get; set; }
	}
}