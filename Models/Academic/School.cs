using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class School
    {
        public School()
        {
            Contacts = new List<Contact>();
            CreateDate = DateTime.Now;
            ModificateDate = DateTime.Now;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("shortName")]
        public string ShortName { get; set; }

        [BsonElement("logoURL")]
        public string LogoURL { get; set; }

		[BsonElement("address")]
		public string Address { get; set; }

        [BsonElement("website")]
        public string Website { get; set; }

        [BsonElement("currentYear")]
        public int CurrentYear { get; set; }
		
		[BsonElement("motto")]
		public string Motto { get; set; }

        [BsonElement("contacts")]
        public List<Contact> Contacts { get; set; }

        [BsonElement("createDate")]
        public DateTime CreateDate { get; set; }

        [BsonElement("modificateDate")]
        public DateTime ModificateDate { get; set; }

		[BsonElement("createUser")]
        public string CreateUser { get; set; }

		[BsonElement("updateUser")]
        public string updateUser { get; set; }
    }
}