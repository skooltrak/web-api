using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class PreScholarEvaluation
    {
        public PreScholarEvaluation()
        {
            Periods = new List<EvaluationPeriod>();
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("student")]
        public Reference Student { get; set; }

        [BsonElement("area")]
        public ReferenceArea Area { get; set; }

        [BsonElement("item")]
        public EvaluationItem Item { get; set; }

        [BsonElement("periods")]
        public List<EvaluationPeriod> Periods { get; set; }
    }

    public class EvaluationPeriod
    {
        [BsonElement("period")]
        public Period Period { get; set; }

        [BsonElement("value")]
        public string Value { get; set; }
    }
}