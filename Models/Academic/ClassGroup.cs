using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class ClassGroup
    {
        public ClassGroup()
        {
            CreateDate = DateTime.Now;
            ModificateDate = DateTime.Now;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("schoolId")]
        public string SchoolId { get; set; }

        [BsonElement("level")]
        public Level Level { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("counselor")]
        public Reference Counselor { get; set; }

        [BsonElement("studyPlan")]
        public Reference StudyPlan { get; set; }

        [BsonIgnore]
        public long StudentsCount { get; set; }

        [BsonElement("createDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime CreateDate { get; set; }

        [BsonElement("schedule")]
        public List<ClassDay> Schedule { get; set; }

        [BsonElement("modificateDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime ModificateDate { get; set; }

    }

    [BsonIgnoreExtraElements]
    public class GroupReference
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("schoolId")]
        public string SchoolId { get; set; }

        [BsonElement("level")]
        public Level Level { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("counselor")]
        public Reference Counselor { get; set; }
    }

    public class ClassDay
    {
        [BsonElement("day")]
        public int Day { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("classHours")]
        public List<ClassHour> ClassHours { get; set; }
    }

    public class ClassHour
    {
        [BsonElement("startTime")]
        public Time StartTime { get; set; }

        [BsonElement("endTime")]
        public Time EndTime { get; set; }

        [BsonElement("isSync")]
        public bool IsSync { get; set; }

        [BsonElement("inPerson")]
        public bool InPerson { get; set; }

        [BsonElement("course")]
        public CourseReference Course { get; set; }
    }

    public class Time
    {

        [BsonElement("hour")]
        public int Hour { get; set; }

        [BsonElement("minute")]
        public int Minute { get; set; }
    }
}