using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class MessageInbox
	{
		public MessageInbox()
		{
			ArrivalDate = DateTime.Now;
			Read = false;
			Trash = false;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("message")]
		public Message Message { get; set; }

		[BsonElement("reference")]
		public MessageReference Reference { get; set; }

		[BsonElement("receiver")]
		public Receiver Receiver { get; set; }

		[BsonElement("read")]
		public bool Read { get; set; }

		[BsonElement("trash")]
		public bool Trash { get; set; }

		[BsonElement("arrivalDate")]
		public DateTime ArrivalDate { get; set; }

		[BsonElement("readDate")]
		public DateTime ReadDate { get; set; }
	}
}
