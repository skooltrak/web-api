using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class Message
    {
        public Message()
        {
            CreateDate = DateTime.Now;
            Attached = new List<FileInfo>();
            Receivers = new List<Receiver>();
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("status")]
        public int Status { get; set; }

        [BsonElement("content")]
        public string Content { get; set; }

        [BsonElement("contentBlocks")]
        public List<ContentBlock> ContentBlocks { get; set; }

        [BsonElement("sender")]
        public UserInfo Sender { get; set; }

        [BsonElement("receivers")]
        public List<Receiver> Receivers { get; set; }

        [BsonElement("users")]
        public List<UserInfo> Users { get; set; }

        [BsonElement("attached")]
        public List<FileInfo> Attached { get; set; }

        [BsonElement("trash")]
        public bool Trash { get; set; }

        [BsonElement("sendDate")]
        public DateTime SendDate { get; set; }

        [BsonElement("createDate")]
        public DateTime CreateDate { get; set; }
    }

    public class MessageReference
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("contentBlocks")]
        public List<ContentBlock> ContentBlocks { get; set; }

        [BsonElement("sender")]
        public UserInfo Sender { get; set; }

        [BsonElement("status")]
        public int Status { get; set; }
    }
}