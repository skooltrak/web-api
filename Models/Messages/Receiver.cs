using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class Receiver
	{
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("name")]
		public string Name { get; set; }

		[BsonElement("displayName")]
		public string DisplayName { get; set;}

		[BsonElement("role")]
		public Role Role { get; set; }

		[BsonElement("description")]
		public string Description { get; set; }

		[BsonElement("course")]
		public CourseReference Course { get; set; }

		[BsonElement("plan")]
		public Reference Plan { get; set; }

		[BsonElement("group")]
		public GroupReference Group { get; set; }

	}
}