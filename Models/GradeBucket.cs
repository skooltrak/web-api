using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class GradeBucket
	{
		[BsonElement("id")]
		public int Id { get; set; }

		[BsonElement("name")]
		public string Name { get; set; }

		[BsonElement("weighting")]
		public double Weighting { get; set; }
	}
}