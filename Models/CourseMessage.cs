using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class CourseMessage
	{
		public CourseMessage()
		{
			CreateDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("course")]
		public CourseReference Course { get; set; }

		[BsonElement("content")]
		public string Content { get; set; }

		[BsonElement("teacher")]
		public Reference Teacher { get; set; }

		[BsonElement("createUser")]
		public UserInfo CreateUser { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }
	}
}