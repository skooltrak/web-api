using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class Parent
    {
        [BsonElement("name")]
        public string Name { get; set; }

		[BsonElement("relation")]
		public string Relation { get; set; }

		[BsonElement("nationality")]
		public string Nationality { get; set; }

		[BsonElement("documentId")]
		public string DocumentId { get; set; }

		[BsonElement("receiveEmail")]
		public bool ReceiveEmail { get; set; }

        [BsonElement("phoneNumber")]
        public string PhoneNumber { get; set; }

        [BsonElement("mobileNumber")]
        public string MobileNumber { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("workAddress")]
        public string WorkAddress { get; set; }

        [BsonElement("address")]
        public string Address { get; set; }
    }
}