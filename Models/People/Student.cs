using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using skooltrak_api.Shared;

namespace skooltrak_api.Models
{
  public class Student : IPerson
  {
    public Student()
    {
      CreateDate = DateTime.Now;
      ModificateDate = DateTime.Now;
      Active = true;
      Guardians = new List<Parent>();
      Discount = 0;
      Code = Util.GenerateID(8);
    }

    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }

    [BsonElement("Code")]
    public string Code { get; set; }

    [BsonElement("schoolId")]
    public string SchoolId { get; set; }

    [BsonElement("firstName")]
    public string FirstName { get; set; }

    [BsonElement("middleName")]
    public string MiddleName { get; set; }

    [BsonElement("surname")]
    public string Surname { get; set; }

    [BsonElement("secondSurname")]
    public string SecondSurname { get; set; }

    [BsonElement("address")]
    public string Address { get; set; }

    [BsonElement("temporary")]
    public bool Temporary { get; set; }

    [BsonElement("courses")]
    public List<CourseReference> Courses { get; set; }

    [DataType(DataType.EmailAddress)]
    [BsonElement("email")]
    public string Email { get; set; }

    [BsonElement("notes")]
    public string Notes { get; set; }

    [BsonElement("country")]
    public string BirthCountry { get; set; }

    [BsonElement("province")]
    public string Province { get; set; }

    [BsonElement("enrollDate")]
    public DateTime EnrollDate { get; set; }

    [BsonElement("originSchool")]
    public string OriginSchool { get; set; }

    [BsonElement("transferReason")]
    public string TransferReason { get; set; }

    [BsonElement("guardians")]
    public List<Parent> Guardians { get; set; }

    [BsonElement("currentYear")]
    public int CurrentYear { get; set; }

    [BsonElement("mother")]
    public Parent Mother { get; set; }

    [BsonElement("discount")]
    public double Discount { get; set; }

    [BsonElement("photoURL")]
    public string PhotoURL { get; set; }

    [BsonIgnore]
    public double Score { get; set; }

    [BsonElement("father")]
    public Parent Father { get; set; }

    [BsonIgnore]
    public string FullName
    {
      get
      {
        return Util.Capitalize(Surname) + (String.IsNullOrEmpty(SecondSurname) ? "" : " " + Util.Capitalize(SecondSurname)) + ", " + Util.Capitalize(FirstName) + (String.IsNullOrEmpty(MiddleName) ? "" : " " + Util.Capitalize(MiddleName));
      }
    }

    [BsonIgnore]
    public string Name
    {
      get
      {
        return Util.Capitalize(FirstName) + " " + Util.Capitalize(Surname);
      }
    }

    public int Age
    {
      get
      {
        return Util.Age(BirthDate);
      }
    }

    [BsonIgnore]
    public string ShortName
    {
      get
      {
        return Util.Capitalize(Surname) + ", " + Util.Capitalize(FirstName);
      }
    }

    [BsonElement("group")]
    public GroupReference Group { get; set; }

    [BsonElement("plan")]
    public StudyPlan Plan { get; set; }

    [BsonElement("section")]
    public Reference Section { get; set; }

    [BsonElement("birthDate")]
    public DateTime BirthDate { get; set; }

    [BsonElement("gender")]
    public Gender Gender { get; set; }

    [BsonElement("documentId")]
    public string DocumentId { get; set; }

    [BsonElement("avatarURL")]
    public string AvatarURL { get; set; }

    [BsonElement("userId")]
    public string UserId { get; set; }

    [BsonElement("active")]
    public bool Active { get; set; }

    [BsonElement("medicalInfo")]
    public MedicalInfo MedicalInfo { get; set; }

    [BsonElement("createDate")]
    public DateTime CreateDate { get; set; }

    [BsonElement("createdUser")]
    public UserInfo CreatedUser { get; set; }

    [BsonElement("modificateDate")]
    public DateTime ModificateDate { get; set; }

  }

  public class StudentSummary
  {
    [BsonId]
    public string Id { get; set; }
    public string FullName { get; set; }
    public string ShortName { get; set; }
    public string FirstName { get; set; }
    public string MiddleName { get; set; }
    public string Surname { get; set; }
    public string SecondSurname { get; set; }
    public string Name { get; set; }
    public string Gender { get; set; }
    public string DocumentId { get; set; }
    public int Age { get; set; }
    public Reference Plan { get; set; }
    public GroupReference Group { get; set; }
    public double DueAmount { get; set; }
    public double CurrentAmount { get; set; }
    public bool IsDefault { get; set; }
  }

  public class MedicalInfo
  {

    [BsonElement("bloodGroup")]
    public string BloodGroup { get; set; }

    [BsonElement("allergies")]
    public string Allergies { get; set; }

    [BsonElement("medicine")]
    public string Medicine { get; set; }

    [BsonElement("pediatrician")]
    public string Pediatrician { get; set; }

    [BsonElement("hospital")]
    public string Hospital { get; set; }
  }

  public class StudentPerformance
  {
    public StudentPerformance()
    {
      Grades = new List<PerformanceDetail>();
    }
    public Period Period { get; set; }
    public List<PerformanceDetail> Grades { get; set; }
  }

  public class PerformanceDetail
  {
    public CourseReference Course { get; set; }
    public double Grade { get; set; }
  }
}