using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class Contact
    {
        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("type")]
        public string Type { get; set; }

        [BsonElement("text")]
        public string ContactText { get; set; }

        [BsonElement("active")]
        public bool Active { get; set; }
    }

    public class ContactType
    {
        [BsonElement("id")]
        public int Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

		[BsonElement("createDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime CreateDate { get; set; }

        [BsonElement("modificateDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime ModificateDate { get; set; }

		[BsonElement("createUser")]
        public string CreateUser { get; set; }

		[BsonElement("updateUser")]
        public string updateUser { get; set; }

		[BsonElement("createTerminal")]
        public string CreateTerminal { get; set; }

		[BsonElement("updateTerminal")]
        public string updateTerminal { get; set; }
    }
}