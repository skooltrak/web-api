using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class Enrollment
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("studentId")]
        public string StudentId { get; set; }

        [BsonElement("firstName")]
        public string FirstName { get; set; }

        [BsonElement("middleName")]
        public string MiddleName { get; set; }

        [BsonElement("surname")]
        public string Surname { get; set; }

        [BsonElement("secondSurname")]
        public string SecondSurname { get; set; }

        [BsonElement("address")]
        public string Address { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("documentId")]
        public string DocumentId { get; set; }

        [BsonElement("birthDate")]
        public Nullable<DateTime> BirthDate { get; set; }

        [BsonElement("gender")]
        public Gender Gender { get; set; }

        [BsonElement("birthCountry")]
        public string BirthCountry { get; set; }

        [BsonElement("originSchool")]
        public string OriginSchool { get; set; }

        [BsonElement("guardians")]
        public List<Parent> Guardians { get; set; }

        [BsonElement("mother")]
        public Parent Mother { get; set; }

        [BsonElement("father")]
        public Parent Father { get; set; }

        [BsonElement("nextLevel")]
        public Level NextLevel { get; set; }

        [BsonElement("medicalInfo")]
        public MedicalInfo MedicalInfo { get; set; }

        [BsonElement("createDate")]
        public DateTime CreateDate { get; set; }

    }
}