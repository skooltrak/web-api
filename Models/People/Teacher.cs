using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using skooltrak_api.Shared;

namespace skooltrak_api.Models
{
    public class Teacher : IPerson
    {
        public Teacher()
        {
            Subjects = new List<Subject>();
            CreateDate = DateTime.Now;
            ModificateDate = DateTime.Now;
            Code = Util.GenerateID(8);
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("code")]
        public string Code { get; set; }

        [BsonElement("schoolId")]
        public string SchoolId { get; set; }

        [BsonElement("firstName")]
        [BsonRequired]
        public string FirstName { get; set; }

        [BsonElement("middleName")]
        public string MiddleName { get; set; }

        [BsonElement("surname")]
        [BsonRequired]
        public string Surname { get; set; }

        [BsonElement("secondSurname")]
        public string SecondSurname { get; set; }

        [BsonIgnore]
        public string Name
        {
            get
            {
                return FirstName + ' ' + Surname;
            }
        }

        [BsonElement("userId")]
        public string UserId { get; set; }

        public Reference User { get; set; }

        [BsonElement("birthDate")]
        public Nullable<DateTime> BirthDate { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("gender")]
        public Gender Gender { get; set; }

        [BsonElement("subjects")]
        public List<Subject> Subjects { get; set; }

        [BsonElement("createDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime CreateDate { get; set; }

        [BsonElement("modificateDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime ModificateDate { get; set; }
    }

    public class TeacherDTO
    {
        public string Id { get; set; }
        public string SchoolId { get; set; }
        public string FullName { get; set; }
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
        public List<Subject> Subjects { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModificateDate { get; set; }
    }
}