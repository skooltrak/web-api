using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class Activity
	{
		public Activity()
		{
			CreateDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("description")]
		public string Description { get; set; }

		[BsonElement("type")]
		public int Type { get; set; }

		[BsonElement("course")]
		public CourseReference Course { get; set; }

		[BsonElement("private")]
		public bool Private { get; set; }

		[BsonElement("createdBy")]
		public UserInfo CreatedBy { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }
	}
}