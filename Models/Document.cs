using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class Document
	{
		public Document()
		{
			CreateDate = DateTime.Now;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("name")]
		public string Name { get; set; }

		[BsonElement("description")]
		public string Description { get; set; }

		[BsonElement("course")]
		public CourseReference Course { get; set; }

		[BsonElement("student")]
		public Reference Student { get; set; }

		[BsonElement("assignment")]
		public Reference Assignment { get; set; }

		[BsonElement("forum")]
		public Reference Forum { get; set; }

		[BsonElement("file")]
		public FileInfo File { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }

		[BsonElement("createUser")]
		public UserInfo CreateUser { get; set; }
	}


	public class FileInfo
	{

		[BsonElement("id")]
		public string Id { get; set; }

		[BsonElement("fileName")]
		public string FileName { get; set; }

		[BsonElement("size")]
		public long Size { get; set; }

		[BsonElement("type")]
		public string Type { get; set; }
	}
}