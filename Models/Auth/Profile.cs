using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
	public class Profile
	{
		[BsonElement("role")]
		public Role Role { get; set; }

		[BsonElement("code")]
		public string Code { get; set; }

		[BsonElement("active")]
		public bool Active { get; set; }

		[BsonElement("registerDate")]
		public DateTime RegisterDate { get; set; }

		[BsonElement("createDate")]
		public DateTime CreateDate { get; set; }
	}
}