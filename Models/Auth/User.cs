using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using skooltrak_api.Shared;

namespace skooltrak_api.Models
{
    [BsonIgnoreExtraElements]
    public class User
    {
        public User()
        {
            RegisterDate = DateTime.Now;
            Password = Util.GenerateID(6);
            People = new List<Reference>();
            Blocked = false;
            MeetingBlocked = false;
            NotificationMails = new List<string>();
            Language = "es";
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("userName")]
        public string UserName { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("displayName")]
        public string DisplayName { get; set; }

        [BsonElement("meetingBlocked")]
        public Nullable<bool> MeetingBlocked { get; set; }

        [BsonElement("blocked")]
        public bool Blocked { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("password")]
        public string Password { get; set; }

        [BsonElement("isLogged")]
        public bool IsLogged { get; set; }

        [BsonElement("role")]
        public Role Role { get; set; }

        [BsonElement("photoURL")]
        public string PhotoURL { get; set; }

        [BsonElement("notificationMails")]
        public List<string> NotificationMails { get; set; }

        [BsonElement("Language")]
        public string Language { get; set; }

        [BsonIgnore]
        public List<Reference> People { get; set; }

        [BsonElement("registerDate")]
        public DateTime RegisterDate { get; set; }

        [BsonElement("updatedAt")]
        public DateTime UpdatedAt { get; set; }
    }

    [BsonIgnoreExtraElements]
    public class UserInfo
    {
        public UserInfo()
        {
            People = new List<Reference>();
            NotificationMails = new List<string>();
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("userName")]
        public string UserName { get; set; }

        [BsonElement("displayName")]
        public string DisplayName { get; set; }

        [BsonElement("blocked")]
        public bool Blocked { get; set; }

        [BsonElement("meetingBlocked")]
        public bool? MeetingBlocked { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("notificationMails")]
        public List<string> NotificationMails { get; set; }

        [BsonElement("Language")]
        public string Language { get; set; }

        [BsonElement("adminAccess")]
        public List<AdminAccess> AdminAccess { get; set; }

        [BsonElement("group")]
        public GroupReference Group { get; set; }

        [BsonIgnore]
        public List<Reference> People { get; set; }

        [BsonElement("role")]
        public Role Role { get; set; }


        [BsonElement("photoURL")]
        public string PhotoURL { get; set; }

        [BsonElement("updatedAt")]
        public DateTime UpdatedAt { get; set; }

    }

    public class UserDetails
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("userName")]
        public string UserName { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("displayName")]
        public string DisplayName { get; set; }

        [BsonElement("blocked")]
        public bool Blocked { get; set; }

        [BsonElement("meetingBlocked")]
        public bool? MeetingBlocked { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("notificationMails")]
        public List<string> NotificationMails { get; set; }

        [BsonElement("Language")]
        public string Language { get; set; }

        [BsonElement("password")]
        public string Password { get; set; }

        [BsonElement("isLogged")]
        public bool IsLogged { get; set; }

        [BsonElement("role")]
        public Role Role { get; set; }

        [BsonElement("photoURL")]
        public string PhotoURL { get; set; }
        public StudyPlan Plan { get; set; }

        public GroupReference Group { get; set; }

        [BsonIgnore]
        public List<Reference> People { get; set; }

        [BsonElement("registerDate")]
        public DateTime RegisterDate { get; set; }

        [BsonElement("updatedAt")]
        public DateTime UpdatedAt { get; set; }
    }
}