using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class Gender
    {
        [BsonElement("id")]
        public int Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }
    }
}