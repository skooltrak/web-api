using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace skooltrak_api.Models
{
    public class ContentBlock
    {
        [BsonElement("type")]
        public string Type { get; set; }

        [BsonElement("data")]
        public ContentData Data { get; set; }
    }

    public class ContentData
    {
        [BsonElement("text")]
        public string Text { get; set; }

        [BsonElement("level")]
        public int Level { get; set; }

        [BsonElement("items")]
        public List<string> Items { get; set; }

        [BsonElement("style")]
        public string Style { get; set; }

        [BsonElement("caption")]
        public string Caption { get; set; }

        [BsonElement("file")]
        public ContentFileData File { get; set; }

        [BsonElement("stretched")]
        public bool Stretched { get; set; }

        [BsonElement("withBackground")]
        public bool WithBackground { get; set; }

        [BsonElement("withBorder")]
        public bool WithBorder { get; set;}
        
        [BsonElement("link")]
        public string Link { get; set; }

        [BsonElement("meta")]
        public ContentMetadataInfo Meta { get; set; }
    }

    public class ContentFileData
    {
        [BsonElement("url")]
        public string Url { get; set; }
    }

    public class ContentMetadataInfo
    {
        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("image")]
        public ContentFileData Image { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }
    }
}