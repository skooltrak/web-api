namespace skooltrak_api.Models
{
    public class MetadataInfo
    {
        public MetadataInfo()
        {
            Image = new ImageInfo();
        }
        public string Title { get; set; }
        public string Description { get; set; }
        public ImageInfo Image { get; set; }
    }

    public class ImageInfo
    {
        public string Url { get; set; }
    }
}