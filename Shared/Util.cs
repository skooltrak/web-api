using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using skooltrak_api.Models;

namespace skooltrak_api.Shared
{
    public class Util
    {
        public static int Age(DateTime birthDate)
        {
            var today = DateTime.Today;
            var age = today.Year - birthDate.Year;
            if (birthDate.Date > today.AddYears(-age)) age--;
            return age;
        }

        public static double Grade(double totalPoints, double points)
        {
            var result = (points * 4) / totalPoints + 1;
            if (result > 5)
            {
                return 5;
            }

            if (result < 1)
            {
                return 1;
            }
            return result;
        }

        public static int Age()
        {
            var birthDate = DateTime.Today;
            var today = DateTime.Today;
            var age = today.Year - birthDate.Year;
            if (birthDate.Date > today.AddYears(-age)) age--;
            return age;
        }

        public static List<GradeBucket> GenerateBuckets()
        {
            var list = new List<GradeBucket>();
            list.Add(new GradeBucket() { Id = 1, Name = "Daily", Weighting = (100 / 3) });
            list.Add(new GradeBucket() { Id = 2, Name = "Appreciation", Weighting = (100 / 3) });
            list.Add(new GradeBucket() { Id = 3, Name = "Final Exam", Weighting = (100 / 3) });

            return list;
        }

        public static string Capitalize(string input)
        {
            input = input.Trim();
            string[] words = input.Split(" ");
            input = "";
            int index = 0;
            foreach (var word in words)
            {
                var current = word.ToLower();
                input = input + char.ToUpper(current[0]) + current.Substring(1);
                index++;
                if (index < words.Length)
                {
                    input = input + " ";
                }
            }
            return input;
        }

        public static string GenerateID(int maxSize)
        {
            char[] chars = new char[62];
            chars = "1234567890".ToCharArray();
            byte[] data = new byte[1];

            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }

            StringBuilder result = new StringBuilder(maxSize);

            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }

            return result.ToString();
        }
    }

    public class AppSettings
    {
        public string Secret { get; set; }
    }
}