using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;
using skooltrak_api.Models;

namespace skooltrak_api.Shared
{
    public class Mailer
    {
        public async Task SendEmail()
        {
            MimeMessage message = new MimeMessage();
            MailboxAddress from = new MailboxAddress("Skooltrak - Moises Bilingual College", "noreply@skooltrak.com");
            message.From.Add(from);

            MailboxAddress to = new MailboxAddress("Joel Nieto de", "joel@skooltrak.com");
            message.To.Add(to);
            message.Subject = "This is email subject";

            message.Body = new TextPart("html") { Text = "<h1>Hello World!</h1>" };

            SmtpClient client = new SmtpClient();
            await client.ConnectAsync("smtp.zoho.com", 465, true);
            await client.AuthenticateAsync("noreply@skooltrak.com", "CG5rG7menH6");

            await client.SendAsync(message);
            await client.DisconnectAsync(true);
            client.Dispose();
        }


        public async Task SendEmail(string email, string content, string subject, string sender = "Moises Bilingual College Skooltrak", string receiver = "Usuario", List<ContentBlock> blocks = null)
        {
            MimeMessage message = new MimeMessage();
            MailboxAddress from = new MailboxAddress(sender, "noreply@skooltrak.com");
            message.From.Add(from);

            MailboxAddress to = new MailboxAddress(receiver, email);
            message.To.Add(to);
            message.Subject = subject;
            content = "<head><link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\"></head>" + content;
            content += "<br/><br/><p><b>Ingresa <a href=\"https://moises.skooltrak.com\">Aquí</a></b></p>";
            content += Disclaimer();
            content += Footer();
            message.Body = new TextPart("html") { Text = content };

            SmtpClient client = new SmtpClient();
            await client.ConnectAsync("smtp.zoho.com", 465, true);
            await client.AuthenticateAsync("noreply@skooltrak.com", "CG5rG7menH6");

            await client.SendAsync(message);
            await client.DisconnectAsync(true);
            client.Dispose();
        }

        public async Task WelcomeMessage(User user)
        {
            var message = "<h2>Bienvenido(a), " + user.DisplayName + ":</h2>";
            message += "<p>Ha sido registrado(a) en la plataforma de Skooltrak del colegio Moises Bilingual College.</p>";
            message += "<p>Sus credenciales son las siguientes</p>";
            message += "<p><b>Correo electrónico: </b>" + user.Email + "</p>";
            message += "<p><b>Usuario: </b>" + user.UserName + "</p>";
            message += "<p><b>Contraseña: </b>" + user.Password + "</p>";
            await SendEmail(email: user.Email, content: message, subject: "Bienvenido a Skooltrak", receiver: user.DisplayName);
        }

        public async Task SendEmail(Message userMessage)
        {
            MimeMessage message = new MimeMessage();
            userMessage.Users.ForEach(receiver =>
            {
                if (!String.IsNullOrEmpty(receiver.Email))
                {
                    MailboxAddress to = new MailboxAddress(receiver.DisplayName, receiver.Email);
                    message.To.Add(to);
                }
                receiver.NotificationMails.ForEach(email =>
                {
                    MailboxAddress to = new MailboxAddress(receiver.DisplayName, email);
                    message.To.Add(to);
                });
            });
            if (message.To.Count > 0)
            {
                MailboxAddress from = new MailboxAddress(userMessage.Sender.DisplayName, "noreply@skooltrak.com");
                message.From.Add(from);
                message.Subject = userMessage.Title;
                string content = "<head><link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\"></head>" + userMessage.Content;
                userMessage.ContentBlocks.ForEach(block =>
                {
                    if (block.Type == "header")
                    {
                        content = content + "<h4>" + block.Data.Text + "</h4>";
                    }
                    if (block.Type == "paragraph")
                    {
                        content = content + "<p>" + block.Data.Text + "</p>";
                    }
                    if (block.Type == "link")
                    {
                        content = content + "<p><a href=\"" + block.Data.Link + "\">" + block.Data.Meta.Title + "</a>";
                    }

                    if (block.Type == "list")
                    {
                        if (block.Data.Style == "unordered")
                        {
                            content = content + "<ul>";
                            block.Data.Items.ForEach(item =>
                            {
                                content = content + "<li>" + item + "</li>";
                            });
                            content = content + "</ul>";
                        }

                        if (block.Data.Style == "ordered")
                        {
                            content = content + "<ol>";
                            block.Data.Items.ForEach(item =>
                            {
                                content = content + "<li>" + item + "</li>";
                            });
                            content = content + "</ol>";
                        }
                    }
                });
                content += "<br/><br/><p><b>Ingresa <a href=\"https://moises.skooltrak.com\">Aquí</a></b></p>";
                content += Disclaimer();
                content += Footer();
                message.Body = new TextPart("html") { Text = content };
                SmtpClient client = new SmtpClient();
                await client.ConnectAsync("smtp.zoho.com", 465, true);
                await client.AuthenticateAsync("noreply@skooltrak.com", "CG5rG7menH6");

                await client.SendAsync(message);
                await client.DisconnectAsync(true);
                client.Dispose();
            }

        }

        public string Footer()
        {
            return "<img style=\"height:40px;\" src=\"https://moises.skooltrak.com/assets/img/logo-horizontal.png\" alt=\"Skooltrak\">";
        }

        public string Disclaimer()
        {
            return "<div class=\"alert alert-info\">Este mensaje es generado automáticamente por la plataforma Skooltrak. No debe ser respondido por esta vía. Favor ingresar a Skooltrak.</div>";
        }
    }
}