using System;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
using skooltrak_api.Models;

namespace skooltrak_api.Shared
{
	public class Mailing : IMailing
	{

		private readonly string _apiKey;

		public Mailing(string apiKey)
		{
			_apiKey = apiKey;
		}
		public async Task SendEmail(string email, string content, string subject, string sender = "Moises Bilingual College Skooltrak", string receiver = "Usuario")
		{
			var client = new SendGridClient(_apiKey);
			var from = new EmailAddress("no_reply@skooltrak.com", sender);
			var to = new EmailAddress(email, receiver);
			var body = "<head><link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\"></head>" + content;
			body += "<br/><br/><p><b>Ingresa <a href=\"https://moises.skooltrak.com\">Aquí</a></b></p>";
			body += Disclaimer();
			body += Footer();
			var msg = MailHelper.CreateSingleEmail(from, to, subject, content, body);
			await client.SendEmailAsync(msg);
		}

		public async Task WelcomeMessage(User user)
		{
			var message = "<h2>Bienvenido(a), " + user.DisplayName + ":</h2>";
			message += "<p>Ha sido registrado(a) en la plataforma de Skooltrak del colegio Moises Bilingual College.</p>";
			message += "<p>Sus credenciales son las siguientes</p>";
			message += "<p><b>Correo electrónico: </b>" + user.Email + "</p>";
			message += "<p><b>Usuario: </b>" + user.UserName + "</p>";
			message += "<p><b>Contraseña: </b>" + user.Password + "</p>";
			await SendEmail(email: user.Email, content: message, subject: "Bienvenido a Skooltrak", receiver: user.DisplayName);
		}

		public void SendEmail(Message userMessage)
		{
			var client = new SendGridClient(_apiKey);
			userMessage.Users.ForEach(async receiver =>
			{
				if (!String.IsNullOrEmpty(receiver.Email))
				{
					var to = new EmailAddress(receiver.Email, receiver.DisplayName);
					var from = new EmailAddress(userMessage.Sender.Email, userMessage.Sender.DisplayName);
					var body = "<head><link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\"></head>" + userMessage.Content;
					body += "<br/><br/><p><b>Ingresa <a href=\"https://moises.skooltrak.com\">Aquí</a></b></p>";
					body += Disclaimer();
					body += Footer();
					var msg = MailHelper.CreateSingleEmail(from, to, userMessage.Title, userMessage.Content, body);
					await client.SendEmailAsync(msg);
				}
			});
		}

		public string Footer()
		{
			return "<img style=\"height:40px;\" src=\"https://moises.skooltrak.com/assets/img/logo-horizontal.png\" alt=\"Skooltrak\">";
		}

		public string Disclaimer()
		{
			return "<div class=\"alert alert-info\">Este mensaje es generado automáticamente por la plataforma Skooltrak. No debe ser respondido por esta vía. Favor ingresar a Skooltrak.</div>";
		}
	}

	public interface IMailing { }
}