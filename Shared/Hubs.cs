using Microsoft.AspNetCore.SignalR;

namespace skooltrak_api.Shared
{
	public class ForumHub : Hub
	{
	}

	public class ActivityHub : Hub { }

	public class MessageHub : Hub {}

	public class ActivityHubContext : IHubContext<ForumHub>
	{
		public ActivityHubContext() {}
		public IHubClients Clients { get; }

		public IGroupManager Groups => throw new System.NotImplementedException();
	}
}