using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using skooltrak_api.Filter;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace skooltrak_api
{
  public class ConfigureSwaggerOptions : IConfigureNamedOptions<SwaggerGenOptions>
  {
    private readonly IApiVersionDescriptionProvider _provider;

    public ConfigureSwaggerOptions(IApiVersionDescriptionProvider provider)
    {
      _provider = provider;
    }

    public void Configure(SwaggerGenOptions options)
    {
      foreach (var description in _provider.ApiVersionDescriptions)
      {
        options.SwaggerDoc(description.GroupName, CreateVersionInfo(description));
      }
      options.OperationFilter<AddRequiredHeaderParameter>();
      var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
      var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
      options.IncludeXmlComments(xmlPath);
    }

    public void Configure(string name, SwaggerGenOptions options)
    {
      Configure(options);
    }

    private OpenApiInfo CreateVersionInfo(ApiVersionDescription description)
    {
      var info = new OpenApiInfo()
      {
        Title = "Skooltrak API",
        Version = description.ApiVersion.ToString(),
        Description = "SkoolTrak API using MongoDB and .NET Core",
        Contact = new OpenApiContact
        {
          Name = "Joel Nieto",
          Email = "joel@skooltrak.com",
          Url = new Uri("https://twitter.com/TheDevJoel"),
        },
      };

      if (description.IsDeprecated)
      {
        info.Description += " This API version had been deprecated.";
      }
      return info;
    }
  }
}