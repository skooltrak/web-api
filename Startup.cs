﻿using skooltrak_api.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using skooltrak_api.Filter;
using Microsoft.AspNetCore.HttpOverrides;
using skooltrak_api.Shared;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Mvc.ApiExplorer;

namespace skooltrak_api
{
	public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {


      var config = new ServerConfig();
      var accessor = new HttpContextAccessor();
      Configuration.Bind(config);
      var context = new DbContext(config.MongoDB);
      services.AddSingleton<IDbContext>(context);
      services.AddSingleton<IMailing>(new Mailing(config.SendGrindAPIKey));
      services.AddSingleton<IAnnouncementsService>(new AnnouncementsService(context));
      services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
      services.AddSingleton<ICoursesService>(new CoursesService(context));
      services.AddSingleton<IClassGroupsService>(new ClassGroupsService(context));
      services.AddSingleton<ITopicTagsService>(new TopicTagsService(context));
      services.AddSingleton<ISchoolsService>(new SchoolsService(context));
      services.AddSingleton<IStudentsService>(new StudentsService(context));
      services.AddSingleton<IStudyPlansService>(new StudyPlansService(context));
      services.AddSingleton<ISubjectsService>(new SubjectsService(context));
      services.AddSingleton<ITeachersService>(new TeachersService(context));
      services.AddSingleton<ITopicsService>(new TopicsService(context));
      services.AddSingleton<IDegreesService>(new DegreesService(context));
      services.AddSingleton<IAssignmentsService>(new AssignmentsService(context));
      services.AddSingleton<IUsersService>(new UsersService(context));
      services.AddSingleton<IRolesService>(new RolesService(context));
      services.AddSingleton<IFilesService>(new FilesService(context));
      services.AddSingleton<IPreScholarService>(new PreScholarService(context));
      services.AddSingleton<ICleaningService>(new CleaningService(context));
      services.AddSingleton<IExamsAssignationsService>(new ExamAssignationsService(context));
      services.AddSingleton<IProfilesService>(new ProfilesService(context));
      services.AddSingleton<IExamResultsService>(new ExamResultsService(context));
      services.AddSingleton<IAssignmentTypesService>(new AssignmentTypesService(context));
      services.AddSingleton<IQuizesService>(new QuizesService(context));
      services.AddSingleton<IGradesService>(new GradesService(context));
      services.AddSingleton<IStudentGradesService>(new StudentGradesService(context));
      services.AddSingleton<IChargesService>(new ChargesService(context));
      services.AddSingleton<IPaymentDaysService>(new PaymentDaysService(context));
      services.AddSingleton<ISkillsService>(new SkillsService(context));
      services.AddSingleton<ICountriesService>(new CountriesService(context));
      services.AddSingleton<IAuthenticationService>(new AuthenticationService(context));
      services.AddSingleton<IPeriodsService>(new PeriodsService(context, new StudentsService(context)));
      services.AddSingleton<IDocumentsService>(new DocumentsService(context, new ActivitiesService(context, activityHubContext)));
      services.AddSingleton<IAttendanceService>(new AttendanceService(context));
      services.AddSingleton<ICourseMessagesService>(new CourseMessagesService(context));
      services.AddSingleton<IForumsService>(new ForumsService(context));
      services.AddSingleton<IMessagesService>(new MessagesService(context));
      services.AddSingleton<IPaymentsService>(new PaymentsService(context));
      services.AddSingleton<IPersonalService>(new PersonalService(context));
      services.AddSingleton<IAccessService>(new AccessService());
      services.AddSingleton<IActivitiesService>(new ActivitiesService(context, activityHubContext));
      services.AddSingleton<ICourseContentsService>(new CourseContentsService(context));
      services.AddSingleton<IVideosService>(new VideosService(context));
      services.AddSingleton<IClassroomsService>(new ClassroomsService(context));
      services.AddSingleton<ICommentsService>(new CommentsService(context));
      services.AddSingleton<IExamsService>(new ExamsService(context));
      services.AddSingleton<IIncidentsService>(new IncidentsService(context));
      services.AddSingleton<ICreditsService>(new CreditsService(context));
      services.AddScoped<NotificationsService>();
      services.AddSingleton<IQuizAssignationsService>(new QuizAssignationsService(context));
      services.AddSingleton<IQuizResultsService>(new QuizResultsService(context));
      services.AddSingleton<ISurveysService>(new SurveysServices(context));
      services.AddApiVersioning(config =>
      {
        config.DefaultApiVersion = new Microsoft.AspNetCore.Mvc.ApiVersion(1, 0);
        config.AssumeDefaultVersionWhenUnspecified = true;
        config.ReportApiVersions = true;
      });

      services.AddVersionedApiExplorer(setup =>
      {
        setup.GroupNameFormat = "'v'VVV";
        setup.SubstituteApiVersionInUrl = true;
      });

      services.AddSwaggerGen();
      services.ConfigureOptions<ConfigureSwaggerOptions>();
      services.AddCors(options =>
      {
        options.AddPolicy("AllowOrigin",
                  builder => builder
                      .WithOrigins("http://localhost", "http://localhost:4200", "http://localhost:4300", "http://localhost:8100", "http://localhost:8101", "https://demo.skooltrak.com", "https://www.demo.skooltrak.com")
                      .AllowAnyHeader()
                      .AllowAnyMethod()
                      .AllowCredentials());
      });
      services.AddMvc(options =>
      {
        options.Filters.Add(new ApiActionFilter(context));
      });
      services.AddSignalR();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IApiVersionDescriptionProvider provider)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseExceptionHandler("/error");
        // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
        app.UseHsts();
      }
      app.UseRouting();



      app.UseSwagger(c => { });
      app.UseSwaggerUI(c =>
      {
        foreach (var description in provider.ApiVersionDescriptions)
        {
          c.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
        }
      });

      app.UseCors("AllowOrigin");

      app.UseForwardedHeaders(new ForwardedHeadersOptions
      {
        ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
      });

      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllers();
        endpoints.MapHub<ForumHub>("/api/forum_chat");
        endpoints.MapHub<MessageHub>("api/messages_stream");
        endpoints.MapHub<ActivityHub>("api/activity_stream");
      });

      activityHubContext = app.ApplicationServices.GetService<IHubContext<ActivityHub>>();
      messageHubContext = app.ApplicationServices.GetService<IHubContext<MessageHub>>();
    }

    public static IHubContext<ActivityHub> activityHubContext;
    public static IHubContext<MessageHub> messageHubContext;
    public static string sendGrindAPIKey;
  }
}
