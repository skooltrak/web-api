using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;
using System.Linq;

namespace skooltrak_api.Services
{
	public class IncidentsService : IIncidentsService
	{
		private readonly IDbContext _context;

		public IncidentsService(IDbContext context)
		{
			_context = context;
		}

		public async Task CheckIncident(string id, UserInfo user)
		{
			var incident = await _context.Incidents.Find(x => x.Id == id).FirstOrDefaultAsync();
			if (incident != null)
			{
				if (incident.Checks.Any(x => x.User.Id == user.Id) == false)
				{
					incident.Checks.Add(new IncidentCheck()
					{
						User = user,
						CheckedAt = DateTime.Now
					});
					await Update(id, incident, user);
				}
			}
		}

		public async Task<Incident> Create(Incident element, UserInfo user)
		{
			element.CreatedBy = user;
			await _context.Incidents.InsertOneAsync(element);
			return element;
		}

		public async Task<IEnumerable<Incident>> Get(UserInfo user)
		{
			return await _context.Incidents.Find(x => true).SortByDescending(x => x.CreatedAt).ToListAsync();
		}

		public async Task<Incident> Get(string id, UserInfo user)
		{
			return await _context.Incidents.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.Incidents.DeleteOneAsync(x => x.Id == id);
		}

		public async Task Update(string id, Incident element, UserInfo user)
		{
			var update = Builders<Incident>.Update
				.Set(x => x.Title, element.Title)
				.Set(x => x.Details, element.Details)
				.Set(x => x.Student, element.Student)
				.Set(x => x.Course, element.Course)
				.Set(x => x.IncidentDate, element.IncidentDate)
				.Set(x => x.Documents, element.Documents)
				.Set(x => x.Checks, element.Checks)
				.Set(x => x.Updates, element.Updates)
				.Set(x => x.UpdatedAt, DateTime.Now);
			await _context.Incidents.UpdateOneAsync(x => x.Id == id, update);
		}

		public async Task AddUpdates(string id, IncidentUpdate update, UserInfo user)
		{
			var incident = await _context.Incidents.Find(x => x.Id == id).FirstOrDefaultAsync();
			if (incident == null)
			{
				throw new System.InvalidOperationException();
			}
			update.CreatedAt = DateTime.Now;
			incident.Updates.Add(update);
			await Update(id, incident, user);
		}
	}

	public interface IIncidentsService : IContextService<Incident, Incident>
	{
		Task CheckIncident(string id, UserInfo user);
		Task AddUpdates(string id, IncidentUpdate update, UserInfo user);
	}
}