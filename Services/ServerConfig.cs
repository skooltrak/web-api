namespace skooltrak_api.Services
{
    public class ServerConfig
    {
        public MongoDBConfig MongoDB { get; set; } = new MongoDBConfig();
        public string SendGrindAPIKey { get; set; }
	}

    public class MongoDBConfig
    {
        public string Database { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string User { get; set; }
        public string Method { get; set; }
        public string Password { get; set; }
        public string ConnectionString
        {
            get
            {
                return $@"{Method}://{User}:{Password}@{Host}";
            }
        }
    }
}