using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class CountriesService : ICountriesService
	{
		private readonly IDbContext _context;

		public CountriesService(IDbContext context)
		{
			_context = context;
		}

		public async Task<Country> Create(Country country, UserInfo user)
		{
			await _context.Countries.InsertOneAsync(country);
			country.Provinces.Add(country.Name);
			await _context.Countries.ReplaceOneAsync(x => x.Id == country.Id, country);
			return country;
		}

		public async Task<IEnumerable<Country>> Get(UserInfo user)
		{
			return await _context.Countries.Find(x => true).SortBy(x => x.Name).ToListAsync();
		}

		public async Task<Country> Get(string id, UserInfo user)
		{
			return await _context.Countries.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public Task Remove(string id, UserInfo user)
		{
			throw new System.NotImplementedException();
		}

		public async Task Update(string id, Country country, UserInfo user)
		{
			var update = Builders<Country>.Update
				.Set(x => x.Name, country.Name)
				.Set(x => x.Provinces, country.Provinces);
			await _context.Countries.UpdateOneAsync(x => x.Id == id, update);
		}
	}

	public interface ICountriesService : IContextService<Country, Country> { }
}