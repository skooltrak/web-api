using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;
using skooltrak_api.Shared;

namespace skooltrak_api.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IDbContext _context;
        private readonly Mailer mailer;

        public AuthenticationService(IDbContext context)
        {
            _context = context;
            mailer = new Mailer();
        }

        public async Task ChangePassword(string id, string newPassword)
        {
            var user = await _context.Users.Find(x => x.Id == id).FirstOrDefaultAsync();

            if (user == null)
            {
                return;
            }
            var update = Builders<User>.Update.Set(x => x.Password, newPassword);
            await _context.Users.UpdateOneAsync(x => x.Id == id, update);

            var message = "<h3>Hola, " + user.DisplayName + ":</h3>";
            message += "<p>Su contraseña ha sido actualizada exitosamente.</p>";
            message += "<small>Si este cambio no fue realizado por usted, favor comunicarse con el colegio</small>";
            await mailer.SendEmail(email: user.Email, content: message, subject: "Actualización de contraseña", receiver: user.DisplayName);
        }

        public async Task<UserInfo> Login(Login login)
        {
            var user = await _context.Users.
                Find(x => (x.Email.ToLower() == login.UserName.ToLower() || x.UserName.ToLower() == login.UserName.ToLower()) && x.Password == login.Password)
                .Project<UserInfo>(Builders<User>.Projection.Expression(x => new UserInfo()
                {
                    Id = x.Id,
                    DisplayName = x.DisplayName,
                    UserName = x.UserName,
                    Email = x.Email,
                    Group = (GetGroup(x)),
                    PhotoURL = x.PhotoURL,
                    MeetingBlocked = x.MeetingBlocked,
                    Role = x.Role,
                    NotificationMails = x.NotificationMails
                }))
                .FirstOrDefaultAsync();

            if (user == null)
            {
                return user;
            }

            if (user.Role.Code == (int)RoleEnum.Administrator)
            {
                return user;
            }

            if (user.Role.Code == (int)RoleEnum.Student)
            {
                var person = await _context.Students.Find(x => x.UserId == user.Id).Project<Reference>(Builders<Student>.Projection.Expression(x => new Reference()
                {
                    Id = x.Id,
                    Name = x.Name
                })).FirstOrDefaultAsync();
                user.People.Add(person);

                return user;
            }

            if (user.Role.Code == (int)RoleEnum.Teacher)
            {
                user.People.Add(await _context.Teachers.Find(x => x.UserId == user.Id).Project<Reference>(Builders<Teacher>.Projection.Expression(x => new Reference()
                {
                    Id = x.Id,
                    Name = x.Name
                })).FirstOrDefaultAsync());
                return user;
            }

            return user;
        }

        public GroupReference GetGroup(User user)
        {
            if (user.Role.Code == (int)RoleEnum.Student)
            {
                var student = _context.Students.Find(x => x.UserId == user.Id).FirstOrDefault();
                if (student == null)
                {
                    return new GroupReference();
                }
                return student.Group;
            }
            return new GroupReference();
        }

        public async Task ResetPassword(string email)
        {
            var user = await _context.Users.Find(x => x.Email == email).FirstOrDefaultAsync();
            if (user == null)
            {
                return;
            }
            string newPassword = Util.GenerateID(6);

            var update = Builders<User>.Update.Set(x => x.Password, newPassword);
            await _context.Users.UpdateOneAsync(x => x.Id == user.Id, update);

            var message = "<h3>Hola, " + user.DisplayName + ":</h3>";
            message += "<p>Su contraseña ha sido reiniciada exitosamente, su nueva contraseña es:</p>";
            message += "<b>" + newPassword + "</b>";
            await mailer.SendEmail(email: email, content: message, subject: "Reinicio de contraseña", receiver: user.DisplayName);
        }

        public async Task UpdateProfile(string id, User user)
        {
            var update = Builders<User>.Update
                .Set(x => x.DisplayName, user.DisplayName)
                .Set(x => x.Email, user.Email)
                .Set(x => x.Description, user.Description)
                .Set(x => x.UserName, user.UserName);
            await _context.Users.UpdateOneAsync(x => x.Id == id, update);
        }

        public async Task<bool> ValidateBlock(UserInfo user)
        {
            var current = await _context.Users.Find(x => x.Id == user.Id).FirstOrDefaultAsync();
            return !current.Blocked;
        }

    }

    public interface IAuthenticationService
    {
        Task<bool> ValidateBlock(UserInfo user);
        Task<UserInfo> Login(Login login);
        Task ResetPassword(string email);
        Task UpdateProfile(string id, User user);
        Task ChangePassword(string userId, string newPassword);
    }

    public class Login
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}