using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class RolesService : IRolesService
	{
		private readonly IDbContext _context;
		public RolesService(IDbContext context)
		{
			_context = context;
		}

		public async Task<Role> Create(Role role, UserInfo user)
		{
			await _context.Roles.InsertOneAsync(role);
			return role;
		}

		public async Task<IEnumerable<Role>> Get(UserInfo user)
		{
			return await _context.Roles.Find(x => true).ToListAsync();
		}

		public async Task<Role> Get(string id, UserInfo user)
		{
			return await _context.Roles.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.Roles.DeleteOneAsync(x => x.Id == id);
		}

		public async Task Update(string id, Role role, UserInfo user)
		{
			await _context.Roles.ReplaceOneAsync(x => x.Id == id, role);
		}
	}

	public interface IRolesService : IContextService<Role, Role> { }
}