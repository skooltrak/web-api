using System.Collections.Generic;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class AccessService : IAccessService
	{
		public IEnumerable<AdminAccess> GetAccess()
		{
			return AdminAccess.GetAll<AdminAccess>();
		}
	}

	public interface IAccessService
	{
		IEnumerable<AdminAccess> GetAccess();
	}
}