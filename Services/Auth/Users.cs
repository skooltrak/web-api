using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;
using skooltrak_api.Shared;

namespace skooltrak_api.Services
{
    public class UsersService : IUsersService
    {
        private readonly IDbContext _context;
        private readonly Mailer mailer;
        public UsersService(IDbContext context)
        {
            _context = context;
            mailer = new Mailer();
        }

        public async Task<User> Create(User user, UserInfo CreatedUser)
        {
            user.UserName = user.UserName.ToUpper();
            await _context.Users.InsertOneAsync(user);
            if (user.Email == null || user.Email.Length == 0)
            {
                return user;
            }
            await mailer.WelcomeMessage(user);
            return user;
        }

        public async Task<IEnumerable<User>> Get(UserInfo user)
        {
            return await _context.Users.Find(x => true).ToListAsync();
        }

        public async Task<User> Get(string id, UserInfo user)
        {
            return await _context.Users.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<User> GetByEmail(string email)
        {
            return await _context.Users.Find(x => x.Email == email).FirstOrDefaultAsync();
        }
        public async Task ResetPassword(string email)
        {
            var user = await _context.Users.Find(x => x.Email == email).FirstOrDefaultAsync();
            if (user == null)
            {
                return;
            }
            string newPassword = Util.GenerateID(6);

            var update = Builders<User>.Update
                        .Set(x => x.Password, newPassword)
                        .Set(x => x.UpdatedAt, DateTime.Now);
            await _context.Users.UpdateOneAsync(x => x.Id == user.Id, update);

            var message = "<h3>Hola, " + user.DisplayName + "</h3>";
            message += "<p>Tu contraseña ha sido reiniciada exitosamente, tu nueva contraseña es:</p>";
            message += "<b>" + newPassword + "</b>";
            await mailer.SendEmail(email: email, content: message, subject: "Reinicio de contraseña", receiver: user.DisplayName);
        }

        public async Task Remove(string id, UserInfo user)
        {
            await _context.Users.DeleteOneAsync(x => x.Id == id);
        }

        public async Task Update(string id, User userIn, UserInfo user)
        {
            var update = Builders<User>.Update
                .Set(x => x.Description, userIn.Description)
                .Set(x => x.Email, userIn.Email)
                .Set(x => x.Role, userIn.Role)
                .Set(x => x.DisplayName, userIn.DisplayName)
                .Set(x => x.Password, userIn.Password)
                .Set(x => x.Blocked, userIn.Blocked)
                .Set(x => x.MeetingBlocked, userIn.MeetingBlocked)
                .Set(x => x.UpdatedAt, DateTime.Now)
                .Set(x => x.UserName, userIn.UserName.ToLower());
            await _context.Users.UpdateOneAsync(x => x.Id == id, update);
        }

        public async Task<bool> ValidateUser(User userIn)
        {
            User user;
            if (String.IsNullOrEmpty(userIn.Email))
            {
                user = await _context.Users.Find(x => x.UserName.ToLower() == userIn.UserName.ToLower()).FirstOrDefaultAsync();
                if (user == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                user = await _context.Users.Find(x => x.Email.ToLower() == userIn.Email.ToLower() || x.UserName.ToLower() == userIn.UserName.ToLower()).FirstOrDefaultAsync();
                if (user == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public async Task UpdateProfile(User user, string id)
        {
            var update = Builders<User>.Update
                .Set(x => x.Email, user.Email)
                .Set(x => x.DisplayName, user.DisplayName)
                .Set(x => x.NotificationMails, user.NotificationMails)
                .Set(x => x.UserName, user.UserName.ToLower());
            await _context.Users.UpdateOneAsync(x => x.Id == id, update);
        }

        public async Task<IEnumerable<UserDetails>> GetDetails()
        {
            return await _context.Users.Find(x => true).Project<UserDetails>(Builders<User>.Projection.Expression(x => new UserDetails()
            {
                Id = x.Id,
                DisplayName = x.DisplayName,
                UserName = x.UserName,
                Email = x.Email,
                PhotoURL = x.PhotoURL,
                Password = x.Password,
                Blocked = x.Blocked,
                MeetingBlocked = x.MeetingBlocked,
                Plan = (GetPlan(x)),
                Role = x.Role,
                People = (GetPeople(x)),
                RegisterDate = x.RegisterDate,
                Group = (GetGroup(x)),
                UpdatedAt = x.UpdatedAt
            })).ToListAsync();
        }

        public GroupReference GetGroup(User user)
        {
            if (user.Role.Code == (int)RoleEnum.Student)
            {
                var student = _context.Students.Find(x => x.UserId == user.Id).FirstOrDefault();
                if (student == null)
                {
                    return new GroupReference();
                }
                return student.Group;
            }
            return new GroupReference();
        }

        public StudyPlan GetPlan(User user)
        {
            if (user.Role.Code == (int)RoleEnum.Student)
            {
                var student = _context.Students.Find(x => x.UserId == user.Id).FirstOrDefault();
                if (student == null)
                {
                    return new StudyPlan();
                }
                return student.Plan;
            }
            return new StudyPlan();
        }


        private List<Reference> GetPeople(User user)
        {
            var people = new List<Reference>();

            if (user.Role.Code == (int)RoleEnum.Student)
            {
                people.Add(_context.Students.Find(x => x.UserId == user.Id).Project<Reference>(Builders<Student>.Projection.Expression(x => new Reference()
                {
                    Id = x.Id,
                    Name = x.Name
                })).FirstOrDefault());
            }

            if (user.Role.Code == (int)RoleEnum.Teacher)
            {
                people.Add(_context.Teachers.Find(x => x.UserId == user.Id).Project<Reference>(Builders<Teacher>.Projection.Expression(x => new Reference()
                {
                    Id = x.Id,
                    Name = x.Name
                })).FirstOrDefault());
            }
            return people;
        }

        public async Task ChangeAvatar(string id, string photoURL)
        {
            var update = Builders<User>.Update
                .Set(x => x.PhotoURL, photoURL);
            var updatePosts = Builders<ForumPost>.Update
                .Set(x => x.CreatedBy.PhotoURL, photoURL);
            var videosPosts = Builders<Video>.Update
                .Set(x => x.UploadedBy.PhotoURL, photoURL);
            var messagePosts = Builders<MessageInbox>.Update
                .Set(x => x.Message.Sender.PhotoURL, photoURL);
            var documents = Builders<Document>.Update
                .Set(x => x.CreateUser.PhotoURL, photoURL);
            await _context.Users.UpdateOneAsync(x => x.Id == id, update);
            _ = _context.Videos.UpdateManyAsync(x => x.UploadedBy.Id == id, videosPosts);
            _ = _context.MessageInboxes.UpdateManyAsync(x => x.Message.Sender.Id == id, messagePosts);
            _ = _context.Documents.UpdateManyAsync(x => x.CreateUser.Id == id, documents);
            _ = _context.ForumPosts.UpdateManyAsync(X => X.CreatedBy.Id == id, updatePosts);
        }

        public async Task<IEnumerable<Survey>> GetSurveys(UserInfo user)
        {
            var result = await _context.SurveyAnswers.Find(x => x.User.Id == user.Id).ToListAsync();
            var ids = result.Select(x => x.Survey.Id).ToArray();
            return await _context.Surveys.Find(x => (DateTime.Now >= x.BeginDate && DateTime.Now <= x.EndDate) && !ids.Contains(x.Id)).ToListAsync();
        }

        public async Task<IEnumerable<Message>> GetMessages(string id)
        {
            return await _context.Messages.Find(x => x.Sender.Id == id).SortByDescending(x => x.SendDate).ToListAsync();
        }
    }

    public interface IUsersService : IContextService<User, User>
    {
        Task<User> GetByEmail(string email);
        Task<IEnumerable<Message>> GetMessages(string id);
        Task<IEnumerable<Survey>> GetSurveys(UserInfo user);
        Task<IEnumerable<UserDetails>> GetDetails();
        Task ChangeAvatar(string id, string photoURL);
        Task UpdateProfile(User user, string id);
        Task<bool> ValidateUser(User user);
        Task ResetPassword(string email);
    }
}