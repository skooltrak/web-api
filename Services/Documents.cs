using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using skooltrak_api.Models;


namespace skooltrak_api.Services
{
    public class DocumentsService : IDocumentsService
    {
        private readonly IDbContext _context;
        private readonly IActivitiesService _activity;

        public DocumentsService(IDbContext context, IActivitiesService activity)
        {
            _context = context;
            _activity = activity;
        }

        public async Task<Document> Create(Document document, UserInfo user)
        {
            document.CreateUser = user;
            await _context.Documents.InsertOneAsync(document);
            if (document.Course != null)
            {
                await _activity.NewDocument(document);
            }

            return document;
        }

        public async Task<IEnumerable<Document>> Get(UserInfo user)
        {
            return await _context.Documents.Find(x => true).ToListAsync();
        }

        public async Task<Document> Get(string id, UserInfo user)
        {
            return await _context.Documents.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task Remove(string id, UserInfo user)
        {
            var doc = await _context.Documents.Find(x => x.Id == id).FirstOrDefaultAsync();
            if (doc != null)
            {
                await _context.bucket.DeleteAsync(ObjectId.Parse(doc.File.Id));
            }

            await _context.Documents.DeleteOneAsync(x => x.Id == id);
        }

        public Task Update(string id, Document element, UserInfo user)
        {
            throw new System.NotImplementedException();
        }
    }

    public interface IDocumentsService : IContextService<Document, Document> { }
}