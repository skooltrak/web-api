using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class VideosService : IVideosService
	{
		private readonly IDbContext _context;

		public VideosService(IDbContext context)
		{
			_context = context;
		}

		public async Task<Video> Create(Video element, UserInfo user)
		{
			element.UploadedBy = user;
			await _context.Videos.InsertOneAsync(element);
			return element;
		}

		public async Task<IEnumerable<Video>> Get(UserInfo user)
		{
			return await _context.Videos.Find(x => true).SortByDescending(x => x.CreatedDate).ToListAsync();
		}

		public async Task<Video> Get(string id, UserInfo user)
		{
			return await _context.Videos.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			var video = await _context.Videos.Find(x => x.Id == id).FirstOrDefaultAsync();
			await _context.bucket.DeleteAsync(ObjectId.Parse(video.File.Id));
			await _context.Videos.DeleteOneAsync(x => x.Id == id);
		}

		public async Task TogglePublished(string id)
		{
			var video = await _context.Videos.Find(x => x.Id == id).FirstOrDefaultAsync();
			var update = Builders<Video>.Update.Set(x => x.Published, !video.Published);
			await _context.Videos.UpdateOneAsync(x => x.Id == id, update);
		}

		public async Task Update(string id, Video element, UserInfo user)
		{
			var update = Builders<Video>.Update
				.Set(x => x.Published, element.Published)
				.Set(x => x.Title, element.Title)
				.Set(x => x.Courses, element.Courses)
				.Set(x => x.Description, element.Description)
				.Set(x => x.Tags, element.Tags);
			await _context.Videos.UpdateOneAsync(x => x.Id == element.Id, update);
		}
	}

	public interface IVideosService : IContextService<Video, Video>
	{
		Task TogglePublished(string id);
	}
}