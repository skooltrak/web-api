using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class ExamsService : IExamsService
	{
		private readonly IDbContext _context;

		public ExamsService(IDbContext context)
		{
			_context = context;
		}

		public async Task<Exam> Create(Exam exam, UserInfo user)
		{
			exam.CreateUser = user;
			await _context.Exams.InsertOneAsync(exam);
			return exam;
		}

		public async Task<Exam> Get(string id, UserInfo user)
		{
			return await _context.Exams.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task<IEnumerable<ExamReference>> Get(UserInfo user)
		{
			return await _context.Exams.Find(x => true).Project<ExamReference>(Builders<Exam>.Projection.Expression(x => new ExamReference()
			{
				Id = x.Id,
				Title = x.Title,
				Description = x.Description,
				Course = x.Course,
				Teacher = x.Teacher,
				CreateDate = x.CreateDate,
				CreateUser = x.CreateUser,
				ModificateDate = x.ModificateDate
			})).ToListAsync();
		}

		public async Task<IEnumerable<ExamAssignation>> GetAssignations(string id)
		{
			return await _context.ExamAssignations.Find(x => x.Exam.Id == id).SortBy(x => x.Group.Level.Ordinal).ThenBy(x => x.CreateDate).ToListAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.Exams.DeleteOneAsync(x => x.Id == id);
		}

		public async Task Update(string id, Exam exam, UserInfo user)
		{
			var update = Builders<Exam>.Update
				.Set(x => x.Title, exam.Title)
				.Set(x => x.Description, exam.Description)
				.Set(x => x.Questions, exam.Questions)
				.Set(x => x.Course, exam.Course)
				.Set(x => x.Documents, exam.Documents)
				.Set(x => x.ModificateDate, DateTime.Now);

			await _context.Exams.UpdateOneAsync(x => x.Id == id, update);
		}
	}

	public interface IExamsService : IContextService<Exam, ExamReference>
	{
		Task<IEnumerable<ExamAssignation>> GetAssignations(string id);
	}
}