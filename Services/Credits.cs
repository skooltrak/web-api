using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
  public class CreditsService : ICreditsService
  {

    private readonly IDbContext _context;

    public CreditsService(IDbContext context)
    {
      _context = context;
    }

    public async Task<IEnumerable<Credit>> GetCredits(string documentId, string Level)
    {
      return await _context.Credits.Find(x => x.DocumentId == documentId && x.Subject != null && x.Type == Level && x.Grade > 0 && !x.ChildSubject)
          .SortBy(x => x.Year)
          .ThenBy(x => x.Period)
          .ThenBy(x => x.Subject)
          .ToListAsync();
    }

    public Task<IEnumerable<Years>> GetYears(string documentId)
    {
      throw new System.NotImplementedException();
      // var credits = await _context.Credits.Find(x => x.DocumentId == documentId)
      //     .SortBy(x => x.Year)
      //     .ThenBy(x => x.Period)
      //     .ThenBy(x => x.Subject)
      //     .ToListAsync();
      // var grouped = credits.GroupBy(x => new { x.Year, x.Level }).Select(x => new
      // {
      //     Year = x.Key,
      //     Grades = x.Select(item => new { item.Subject, item })
      // });
      // return grouped;
    }
  }

  public interface ICreditsService
  {
    Task<IEnumerable<Credit>> GetCredits(string documentId, string Level);
    Task<IEnumerable<Years>> GetYears(string documentId);
  }

  public class Years
  {
    public int Year { get; set; }
    public string Level { get; set; }
    public double Score { get; set; }
  }
}