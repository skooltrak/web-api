using System.Collections.Generic;
using System.Threading.Tasks;
using skooltrak_api.Models;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using System.Threading;

namespace skooltrak_api.Services
{

    public class ConnectionThrottlingPipeline
    {
        private readonly Semaphore openConnectionSemaphore;

        public ConnectionThrottlingPipeline(IMongoClient client)
        {
            //Only grabbing half the available connections to hedge against collisions.
            //If you send every operation through here
            //you should be able to use the entire connection pool.
            openConnectionSemaphore = new Semaphore(client.Settings.MaxConnectionPoolSize / 2,
                    client.Settings.MaxConnectionPoolSize / 2);
        }

        public async Task<T> AddRequest<T>(Task<T> task)
        {
            openConnectionSemaphore.WaitOne();
            try
            {
                var result = await task;
                return result;
            }
            finally
            {
                openConnectionSemaphore.Release();
            }
        }
    }
    public class DbContext : IDbContext
    {
        public IMongoDatabase db;
        public DbContext(MongoDBConfig config)
        {
            var client = new MongoClient(config.ConnectionString);
            db = client.GetDatabase(config.Database);
        }

        public IMongoCollection<Student> Students => db.GetCollection<Student>("students");
        public IMongoCollection<Teacher> Teachers => db.GetCollection<Teacher>("teachers");
        public IMongoCollection<ClassGroup> Groups => db.GetCollection<ClassGroup>("class_groups");
        public IMongoCollection<Degree> Degrees => db.GetCollection<Degree>("degrees");
        public IMongoCollection<Level> Levels => db.GetCollection<Level>("levels");
        public IMongoCollection<StudyPlan> StudyPlans => db.GetCollection<StudyPlan>("study_plans");
        public IMongoCollection<Subject> Subjects => db.GetCollection<Subject>("subjects");
        public IMongoCollection<School> Schools => db.GetCollection<School>("schools");
        public IMongoCollection<Course> Courses => db.GetCollection<Course>("courses");
        public IMongoCollection<Topic> Topics => db.GetCollection<Topic>("topics");
        public IMongoCollection<Assignment> Assignments => db.GetCollection<Assignment>("assignments");
        public IMongoCollection<User> Users => db.GetCollection<User>("users");
        public IMongoCollection<Role> Roles => db.GetCollection<Role>("roles");
        public IMongoCollection<Announcement> Announcements => db.GetCollection<Announcement>("announcements");
        public IMongoCollection<TopicTag> Tags => db.GetCollection<TopicTag>("tags");
        public IMongoCollection<Quiz> Quizes => db.GetCollection<Quiz>("quizes");
        public IMongoCollection<AssignmentType> AssignmentTypes => db.GetCollection<AssignmentType>("assignment_types");
        public IGridFSBucket bucket => new GridFSBucket(db);
        public IMongoCollection<Grade> Grades => db.GetCollection<Grade>("grades");
        public IMongoCollection<StudentGrade> StudentGrades => db.GetCollection<StudentGrade>("student_grades");
        public IMongoCollection<Charge> Charges => db.GetCollection<Charge>("charges");
        public IMongoCollection<PaymentDay> PaymentDays => db.GetCollection<PaymentDay>("payment_days");
        public IMongoCollection<Payment> Payments => db.GetCollection<Payment>("payments");
        public IMongoCollection<Country> Countries => db.GetCollection<Country>("countries");
        public IMongoCollection<Message> Messages => db.GetCollection<Message>("messages");
        public IMongoCollection<Document> Documents => db.GetCollection<Document>("documents");
        public IMongoCollection<Attendance> Attendance => db.GetCollection<Attendance>("attendance");
        public IMongoCollection<Period> Periods => db.GetCollection<Period>("period");
        public IMongoCollection<Forum> Forums => db.GetCollection<Forum>("forums");
        public IMongoCollection<Skill> Skills => db.GetCollection<Skill>("skills");
        public IMongoCollection<ForumPost> ForumPosts => db.GetCollection<ForumPost>("forum_posts");
        public IMongoCollection<CourseMessage> CourseMessages => db.GetCollection<CourseMessage>("course_messages");
        public IMongoCollection<MessageInbox> MessageInboxes => db.GetCollection<MessageInbox>("message_inbox");
        public IMongoCollection<CourseContent> CourseContents => db.GetCollection<CourseContent>("course_contents");
        public IMongoCollection<Incident> Incidents => db.GetCollection<Incident>("incidents");
        public IMongoCollection<QuizAssignation> QuizAssignations => db.GetCollection<QuizAssignation>("quiz_assignations");
        public IMongoCollection<QuizResult> QuizResults => db.GetCollection<QuizResult>("quiz_results");
        public IMongoCollection<Activity> Activities => db.GetCollection<Activity>("activities");
        public IMongoCollection<Survey> Surveys => db.GetCollection<Survey>("surveys");
        public IMongoCollection<UserSurvey> SurveyAnswers => db.GetCollection<UserSurvey>("survey_answers");
        public IMongoCollection<StudentBalance> Balances => db.GetCollection<StudentBalance>("students_balances");
        public IMongoCollection<Comment> Comments => db.GetCollection<Comment>("comments");
        public IMongoCollection<ExamResult> ExamResults => db.GetCollection<ExamResult>("exam_results");
        public IMongoCollection<Exam> Exams => db.GetCollection<Exam>("exams");
        public IMongoCollection<ExamAssignation> ExamAssignations => db.GetCollection<ExamAssignation>("exam_assignations");
        public IMongoCollection<ArchiveGrade> ArchiveGrades => db.GetCollection<ArchiveGrade>("archive_grades");
        public IMongoCollection<Observation> Observations => db.GetCollection<Observation>("observations");
        public IMongoCollection<EvaluationArea> EvaluationAreas => db.GetCollection<EvaluationArea>("evaluation_areas");
        public IMongoCollection<Ranking> Rankings => db.GetCollection<Ranking>("rankings");
        public IMongoCollection<Video> Videos => db.GetCollection<Video>("videos");
        public IMongoCollection<TeacherClass> TeacherClasses => db.GetCollection<TeacherClass>("teacher_classes");
        public IMongoCollection<StudentSkill> StudentSkills => db.GetCollection<StudentSkill>("students_skills");
        public IMongoCollection<Credit> Credits => db.GetCollection<Credit>("credits");
        public IMongoCollection<PreScholarEvaluation> PreScholarEvaluations => db.GetCollection<PreScholarEvaluation>("kinder_evaluations");
        public IMongoCollection<Enrollment> Enrollments => db.GetCollection<Enrollment>("enrollments");
        public IMongoCollection<Classroom> Classrooms => db.GetCollection<Classroom>("classrooms");
        IMongoDatabase IDbContext.db => db;
    }

    public interface IDbContext
    {
        IMongoDatabase db { get; }
        IGridFSBucket bucket { get; }
        IMongoCollection<Enrollment> Enrollments { get; }
        IMongoCollection<Student> Students { get; }
        IMongoCollection<Teacher> Teachers { get; }
        IMongoCollection<ClassGroup> Groups { get; }
        IMongoCollection<Degree> Degrees { get; }
        IMongoCollection<Incident> Incidents { get; }
        IMongoCollection<ExamResult> ExamResults { get; }
        IMongoCollection<Level> Levels { get; }
        IMongoCollection<StudyPlan> StudyPlans { get; }
        IMongoCollection<PreScholarEvaluation> PreScholarEvaluations { get; }
        IMongoCollection<Subject> Subjects { get; }
        IMongoCollection<TeacherClass> TeacherClasses { get; }
        IMongoCollection<School> Schools { get; }
        IMongoCollection<Skill> Skills { get; }
        IMongoCollection<StudentSkill> StudentSkills { get; }
        IMongoCollection<Exam> Exams { get; }
        IMongoCollection<CourseContent> CourseContents { get; }
        IMongoCollection<Course> Courses { get; }
        IMongoCollection<Topic> Topics { get; }
        IMongoCollection<Assignment> Assignments { get; }
        IMongoCollection<Ranking> Rankings { get; }
        IMongoCollection<User> Users { get; }
        IMongoCollection<Role> Roles { get; }
        IMongoCollection<Survey> Surveys { get; }
        IMongoCollection<ExamAssignation> ExamAssignations { get; }
        IMongoCollection<UserSurvey> SurveyAnswers { get; }
        IMongoCollection<TopicTag> Tags { get; }
        IMongoCollection<AssignmentType> AssignmentTypes { get; }
        IMongoCollection<Quiz> Quizes { get; }
        IMongoCollection<StudentBalance> Balances { get; }
        IMongoCollection<Grade> Grades { get; }
        IMongoCollection<StudentGrade> StudentGrades { get; }
        IMongoCollection<Announcement> Announcements { get; }
        IMongoCollection<PaymentDay> PaymentDays { get; }
        IMongoCollection<Payment> Payments { get; }
        IMongoCollection<Charge> Charges { get; }
        IMongoCollection<Message> Messages { get; }
        IMongoCollection<MessageInbox> MessageInboxes { get; }
        IMongoCollection<Document> Documents { get; }
        IMongoCollection<Country> Countries { get; }
        IMongoCollection<Period> Periods { get; }
        IMongoCollection<Forum> Forums { get; }
        IMongoCollection<Observation> Observations { get; }
        IMongoCollection<EvaluationArea> EvaluationAreas { get; }
        IMongoCollection<ForumPost> ForumPosts { get; }
        IMongoCollection<ArchiveGrade> ArchiveGrades { get; }
        IMongoCollection<Attendance> Attendance { get; }
        IMongoCollection<CourseMessage> CourseMessages { get; }
        IMongoCollection<QuizAssignation> QuizAssignations { get; }
        IMongoCollection<Credit> Credits { get; }
        IMongoCollection<Comment> Comments { get; }
        IMongoCollection<QuizResult> QuizResults { get; }
        IMongoCollection<Activity> Activities { get; }
        IMongoCollection<Classroom> Classrooms { get; }
        IMongoCollection<Video> Videos { get; }
    }

    public interface IContextService<T, D>
    {
        Task<IEnumerable<D>> Get(UserInfo user);
        Task<T> Get(string id, UserInfo user);
        Task<T> Create(T element, UserInfo user);
        Task Update(string id, T element, UserInfo user);
        Task Remove(string id, UserInfo user);
    }
}