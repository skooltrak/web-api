using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
    public class SkillsService : ISkillsService
    {
        private readonly IDbContext _context;

        public SkillsService(IDbContext context)
        {
            _context = context;
        }

        public async Task<Skill> Create(Skill element, UserInfo user)
        {
            await _context.Skills.InsertOneAsync(element);
            return element;
        }

        public async Task<IEnumerable<Skill>> Get(UserInfo user)
        {
            return await _context.Skills.Find(x => true).ToListAsync();
        }

        public async Task<Skill> Get(string id, UserInfo user)
        {
            return await _context.Skills.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public void InitSkills()
        {
            var students = _context.Students.Find(x => x.Active).ToList();
            var skills = _context.Skills.Find(x => true).ToList();
            var periods = _context.Periods.Find(x => true).ToList();
            students.ForEach(student =>
            {
                skills.ForEach(skill =>
                {
                    var current = _context.StudentSkills.Find
                        (x => x.Skill.Id == skill.Id && x.Student.Id == student.Id).FirstOrDefault();
                    if (current == null)
                    {
                        var newSkill = new StudentSkill()
                        {
                            Skill = skill,
                            Student = new Reference()
                            {
                                Id = student.Id,
                                Name = student.ShortName
                            },
                            Year = DateTime.Now.Year
                        };
                        periods.ForEach(period =>
                        {
                            newSkill.Periods.Add(new SkillPeriod()
                            {
                                Period = period,
                                Value = null
                            });
                        });
                        _context.StudentSkills.InsertOne(newSkill);
                    }
                });
            });
        }

        public async Task Remove(string id, UserInfo user)
        {
            await _context.Skills.DeleteOneAsync(x => x.Id == id);
        }


        public async Task SetValue(SkillValue item)
        {
            var filter = Builders<StudentSkill>.Filter.And(
                Builders<StudentSkill>.Filter.Eq(x => x.Student.Id, item.StudentId),
                Builders<StudentSkill>.Filter.Eq(x => x.Year, item.Year),
                Builders<StudentSkill>.Filter.Eq(x => x.Skill.Id, item.SkillId),
                Builders<StudentSkill>.Filter.ElemMatch(x => x.Periods, x => x.Period.Id == item.PeriodId)
            );

            var update = Builders<StudentSkill>.Update.Set(x => x.Periods[-1].Value, item.Value);
            await _context.StudentSkills.UpdateOneAsync(filter, update);
        }

        public async Task Update(string id, Skill element, UserInfo user)
        {
            var update = Builders<StudentSkill>.Update.Set(x => x.Skill, element);
            await _context.Skills.ReplaceOneAsync(x => x.Id == id, element);
            await _context.StudentSkills.UpdateManyAsync(x => x.Skill.Id == id, update);
        }

    }

    public interface ISkillsService : IContextService<Skill, Skill>
    {
        Task SetValue(SkillValue item);
        void InitSkills();
    }

    public class SkillValue
    {
        public string StudentId { get; set; }
        public int Year { get; set; }
        public string SkillId { get; set; }
        public string PeriodId { get; set; }
        public string Value { get; set; }
    }
}