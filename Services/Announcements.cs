using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class AnnouncementsService : IAnnouncementsService
	{
		private readonly IDbContext _context;

		public AnnouncementsService(IDbContext context)
		{
			_context = context;
		}

		public async Task<Announcement> Create(Announcement announcement, UserInfo user)
		{
			announcement.Author = user;
			await _context.Announcements.InsertOneAsync(announcement);
			return announcement;
		}

		public async Task<IEnumerable<Announcement>> Get(UserInfo user)
		{
			return await _context.Announcements.Find(x => true).ToListAsync();
		}

		public async Task<Announcement> Get(string id, UserInfo user)
		{
			return await _context.Announcements.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task<IEnumerable<Announcement>> GetActive()
		{
			return await _context.Announcements.Find(x => x.ActiveSince <= DateTime.Now && x.ActiveUntil >= DateTime.Now).ToListAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.Announcements.DeleteOneAsync(x => x.Id == id);
		}

		public async Task Update(string id, Announcement announcement, UserInfo user)
		{
			await _context.Announcements.ReplaceOneAsync(x => x.Id == id, announcement);
		}
	}

	public interface IAnnouncementsService : IContextService<Announcement, Announcement>
	{
		Task<IEnumerable<Announcement>> GetActive();
	}
}