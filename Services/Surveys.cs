using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class SurveysServices : ISurveysService
	{
		private readonly IDbContext _context;

		public SurveysServices(IDbContext context)
		{
			_context = context;
		}

		public async Task AnswerSurvey(UserSurvey answer, UserInfo user)
		{
			answer.User = user;
			var survey = await _context.Surveys.Find(x => x.Id == answer.Survey.Id).FirstOrDefaultAsync();
			int i = 0;
			answer.Questions.ForEach(question =>
			{
				survey.Questions[i].Options[question.AnswerIndex].Count++;
				i++;
			});

			await _context.Surveys.ReplaceOneAsync(x => x.Id == survey.Id, survey);
			await _context.SurveyAnswers.InsertOneAsync(answer);
		}


		public async Task RechargeSurvey(string id)
		{
			var answers = await _context.SurveyAnswers.Find(x => x.Survey.Id == id).ToListAsync();
			var survey = await _context.Surveys.Find(x => x.Id == id).FirstOrDefaultAsync();

			answers.ForEach(answer =>
			{
				int i = 0;
				answer.Questions.ForEach(question =>
				{
					survey.Questions[i].Options[question.AnswerIndex].Count++;
					i++;
				});
			});

			await _context.Surveys.ReplaceOneAsync(x => x.Id == survey.Id, survey);
		}

		public async Task<Survey> Create(Survey survey, UserInfo user)
		{
			survey.CreateUser = user;
			await _context.Surveys.InsertOneAsync(survey);
			return survey;
		}

		public async Task<IEnumerable<Survey>> Get(UserInfo user)
		{
			return await _context.Surveys.Find(x => true).SortByDescending(x => x.CreateDate).ToListAsync();
		}

		public async Task<Survey> Get(string id, UserInfo user)
		{
			return await _context.Surveys.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.Surveys.DeleteOneAsync(x => x.Id == id);
		}

		public async Task Update(string id, Survey element, UserInfo user)
		{
			var update = Builders<Survey>.Update
				.Set(x => x.Description, element.Description)
				.Set(x => x.Title, element.Title)
				.Set(x => x.BeginDate, element.BeginDate)
				.Set(x => x.EndDate, element.EndDate)
				.Set(x => x.Questions, element.Questions);
			await _context.Surveys.UpdateOneAsync(x => x.Id == id, update);
		}

		public async Task<IEnumerable<UserSurvey>> GetAnswers(string id)
		{
			return await _context.SurveyAnswers.Find(x => x.Survey.Id == id).Project<UserSurvey>(Builders<UserSurvey>.Projection.Expression(x => new UserSurvey()
			{
				Id = x.Id,
				Survey = x.Survey,
				User = x.User,
				Questions = x.Questions,
				Student = (GetStudent(x.User.Id)),
				Group = (GetStudent(x.User.Id).Group),
				CreateDate = x.CreateDate
			})).ToListAsync();
		}
		private Student GetStudent(string id)
		{
			return _context.Students.Find(x => x.UserId == id).FirstOrDefault();
		}
	}



	public interface ISurveysService : IContextService<Survey, Survey>
	{
		Task AnswerSurvey(UserSurvey answer, UserInfo user);
		Task<IEnumerable<UserSurvey>> GetAnswers(string id);
		Task RechargeSurvey(string id);
	}
}