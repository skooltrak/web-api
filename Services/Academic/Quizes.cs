using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class QuizesService : IQuizesService
	{
		private readonly IDbContext _context;

		public QuizesService(IDbContext context)
		{
			_context = context;
		}

		public async Task<Quiz> Create(Quiz quiz, UserInfo user)
		{
			quiz.CreateUser = user;
			await _context.Quizes.InsertOneAsync(quiz);
			return quiz;
		}

		public async Task<IEnumerable<Quiz>> Get(UserInfo user)
		{
			return await _context.Quizes.Find(x => true).ToListAsync();
		}

		public async Task<Quiz> Get(string id, UserInfo user)
		{
			return await _context.Quizes.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.Quizes.DeleteOneAsync(x => x.Id == id);
		}

		public async Task Update(string id, Quiz quiz, UserInfo user)
		{
			var update = Builders<Quiz>.Update
					.Set(x => x.Questions, quiz.Questions)
					.Set(x => x.Settings, quiz.Settings)
					.Set(x => x.Course, quiz.Course)
					.Set(x => x.Title, quiz.Title)
					.Set(x => x.ModificateDate, DateTime.Now);

			await _context.Quizes.UpdateOneAsync<Quiz>(x => x.Id == id, update);
		}
	}

	public interface IQuizesService : IContextService<Quiz, Quiz> { }
}