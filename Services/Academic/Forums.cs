using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class ForumsService : IForumsService
	{
		private readonly IDbContext _context;

		public ForumsService(IDbContext context)
		{
			_context = context;
		}

		public async Task AddPost(string forumId, ForumPost post, UserInfo user)
		{
			post.CreatedBy = user;
			await _context.ForumPosts.InsertOneAsync(post);
			var update = Builders<Forum>.Update.Inc(x => x.Posts, 1).Set(x => x.LastPost, DateTime.Now);
			await _context.Forums.UpdateOneAsync(x => x.Id == forumId, update);
		}

		public async Task<Forum> Create(Forum forum, UserInfo user)
		{
			forum.CreatedBy = user;
			await _context.Forums.InsertOneAsync(forum);
			return forum;
		}

		public async Task<IEnumerable<Forum>> Get(UserInfo user)
		{
			return await _context.Forums.Find(x => true).ToListAsync();
		}

		public async Task<Forum> Get(string id, UserInfo user)
		{
			return await _context.Forums.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task<IEnumerable<Document>> GetDocuments(string id)
		{
			return await _context.Documents.Find(x => x.Forum.Id == id).SortByDescending(x => x.CreateDate).ToListAsync();
		}

		public async Task<IEnumerable<ForumPost>> GetPosts(string id)
		{
			return await _context.ForumPosts.Find(x => x.Forum.Id == id).SortByDescending(x => x.CreateDate).ToListAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.ForumPosts.DeleteManyAsync(x => x.Forum.Id == id);
			await _context.Forums.DeleteOneAsync(x => x.Id == id);
		}

		public async Task RemovePost(string forumId, string id)
		{
			await _context.ForumPosts.DeleteOneAsync(x => x.Id == id);
			var update = Builders<Forum>.Update.Inc(x => x.Posts, -1);
			await _context.Forums.UpdateOneAsync(x => x.Id == forumId, update);
		}

		public async Task Update(string id, Forum forum, UserInfo user)
		{
			var update = Builders<Forum>.Update
				.Set(x => x.Name, forum.Name)
				.Set(x => x.Description, forum.Description);
			await _context.Forums.UpdateOneAsync(x => x.Id == id, update);
		}
	}

	public interface IForumsService : IContextService<Forum, Forum>
	{
		Task<IEnumerable<ForumPost>> GetPosts(string id);
		Task<IEnumerable<Document>> GetDocuments(string id);
		Task AddPost(string forumId, ForumPost post, UserInfo user);
		Task RemovePost(string forumId, string id);
	}
}