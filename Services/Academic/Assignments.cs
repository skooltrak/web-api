using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;
using skooltrak_api.Shared;

namespace skooltrak_api.Services
{
    public class AssignmentsService : IAssignmentsService
    {
        private readonly IDbContext _context;
        private readonly Mailer mailer;

        public AssignmentsService(IDbContext context)
        {
            _context = context;
            mailer = new Mailer();
        }

        public async Task<Assignment> Create(Assignment assignment, UserInfo user)
        {
            if (assignment.Type.Sumative)
            {
                var count = await _context.Assignments.Find(x => x.Group.Id == assignment.Group.Id && x.StartDate == assignment.StartDate && x.Type.Sumative).CountDocumentsAsync();
                if (count >= 3)
                {
                    throw new System.InvalidOperationException();
                }
            }
            assignment.CreateUser = user;
            if (assignment.DueDate == null)
            {
                assignment.DueDate = assignment.StartDate;
            }
            await _context.Assignments.InsertOneAsync(assignment);
            if (assignment.HasForum)
            {
                var forum = new Forum()
                {
                    Name = assignment.Title,
                    Course = new CourseReference()
                    {
                        Id = assignment.Course.Id,
                        Name = assignment.Course.Name,
                        Subject = assignment.Course.Subject,
                        ParentSubject = assignment.Course.ParentSubject,
                        Color = assignment.Course.Color,
                        Icon = assignment.Course.Icon
                    },
                    Assignment = new Reference()
                    {
                        Id = assignment.Id,
                        Name = assignment.Title
                    }
                };
                await _context.Forums.InsertOneAsync(forum);
            }
            _ = NotifyAssignment(assignment);
            return assignment;
        }

        public async Task<Assignment> Get(string id, UserInfo user)
        {
            var assignment = await _context.Assignments.Find(x => x.Id == id).FirstOrDefaultAsync();
            assignment.Documents = await _context.Documents.Find(x => x.Assignment.Id == id && (x.CreateUser.Role.Code == (int)RoleEnum.Teacher)).ToListAsync();
            return assignment;
        }

        public async Task NotifyAssignment(Assignment assignment)
        {
            var students = await _context.Students.Find(x => x.Group.Id == assignment.Group.Id && x.Active).ToListAsync();
            students.ForEach(async student =>
            {
                var user = await _context.Users.Find(x => x.Id == student.UserId).FirstOrDefaultAsync();
                if (!String.IsNullOrEmpty(user.Email))
                {
                    await mailer.SendEmail(user.Email, assignment.Description, "Nueva asignación de " + assignment.Course.Subject.Name + ": " + assignment.Type.Name + " - " + assignment.Title, "Docente: " + assignment.Teacher.Name, user.DisplayName);
                }
                user.NotificationMails.ForEach(async email =>
                {
                    await mailer.SendEmail(email, assignment.Description, "Nueva asignación de " + assignment.Course.Subject.Name + ": " + assignment.Type.Name + " - " + assignment.Title, "Docente: " + assignment.Teacher.Name, user.DisplayName);
                });
            });
        }

        public async Task<IEnumerable<Assignment>> Get(UserInfo user)
        {
            return await _context.Assignments.Find(x => true).ToListAsync();
        }

        public async Task<IEnumerable<Comment>> GetComments(string id)
        {
            return await _context.Comments.Find(x => x.Object.Id == id).ToListAsync();
        }

        public async Task<IEnumerable<Document>> GetDocuments(string id, UserInfo user)
        {
            if (user.Role.Code == (int)RoleEnum.Student || user.Role.Code == (int)RoleEnum.Parent)
            {
                return await _context.Documents.Find(x => x.Assignment.Id == id && x.CreateUser.Id == user.Id).SortByDescending(x => x.CreateDate).ToListAsync();
            }
            return await _context.Documents.Find(x => x.Assignment.Id == id && x.CreateUser.Id != user.Id).SortByDescending(x => x.CreateDate).ToListAsync();
        }

        public async Task<Forum> GetForum(string id)
        {
            return await _context.Forums.Find(x => x.Assignment.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Video>> GetVideos(string id)
        {
            return await _context.Videos.Find(x => x.Assignment.Id == id).ToListAsync();
        }

        public async Task Remove(string id, UserInfo user)
        {
            await _context.Assignments.DeleteOneAsync(x => x.Id == id);
            await _context.Documents.DeleteManyAsync(x => x.Assignment.Id == id && x.CreateUser.Role.Code == (int)RoleEnum.Teacher);
            await _context.Videos.DeleteManyAsync(x => x.Assignment.Id == id);
        }

        public async Task Update(string id, Assignment assignment, UserInfo user)
        {
            var update = Builders<Assignment>.Update
                    .Set(x => x.Title, assignment.Title)
                    .Set(x => x.Type, assignment.Type)
                    .Set(x => x.HasForum, assignment.HasForum)
                    .Set(x => x.UploadFile, assignment.UploadFile)
                    .Set(x => x.UploadVideo, assignment.UploadVideo)
                    .Set(x => x.Description, assignment.Description)
                    .Set(x => x.Course, assignment.Course)
                    .Set(x => x.Group, assignment.Group)
                    .Set(x => x.ContentBlocks, assignment.ContentBlocks)
                    .Set(x => x.StartDate, assignment.StartDate)
                    .Set(x => x.DueDate, assignment.DueDate)
                    .Set(x => x.ModificateDate, DateTime.Now);
            if (assignment.HasForum)
            {
                if (await GetForum(assignment.Id) == null)
                {
                    var forum = new Forum()
                    {
                        Name = assignment.Title,
                        Course = new CourseReference()
                        {
                            Id = assignment.Course.Id,
                            Name = assignment.Course.Name,
                            Subject = assignment.Course.Subject,
                            ParentSubject = assignment.Course.ParentSubject,
                            Color = assignment.Course.Color,
                            Icon = assignment.Course.Icon
                        },
                        Assignment = new Reference()
                        {
                            Id = assignment.Id,
                            Name = assignment.Title
                        }
                    };
                    await _context.Forums.InsertOneAsync(forum);
                };
            }
            await _context.Assignments.UpdateOneAsync<Assignment>(x => x.Id == id, update);
        }
    }

    public interface IAssignmentsService : IContextService<Assignment, Assignment>
    {
        Task<IEnumerable<Comment>> GetComments(string id);
        Task<Forum> GetForum(string id);
        Task<IEnumerable<Document>> GetDocuments(string id, UserInfo user);
        Task<IEnumerable<Video>> GetVideos(string id);
    }
}