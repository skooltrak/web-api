using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using skooltrak_api.Models;
using MongoDB.Driver;
using System.Linq;
using skooltrak_api.Shared;

namespace skooltrak_api.Services
{
  public class StudentsService : IStudentsService
  {
    private readonly IDbContext _context;
    private readonly Mailer mailer;

    public StudentsService(IDbContext context)
    {
      _context = context;
      mailer = new Mailer();
    }

    public async Task<Student> Create(Student student, UserInfo user)
    {
      if (student.Plan != null)
      {
        var plan = await _context.StudyPlans.Find(x => x.Id == student.Plan.Id).FirstOrDefaultAsync();
        if (plan.HasUser && student.Active)
        {
          User studentUser = new User()
          {
            DisplayName = student.FirstName + " " + student.Surname,
            Email = student.Email,
            UserName = await GenerateUsername(student),
            Role = await _context.Roles.Find(x => x.Code == (int)RoleEnum.Student).FirstOrDefaultAsync()
          };
          await _context.Users.InsertOneAsync(studentUser);
          _ = mailer.WelcomeMessage(studentUser);
          student.UserId = studentUser.Id;
        }
        student.CreatedUser = user;
      }

      await _context.Students.InsertOneAsync(student);


      if (!student.Temporary)
      {
        /* SetCharges(student.Id); */
      }
      await SetPreviousGrades(student.Id);
      return student;
    }

    private async Task SetPreviousGrades(string studentId)
    {
      var student = await _context.Students.Find(x => x.Id == studentId).FirstOrDefaultAsync();
      if (student == null)
      {
        return;
      }

      var courses = await _context.Courses.Find(x => x.Plan.Id == student.Plan.Id).ToListAsync();

      courses.ForEach(async course =>
      {
        var grades = await _context.Grades.Find(x => x.Course.Id == course.Id && !x.StudentsGrades.Any(y => y.Student.Id == student.Id)).ToListAsync();
        grades.ForEach(async grade =>
              {
                var item = new GradeStudent()
                {
                  Student = new Reference()
                  {
                    Id = student.Id,
                    Name = student.ShortName
                  },
                  Group = student.Group
                };

                var update = Builders<Grade>
                          .Update.Push(x => x.StudentsGrades, item);
                await _context.Grades.UpdateManyAsync(x => x.Id == grade.Id, update);
                if (grade.Published)
                {
                  var student = new StudentGrade()
                  {
                    Student = item.Student,
                    Comments = item.Comments,
                    Period = grade.Period,
                    Score = item.Score,
                    Course = grade.Course,
                    Grade = new Reference() { Id = grade.Id, Name = grade.Title },
                    Bucket = grade.Bucket,
                    CreateUser = grade.CreateUser
                  };
                  await _context.StudentGrades.InsertOneAsync(student);
                }
              });
      });
    }

    public async Task CreateUsers()
    {
      var students = await _context.Students.Find(x => x.UserId == null).ToListAsync();
      students.ForEach(async student =>
      {
        var plan = await _context.StudyPlans.Find(x => x.Id == student.Plan.Id).FirstOrDefaultAsync();
        if (plan.HasUser)
        {
          User user = new User()
          {
            DisplayName = student.FirstName + " " + student.Surname,
            UserName = await GenerateUsername(student),
            Role = await _context.Roles.Find(x => x.Code == (int)RoleEnum.Student).FirstOrDefaultAsync()
          };
          await _context.Users.InsertOneAsync(user);
          var update = Builders<Student>.Update.Set(x => x.UserId, user.Id);
          await _context.Students.UpdateOneAsync(x => x.Id == student.Id, update);
        }
      });
    }

    public async Task<Student> Get(string id, UserInfo user)
    {
      return await _context.Students.Find(x => x.Id == id).FirstOrDefaultAsync();
    }

    public async Task<IEnumerable<StudentSummary>> Get(UserInfo user)
    {
      return await _context.Students.Find(x => x.Active).Project<StudentSummary>(Builders<Student>.Projection.Expression(item => new StudentSummary()
      {
        Id = item.Id,
        Name = item.Name,
        FullName = item.FullName,
        ShortName = item.ShortName,
        DocumentId = item.DocumentId,
        FirstName = item.FirstName,
        MiddleName = item.MiddleName,
        Surname = item.Surname,
        SecondSurname = item.SecondSurname,
        Gender = item.Gender != null ? item.Gender.Name : "",
        Age = Util.Age(item.BirthDate),
        Group = item.Group,
        Plan = item.Plan != null ? new Reference() { Id = item.Plan.Id, Name = item.Plan.Name } : null,
      })).SortBy(x => x.Surname)
          .ThenBy(x => x.SecondSurname)
          .ThenBy(x => x.FirstName)
          .ThenBy(x => x.MiddleName).ToListAsync();
    }

    public async Task<IEnumerable<Charge>> GetCharges(string id)
    {
      return await _context.Charges.Find(x => x.Student.Id == id).SortBy(x => x.DueDate).ToListAsync();
    }

    public IEnumerable<object> GetCountByPlan()
    {
      var aggregate = _context.Students.AsQueryable().GroupBy(x => x.Plan).
      Select(g => new { g.Key, count = g.Count() }).OrderBy(a => a.Key).ToList();
      return aggregate;
    }

    public double GetTotalCharges(string id)
    {
      return _context.Charges.AsQueryable()
          .Where(x => x.Balance > 0 && (x.DueDate < DateTime.Now || x.StartDate < DateTime.Now) && x.Student.Id == id)
          .GroupBy(x => x.Student)
          .Select(x => x.Sum(y => y.Balance))
          .FirstOrDefault();
    }

    public bool IsDefault(string id)
    {
      return _context.Charges.Find(x => x.Student.Id == id && x.Balance > 0 && x.DueDate < DateTime.Now).Any();
    }

    public double GetCurrentCharges(string id)
    {
      return _context.Charges.AsQueryable()
          .Where(x => x.Balance > 0 && (x.DueDate > DateTime.Now && x.StartDate < DateTime.Now) && x.Student.Id == id)
          .GroupBy(x => x.Student)
          .Select(x => x.Sum(y => y.Balance))
          .FirstOrDefault();
    }

    public double GetScore(string courseId, string studentId, string periodId)
    {
      var course = _context.Courses.Find(x => x.Id == courseId).FirstOrDefault();
      double totalWeighting = 0;
      double score = 0;
      List<StudentGrade> grades;
      if (periodId == "undefined" || periodId == null)
      {
        grades = _context.StudentGrades.Find(x => x.Student.Id == studentId && x.Course.Id == courseId).ToList();
      }
      else
      {
        grades = _context.StudentGrades.Find(x => x.Student.Id == studentId && x.Course.Id == courseId && x.Period.Id == periodId).ToList();
      }
      if (grades.Any(x => x.Bucket.Id == 1))
      {
        totalWeighting = totalWeighting + course.Buckets.Find(x => x.Id == 1).Weighting;
      }
      if (grades.Any(x => x.Bucket.Id == 2))
      {
        totalWeighting = totalWeighting + course.Buckets.Find(x => x.Id == 2).Weighting;
      }
      if (grades.Any(x => x.Bucket.Id == 3))
      {
        totalWeighting = totalWeighting + course.Buckets.Find(x => x.Id == 3).Weighting;
      }
      if (grades.Count > 0)
      {
        course.Buckets.ForEach(bucket =>
        {
          var current = grades.Where(x => x.Bucket.Id == bucket.Id).Select(x => x.Score).ToList();
          if (current.Count > 0)
          {
            var weighting = current.Average();
            score = score + (weighting * (bucket.Weighting / totalWeighting));
          }
        });
      }
      return TruncateDecimal(score, 1);
    }

    public async Task<IEnumerable<Course>> GetCourses(string id, string periodId)
    {
      var student = await _context.Students.Find(x => x.Id == id).FirstOrDefaultAsync();
      var periods = await _context.Periods.Find(x => true).ToListAsync();
      if (student.Temporary)
      {
        var ids = student.Courses.Select(x => x.Id).ToArray();
        return await _context.Courses.Find(x => ids.Contains(x.Id)).ToListAsync();
      }
      var courses = await _context.Courses.Find(x => x.Plan.Id == student.Plan.Id && x.Active).SortBy(x => x.Subject.Name).ToListAsync();

      courses.ForEach(course =>
          {
            if (periodId != null && periodId != "undefined")
            {
              course.CurrentScore = GetScore(course.Id, id, periodId);
            }
            else
            {
              var scores = new List<double>();
              periods.ForEach(period =>
                    {
                      var score = GetScore(course.Id, id, period.Id);
                      if (score > 0)
                      {
                        scores.Add(score);
                      }
                    });
              if (scores.Count > 0)
              {
                course.CurrentScore = TruncateDecimal(scores.Average(), 1);
              }
            }
          });
      return courses;
    }

    public async Task<IEnumerable<Forum>> GetForums(string id)
    {
      var student = await _context.Students.Find(x => x.Id == id).FirstOrDefaultAsync();
      var plan = await _context.StudyPlans.Find(x => x.Id == student.Plan.Id).FirstOrDefaultAsync();
      var courses = await _context.Courses.Find(x => x.Plan.Id == plan.Id).ToListAsync();
      var ids = courses.Select(x => x.Id).ToArray();
      return await _context.Forums.Find(x => ids.Contains(x.Course.Id)).SortByDescending(x => x.LastPost).ToListAsync();
    }

    public async Task<IEnumerable<Payment>> GetPayments(string id)
    {
      return await _context.Payments.Find(x => x.Student.Id == id).SortByDescending(x => x.PaymentDate).ToListAsync();
    }

    public async Task<StudyPlan> GetPlan(string id)
    {
      var student = await _context.Students.Find(x => x.Id == id).FirstOrDefaultAsync();
      return await _context.StudyPlans.Find(x => x.Id == student.Plan.Id).FirstOrDefaultAsync();
    }

    public async Task<Student> GetStudent(string code)
    {
      return await _context.Students.Find(x => x.Code == code).FirstOrDefaultAsync();
    }

    public async Task Remove(string id, UserInfo user)
    {
      var student = await _context.Students.Find(x => x.Id == id).FirstOrDefaultAsync();
      await _context.Charges.DeleteManyAsync(x => x.Student.Id == id);
      await _context.Users.DeleteOneAsync(x => x.Id == student.UserId);
      var update = Builders<Student>.Update
                  .Set(x => x.Active, false)
                  .Set(x => x.Group, null)
                  .Set(x => x.Plan, null)
                  .Set(x => x.UserId, null);
      await _context.Students.UpdateOneAsync(x => x.Id == id, update);
    }

    public void SetCharges(string id)
    {
      var student = _context.Students.Find(x => x.Id == id).FirstOrDefault();
      var plan = _context.StudyPlans.Find(x => x.Id == student.Plan.Id).FirstOrDefault();
      plan.EnrollCharges.ForEach(cost =>
      {
        var existing = _context.Charges.Find(x => x.Student.Id == student.Id && x.Description == cost.Description).FirstOrDefault();
        if (existing == null)
        {
          var charge = new Charge()
          {
            Student = new Reference() { Id = student.Id, Name = student.FullName },
            Description = cost.Description,
            StartDate = new DateTime(DateTime.Now.Year, 1, 15),
            DueDate = new DateTime(DateTime.Now.Year, 1, 15),
            Amount = cost.Cost,
            Balance = cost.Cost
          };
          _context.Charges.InsertOne(charge);
        }

      });

      var paymentDays = _context.PaymentDays.Find(x => true).ToList();
      paymentDays.ForEach(day =>
      {
        var existing = _context.Charges.Find(x => x.Student.Id == student.Id && x.MonthlyFee.Id == day.Id).FirstOrDefault();
        if (existing == null)
        {
          var charge = new Charge()
          {
            MonthlyFee = new Reference() { Id = day.Id, Name = day.Title },
            Student = new Reference() { Id = student.Id, Name = student.FullName },
            Description = day.Title,
            StartDate = day.StartDate,
            DueDate = day.DueDate,
            Amount = plan.MonthlyCost,
            Balance = plan.MonthlyCost
          };
          _context.Charges.InsertOne(charge);
        }
      });
    }

    public async Task Update(string id, Student student, UserInfo user)
    {
      var current = await _context.Students.Find(x => x.Id == id).FirstOrDefaultAsync();
      student.UserId = current.UserId;
      if (current.UserId == null && student.Active)
      {
        User studentUser = new User()
        {
          DisplayName = student.FirstName + " " + student.Surname,
          Email = student.Email,
          UserName = await GenerateUsername(student),
          Role = await _context.Roles.Find(x => x.Code == (int)RoleEnum.Student).FirstOrDefaultAsync()
        };
        await _context.Users.InsertOneAsync(studentUser);
        _ = mailer.WelcomeMessage(studentUser);
        student.UserId = studentUser.Id;
      }

      var update = Builders<Student>.Update
          .Set(x => x.FirstName, student.FirstName)
          .Set(x => x.MiddleName, student.MiddleName)
          .Set(x => x.Surname, student.Surname)
          .Set(x => x.SecondSurname, student.SecondSurname)
          .Set(x => x.Courses, student.Courses)
          .Set(x => x.Email, student.Email)
          .Set(x => x.SchoolId, student.SchoolId)
          .Set(x => x.BirthDate, student.BirthDate)
          .Set(x => x.Address, student.Address)
          .Set(x => x.Guardians, student.Guardians)
          .Set(x => x.Plan, student.Plan)
          .Set(x => x.Group, student.Group)
          .Set(x => x.Gender, student.Gender)
          .Set(x => x.MedicalInfo, student.MedicalInfo)
          .Set(x => x.Mother, student.Mother)
          .Set(x => x.Father, student.Father)
          .Set(x => x.EnrollDate, student.EnrollDate)
          .Set(x => x.BirthCountry, student.BirthCountry)
          .Set(x => x.Province, student.Province)
          .Set(x => x.DocumentId, student.DocumentId)
          .Set(x => x.AvatarURL, student.AvatarURL)
          .Set(x => x.ModificateDate, DateTime.Now)
          .Set(x => x.Active, student.Active)
          .Set(x => x.UserId, student.UserId);

      await _context.Students.UpdateOneAsync<Student>(x => x.Id == id, update);
    }

    private async Task<string> GenerateUsername(Student student)
    {
      var userName = student.FirstName.ToLower().Trim() + "." + student.Surname.ToLower().Trim();
      userName.Replace(" ", "");
      var existing = await _context.Users.Find(x => x.UserName == userName).FirstOrDefaultAsync();
      if (existing == null)
      {
        return userName;
      }
      userName = student.FirstName.ToLower().Trim() + student.MiddleName.ToLower().Trim() + "." + student.Surname.ToLower().Trim();
      userName.Replace(" ", "");
      return userName;
    }

    public async Task<bool> ValidDocumentId(string documentId)
    {
      documentId = documentId.Trim();
      var count = await _context.Students.Find(x => x.DocumentId == documentId).CountDocumentsAsync();
      if (count > 0)
      {
        return false;
      }
      return true;
    }

    public async Task<bool> ValidDocumentId(string studentId, string documentId)
    {
      documentId = documentId.Trim();
      var count = await _context.Students.Find(x => x.DocumentId == documentId && x.Id != studentId).CountDocumentsAsync();
      if (count > 0)
      {
        return false;
      }
      return true;
    }

    public async Task<long> GetCount()
    {
      return await _context.Students.CountDocumentsAsync(x => x.Active);
    }

    public async Task<IEnumerable<StudentAttendance>> GetAttendances(string id)
    {
      return await _context.Attendance.Find(x => x.Students.Any(y => y.Id == id))
          .Project<StudentAttendance>(Builders<Attendance>.Projection.Expression(x => x.Students.Find(y => y.Id == id)))
          .ToListAsync();
    }

    public async Task<Student> GetStudentByDocument(string documentId)
    {
      return await _context.Students.Find(x => x.DocumentId == documentId).FirstOrDefaultAsync();
    }

    public async Task<IEnumerable<Assignment>> GetAssignments(string id, string DateFrom = null, string DateTo = null)
    {
      var student = await _context.Students.Find(x => x.Id == id).FirstOrDefaultAsync();
      if (DateFrom != null && DateTo != null)
      {
        DateTime from = DateTime.ParseExact(DateFrom, "dd-MM-yyyy", null);
        DateTime to = DateTime.ParseExact(DateTo, "dd-MM-yyyy", null);
        return await _context.Assignments.Find(x => x.Group.Id == student.Group.Id && ((x.StartDate >= from && x.StartDate <= to) || (x.DueDate >= from && x.DueDate <= to))).ToListAsync();
      }
      return await _context.Assignments.Find(x => x.Group.Id == student.Group.Id).ToListAsync();
    }

    public async Task<IEnumerable<Assignment>> GetAssignmentsRange(string id, DateTime startDate, DateTime endDate)
    {
      var student = await _context.Students.Find(x => x.Id == id).FirstOrDefaultAsync();

      return await _context.Assignments.Find(x => x.Group.Id == student.Group.Id).ToListAsync();
    }

    public async Task<IEnumerable<QuizResult>> GetResults(string id)
    {
      return await _context.QuizResults.Find(x => x.Student.Id == id && x.Status == (int)QuizStatusEnum.Completed && DateTime.Now > x.EndDate).ToListAsync();
    }

    public async Task<IEnumerable<ExamResult>> GetExamResults(string id)
    {
      return await _context.ExamResults.Find(x => x.Student.Id == id && x.Status == (int)QuizStatusEnum.Revisited && DateTime.Now > x.EndDate).ToListAsync();
    }

    public async Task<IEnumerable<Teacher>> GetTeachers(string id)
    {
      var student = await _context.Students.Find(x => x.Id == id).FirstOrDefaultAsync();
      var courses = await _context.Courses.Find(x => x.Plan.Id == student.Plan.Id).ToListAsync();
      var teachersId = courses.SelectMany(x => x.Teachers.Select(y => y.Id)).ToArray();
      return await _context.Teachers.Find(x => teachersId.Contains(x.Id)).ToListAsync();
    }

    public async Task<IEnumerable<Activity>> GetActivities(string id, UserInfo user)
    {
      var student = await _context.Students.Find(x => x.Id == id).FirstOrDefaultAsync();
      string[] ids;
      if (student.Temporary)
      {
        ids = student.Courses.Select(x => x.Id).ToArray();
      }
      else
      {
        var courses = await _context.Courses.Find(x => x.Plan.Id == student.Plan.Id).ToListAsync();
        ids = courses.Select(x => x.Id).ToArray();
      }

      return await _context.Activities.Find(x => ids.Contains(x.Course.Id) && (x.Private == false || x.CreatedBy.Id == user.Id)).SortByDescending(x => x.CreateDate).ToListAsync();
    }

    public async Task<IEnumerable<QuizResult>> GetQuizes(string id)
    {
      return await _context.QuizResults.Find(x => x.Student.Id == id && (x.Status == (int)QuizStatusEnum.Pending || x.Status == (int)QuizStatusEnum.Incomplete) && (DateTime.Now >= x.StartDate && DateTime.Now <= x.EndDate)).ToListAsync();
    }

    public async Task<IEnumerable<ExamResult>> GetExams(string id)
    {
      return await _context.ExamResults.Find(x => x.Student.Id == id && x.Status == (int)QuizStatusEnum.Pending && (DateTime.Now >= x.StartDate && DateTime.Now <= x.EndDate)).ToListAsync();
    }

    public async Task<IEnumerable<Student>> GetTemporaryStudents()
    {
      return await _context.Students.Find(x => x.Temporary).ToListAsync();
    }

    public async Task<IEnumerable<StudentGrade>> GetGrades(string id)
    {
      return await _context.StudentGrades.Find(x => x.Student.Id == id).ToListAsync();
    }

    public async Task<IEnumerable<StudentGrade>> GetGradesByCourse(string id, string courseId, string periodId)
    {
      return await _context.StudentGrades.Find(x => x.Student.Id == id && x.Course.Id == courseId && x.Period.Id == periodId).SortBy(x => x.CreateDate).ToListAsync();
    }

    public async Task<IEnumerable<Document>> GetDocuments(string studentId)
    {
      return await _context.Documents.Find(x => x.Student.Id == studentId).SortByDescending(x => x.CreateDate).ToListAsync();
    }

    public async Task SetAllCharges()
    {
      var students = await _context.Students.Find(x => x.Active).ToListAsync();
      students.ForEach(student =>
      {
        SetCharges(student.Id);
      });
    }

    public async Task ClosePeriod(Course course)
    {
      var newPeriod = await _context.Periods.Find(x => x.Sort == course.CurrentPeriod.Sort + 1).FirstOrDefaultAsync();
      var update = Builders<Grade>.Update.Set(x => x.Period, course.CurrentPeriod);
      var studentGrades = Builders<StudentGrade>.Update.Set(x => x.Period, course.CurrentPeriod);
      await _context.Grades.UpdateManyAsync(x => x.Period == null && x.Course.Id == course.Id, update);
      await _context.StudentGrades.UpdateManyAsync(x => x.Period == null && x.Course.Id == course.Id, studentGrades);
      if (newPeriod != null)
      {
        var updateCourse = Builders<Course>.Update.Set(x => x.CurrentPeriod, newPeriod);
      }
    }

    public async Task<double> GetCourseCurrentScore(string id, string courseId)
    {
      var periods = await _context.Periods.Find(x => true).ToListAsync();
      var scores = new List<double>();
      periods.ForEach(period =>
      {
        var score = (GetScore(id, period.Id, courseId));
        if (score > 0)
        {
          scores.Add(score);
        }
      });
      return TruncateDecimal(scores.Average(), 1);
    }

    public double GetCoursePeriodScore(string id, string periodId, string courseId)
    {
      var student = _context.Students.Find(x => x.Id == id).FirstOrDefault();
      var courses = _context.Courses.Find(x => x.Plan.Id == student.Plan.Id && x.Active).ToList();
      var scores = new List<double>();
      courses.ForEach(course =>
      {
        var score = GetScore(course.Id, id, periodId);
        if (score > 0)
        {
          scores.Add(score);
        }
      });
      if (scores.Count > 0)
      {
        return TruncateDecimal(scores.Average(), 1);
      }
      return 0;
    }

    public async Task<double> GetCurrentScore(string id)
    {
      var periods = await _context.Periods.Find(x => true).ToListAsync();
      var scores = new List<double>();
      periods.ForEach(period =>
      {
        var score = (GetPeriodScore(id, period.Id));
        if (score > 0)
        {
          scores.Add(score);
        }
      });
      if (scores.Count > 0)
      {
        return TruncateDecimal(scores.Average(), 2);
      }
      return 0;
    }

    public double GetPeriodScore(string id, string periodId)
    {
      var student = _context.Students.Find(x => x.Id == id).FirstOrDefault();
      var courses = _context.Courses.Find(x => x.Plan.Id == student.Plan.Id && x.Active && x.ParentSubject == null).ToList();
      var scores = new List<double>();
      courses.ForEach(course =>
      {
        var score = GetScore(course.Id, id, periodId);
        if (score > 0)
        {
          scores.Add(score);
        }
      });
      var parents = GetParentSubject(id, periodId);
      parents.ForEach(parent =>
      {
        if (parent.Score > 0)
        {
          scores.Add(parent.Score);
        }
      });
      if (scores.Count > 0)
      {
        return TruncateDecimal(scores.Average(), 2);
      }
      return 0;
    }

    public List<ParentSubject> GetParentSubject(string id, string periodId)
    {
      var subjects = new List<ParentSubject>();
      var student = _context.Students.Find(x => x.Id == id).FirstOrDefault();
      var courses = _context.Courses.Find(x => x.Plan.Id == student.Plan.Id && x.ParentSubject != null && x.Active).SortBy(x => x.ParentSubject.Name).ToList();
      courses.ForEach(course =>
      {
        if (subjects.Any(x => x.Id == course.ParentSubject.Id))
        {
          var subject = subjects.Where(x => x.Id == course.ParentSubject.Id).FirstOrDefault();
          course.CurrentScore = GetScore(course.Id, id, periodId);
          subject.Courses.Add(course);
        }
        else
        {
          var subject = new ParentSubject()
          {
            Id = course.ParentSubject.Id,
            Name = course.ParentSubject.Name,
            Courses = new List<Course>()
          };

          course.CurrentScore = GetScore(course.Id, id, periodId);
          subject.Courses.Add(course);
          subjects.Add(subject);
        }
      });
      subjects.ForEach(subject =>
      {
        var scores = subject.Courses.Where(x => x.CurrentScore > 0).Select(x => x.CurrentScore).ToList();
        if (scores.Count > 0)
        {
          subject.Score = TruncateDecimal(scores.Average(), 1);
        }
        else
        {
          subject.Score = 0;
        }

      });
      return subjects;
    }

    public async Task<GradeSummary> GetGradeSummary(string id, Period period)
    {
      var summary = new GradeSummary();
      summary.StudentId = id;
      var student = _context.Students.Find(x => x.Id == id).FirstOrDefault();
      var courses = _context.Courses.Find(x => x.Plan.Id == student.Plan.Id && x.Active && x.ParentSubject == null).ToList();
      var parents = _context.Courses.Find(x => x.Plan.Id == student.Plan.Id && x.Active && x.ParentSubject != null).ToList();
      var periods = await _context.Periods.Find(x => x.Sort <= period.Sort).ToListAsync();
      courses.ForEach(course =>
      {
        var item = new CourseGrade();
        item.Course = new Reference()
        {
          Id = course.Id,
          Name = course.Subject.Name
        };
        periods.ForEach(period =>
              {
                var x = new GradeItem();
                x.Period = period;
                x.Score = GetScore(course.Id, id, period.Id);
                item.Grades.Add(x);
              });
        summary.Courses.Add(item);
      });
      courses = _context.Courses.Find(x => x.Plan.Id == student.Plan.Id && x.ParentSubject != null && x.Active).SortBy(x => x.ParentSubject.Name).ToList();
      courses.ForEach(course =>
      {
        if (summary.Courses.Any(x => x.Course.Id == course.ParentSubject.Id))
        {
          var item = new CourseGrade();
          item.Course = new Reference()
          {
            Id = course.Id,
            Name = course.Subject.Name
          };
          var subject = summary.Courses.Where(x => x.Course.Id == course.ParentSubject.Id).FirstOrDefault();
          periods.ForEach(period =>
                {
                  var x = new GradeItem();
                  x.Period = period;
                  x.Score = GetScore(course.Id, id, period.Id);
                  item.Grades.Add(x);

                  var attendance = new CourseAttendance();
                  var currentAttendance = _context.Attendance.Find(x => x.Course.Id == course.Id && x.Period.Id == period.Id && x.Group.Id == student.Group.Id).ToList();
                });
          subject.Children.Add(item);
          subject.Grades = new List<GradeItem>();
          int i = 0;
          periods.ForEach(period =>
                {
                  var scores = subject.Children;
                  var x = new GradeItem();
                  x.Period = period;
                  x.Score =
                        i++;
                });
        }
        else
        {
          var subject = new CourseGrade();
          subject.Children = new List<CourseGrade>();
          subject.Course = new Reference()
          {
            Id = course.ParentSubject.Id,
            Name = course.ParentSubject.Name
          };
          var item = new CourseGrade();
          item.Course = new Reference()
          {
            Id = course.Id,
            Name = course.Subject.Name
          };
          periods.ForEach(period =>
                {
                  var x = new GradeItem();
                  x.Period = period;
                  x.Score = GetScore(course.Id, id, period.Id);
                  item.Grades.Add(x);
                });
          subject.Children.Add(item);
          summary.Courses.Add(subject);
        }
      });
      summary.Courses.ForEach(item =>
      {
        if (item.Children != null)
        {
          periods.ForEach(period =>
                {
                  var subjects = GetParentSubject(id, period.Id);
                  subjects.ForEach(subject =>
                        {
                          if (item.Course.Id == subject.Id)
                          {
                            var x = new GradeItem();
                            x.Period = period;
                            x.Score = subject.Score;
                            item.Grades.Add(x);
                          }
                        });

                });
        }
      });
      return summary;
    }
    public async Task<IEnumerable<Student>> GetStudents()
    {
      return await _context.Students.Find(x => true).SortBy(x => x.Surname).ThenBy(x => x.FirstName).ToListAsync();
    }

    public void GetAttendance()
    {
      return;
    }

    public async Task<IEnumerable<ClassDay>> GetSchedule(string id)
    {
      var student = await _context.Students.Find(x => x.Id == id).FirstOrDefaultAsync();
      return (await _context.Groups.Find(x => x.Id == student.Group.Id).FirstOrDefaultAsync()).Schedule;
    }

    private double TruncateDecimal(double value, int precision)
    {
      double step = Math.Pow(10, precision);
      double tmp = Math.Truncate(step * value);
      return tmp / step;
    }

    public async Task<List<StudentPerformance>> GetPerformance(string id)
    {
      var student = await _context.Students.Find(x => x.Id == id).FirstOrDefaultAsync();
      var periods = await _context.Periods.Find(x => true).ToListAsync();
      var courses = await _context.Courses.Find(x => x.Plan.Id == student.Plan.Id && x.Active).SortBy(x => x.Subject.Name).ToListAsync();
      var performance = new List<StudentPerformance>();
      periods.ForEach(period =>
      {
        var item = new StudentPerformance();
        item.Period = period;
        courses.ForEach(course =>
              {
                var detail = new PerformanceDetail();
                detail.Course = new CourseReference()
                {
                  Id = course.Id,
                  Subject = course.Subject,
                  ParentSubject = course.ParentSubject,
                  Color = course.Color,
                  Icon = course.Icon
                };
                detail.Grade = GetScore(course.Id, id, period.Id);
                item.Grades.Add(detail);
              });
        performance.Add(item);
      });
      return performance;
    }

    public async Task SetSection()
    {
      var students = await _context.Students.Find(x => x.Active).ToListAsync();

      var plans = await _context.StudyPlans.Find(x => true).ToListAsync();
      plans.ForEach(async plan =>
      {
        var filter = Builders<Student>.Filter.Where(x => x.Plan.Id == plan.Id);
        var update = Builders<Student>.Update.Set(x => x.Section, plan.Degree);
        await _context.Students.UpdateManyAsync(filter, update);
      });

    }

    public async Task<List<StudentSkill>> GetSkills(string id)
    {
      return await _context.StudentSkills.Find(x => x.Student.Id == id).ToListAsync();
    }

    public async Task SetSkill(string id, StudentSkill skill)
    {
      if (skill.Id == null)
      {
        await _context.StudentSkills.InsertOneAsync(skill);
      }
      else
      {
        await _context.StudentSkills.ReplaceOneAsync(x => x.Id == skill.Id, skill);
      }
    }

    public async Task<List<PreScholarEvaluation>> GetEvaluations(string id)
    {
      return await _context.PreScholarEvaluations.Find(x => x.Student.Id == id).ToListAsync();
    }

    public async Task SetEvaluation(string id, PreScholarEvaluation item)
    {
      if (item.Id == null)
      {
        await _context.PreScholarEvaluations.InsertOneAsync(item);
      }
      else
      {
        await _context.PreScholarEvaluations.ReplaceOneAsync(x => x.Id == item.Id, item);
      }
    }

    public async Task SetPlans()
    {
      var plans = await _context.StudyPlans.Find(x => true).ToListAsync();
      plans.ForEach(async plan =>
      {
        var filter = Builders<Student>.Filter.Where(x => x.Plan.Id == plan.Id);
        var update = Builders<Student>.Update.Set(x => x.Plan, plan);
        await _context.Students.UpdateManyAsync(filter, update);
      });
    }

    public async Task<IEnumerable<ArchiveGrade>> GetArchiveGrades(string id, int year)
    {
      return await _context.ArchiveGrades.Find(x => x.Student.Id == id && x.Year == year).ToListAsync();
    }

    public async Task<IEnumerable<int>> GetArchiveYears(string id)
    {
      return await _context.ArchiveGrades.Distinct(x => x.Year, x => x.Student.Id == id).ToListAsync();
    }

    public async Task<IEnumerable<Student>> GetInactiveStudents()
    {
      return await _context.Students.Find(x => !x.Active).SortBy(x => x.Surname).ThenBy(x => x.FirstName).ToListAsync();
    }

    public async Task FixStudentsGroup()
    {
      var update = Builders<Student>.Update
          .Unset(x => x.Plan);
      var updateMessages = Builders<Message>.Update
          .Unset("Receivers.$[].Group");
      await _context.Students.UpdateManyAsync(x => true, update);
      await _context.Messages.UpdateManyAsync(x => true, updateMessages);
    }

    public async Task<IEnumerable<Student>> GetAllStudents(UserInfo user)
    {
      return await _context.Students.Find(x => true).SortBy(x => x.Surname).ThenBy(x => x.FirstName).ToListAsync();
    }
  }

  public interface IStudentsService : IContextService<Student, StudentSummary>
  {
    Task<IEnumerable<Student>> GetAllStudents(UserInfo user);
    Task<GradeSummary> GetGradeSummary(string id, Period period);
    Task<IEnumerable<Student>> GetInactiveStudents();
    Task<List<StudentSkill>> GetSkills(string id);
    Task SetSkill(string id, StudentSkill skill);
    Task<List<PreScholarEvaluation>> GetEvaluations(string id);
    Task SetEvaluation(string id, PreScholarEvaluation item);
    Task<List<StudentPerformance>> GetPerformance(string id);
    Task<IEnumerable<ClassDay>> GetSchedule(string id);
    Task<IEnumerable<Student>> GetStudents();
    Task<IEnumerable<Student>> GetTemporaryStudents();
    List<ParentSubject> GetParentSubject(string id, string periodId);
    IEnumerable<object> GetCountByPlan();
    Task<IEnumerable<Activity>> GetActivities(string id, UserInfo user);
    Task<IEnumerable<ArchiveGrade>> GetArchiveGrades(string id, int year);
    Task<IEnumerable<int>> GetArchiveYears(string id);
    Task ClosePeriod(Course course);
    Task<long> GetCount();
    Task<bool> ValidDocumentId(string documentId);
    Task<IEnumerable<Document>> GetDocuments(string studentId);
    Task<Student> GetStudentByDocument(string documentId);
    Task<IEnumerable<Teacher>> GetTeachers(string id);
    Task<bool> ValidDocumentId(string studentId, string documentId);
    Task<IEnumerable<Course>> GetCourses(string id, string periodId);
    Task<IEnumerable<QuizResult>> GetQuizes(string id);
    Task<IEnumerable<ExamResult>> GetExams(string id);
    Task<IEnumerable<Charge>> GetCharges(string id);
    Task<IEnumerable<Forum>> GetForums(string id);
    Task<IEnumerable<QuizResult>> GetResults(string id);
    Task<IEnumerable<ExamResult>> GetExamResults(string id);
    Task<IEnumerable<Payment>> GetPayments(string id);
    Task<IEnumerable<Assignment>> GetAssignments(string id, string DateFrom = null, string DateTo = null);
    Task<IEnumerable<Assignment>> GetAssignmentsRange(string id, DateTime startDate, DateTime endDate);
    Task<IEnumerable<StudentAttendance>> GetAttendances(string id);
    Task<IEnumerable<StudentGrade>> GetGrades(string id);
    Task<IEnumerable<StudentGrade>> GetGradesByCourse(string id, string courseId, string periodId);
    Task<double> GetCurrentScore(string id);
    double GetPeriodScore(string id, string periodId);
    void SetCharges(string id);
    Task SetAllCharges();
    Task SetPlans();
    Task SetSection();
    Task<Student> GetStudent(string code);
    Task<StudyPlan> GetPlan(string id);
    Task CreateUsers();

    Task FixStudentsGroup();
  }
}