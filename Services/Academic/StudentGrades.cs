using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class StudentGradesService : IStudentGradesService
	{
		private readonly IDbContext _context;
		public StudentGradesService(IDbContext context)
		{
			_context = context;
		}

		public async Task<StudentGrade> Create(StudentGrade grade, UserInfo user)
		{
			await _context.StudentGrades.InsertOneAsync(grade);
			return grade;
		}

		public async Task<IEnumerable<StudentGrade>> Get(UserInfo user)
		{
			return await _context.StudentGrades.Find(x => true).ToListAsync();
		}

		public async Task<StudentGrade> Get(string id, UserInfo user)
		{
			return await _context.StudentGrades.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.StudentGrades.DeleteOneAsync(x => x.Id == id);
		}

		public async Task Update(string id, StudentGrade grade, UserInfo user)
		{
			grade.ModificateDate = DateTime.Now;
			await _context.StudentGrades.ReplaceOneAsync(x => x.Id == id, grade);

		}
	}

	public interface IStudentGradesService : IContextService<StudentGrade, StudentGrade> { }
}