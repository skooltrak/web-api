using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class AttendanceService : IAttendanceService
	{
		private readonly IDbContext _context;

		public AttendanceService(IDbContext context)
		{
			_context = context;
		}

		public async Task<Attendance> Create(Attendance attendance, UserInfo user)
		{
			attendance.CreateUser = user;
			await _context.Attendance.InsertOneAsync(attendance);
			return attendance;
		}

		public async Task<IEnumerable<Attendance>> Get(UserInfo user)
		{
			return await _context.Attendance.Find(x => true).ToListAsync();
		}

		public async Task<Attendance> Get(string id, UserInfo user)
		{
			return await _context.Attendance.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.Attendance.DeleteOneAsync(x => x.Id == id);
		}

		public async Task Update(string id, Attendance attendance, UserInfo user)
		{
			attendance.ModificateDate = DateTime.Now;
			await _context.Attendance.ReplaceOneAsync(x => x.Id == id, attendance);
		}
	}

	public interface IAttendanceService : IContextService<Attendance, Attendance> { }
}