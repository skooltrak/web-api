using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using skooltrak_api.Models;
using MongoDB.Driver;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver.GridFS;

namespace skooltrak_api.Services
{
  public class SchoolsService : ISchoolsService
  {

    private readonly IDbContext _context;
    private readonly StudentsService _studentsService;
    public SchoolsService(IDbContext context)
    {
      _context = context;
      _studentsService = new StudentsService(context);
    }

    public async Task<IEnumerable<School>> Get(UserInfo user)
    {
      return await _context.Schools.Find(x => true).ToListAsync();
    }

    public async Task<School> Get(string id, UserInfo user)
    {
      return await _context.Schools.Find(x => x.Id == id).FirstOrDefaultAsync();
    }

    public async Task<School> Create(School school, UserInfo user)
    {
      await _context.Schools.InsertOneAsync(school);
      return school;
    }

    public async Task Update(string id, School school, UserInfo user)
    {
      var update = Builders<School>.Update
          .Set(x => x.Name, school.Name)
          .Set(x => x.CurrentYear, school.CurrentYear)
          .Set(x => x.Motto, school.Motto)
          .Set(x => x.ShortName, school.ShortName)
          .Set(x => x.Website, school.Website)
          .Set(x => x.Address, school.Address)
          .Set(x => x.Contacts, school.Contacts)
          .Set(x => x.LogoURL, school.LogoURL)
          .Set(x => x.ModificateDate, DateTime.Now);
      await _context.Schools.UpdateOneAsync(x => x.Id == school.Id, update);
    }

    public async Task Remove(string id, UserInfo user)
    {
      await _context.Schools.DeleteOneAsync(x => x.Id == id);
    }

    public async Task<IEnumerable<Student>> GetStudents(string id)
    {
      return await _context.Students.Find(x => x.SchoolId == id).ToListAsync();
    }

    public async Task<IEnumerable<Teacher>> GetTeachers(string id)
    {
      return await _context.Teachers.Find(x => x.SchoolId == id).ToListAsync();
    }

    public async Task<School> GetDefaultSchool()
    {
      return await _context.Schools.Find(x => true).FirstOrDefaultAsync();
    }

    public async Task SetStudents(string id)
    {
      var update = Builders<Student>.Update.Set(x => x.SchoolId, id);
      await _context.Students.UpdateManyAsync(x => true, update);
    }

    public void CloseYear(string id)
    {
      /* var students = _context.Students.Find(x => x.SchoolId == id).ToList();
      var school = _context.Schools.Find(x => x.Id == id).FirstOrDefault();
      students.ForEach(student =>
      {
        var periods = _context.Periods.Find(x => true).ToList();
        periods.ForEach(period =>
        {
          var grades = _context.StudentGrades
                .Find(x => x.Student.Id == student.Id
                    && x.Period.Id == period.Id)
                .ToList();
          var archive = new ArchiveGrade()
          {
            Student = new Reference()
            {
              Id = student.Id,
              Name = student.FullName
            },
            Year = school.CurrentYear,
            Plan = student.Plan,
            Group = (grades.Count > 0 ? grades.First().Group : null),
            School = new Reference()
            {
              Id = school.Id,
              Name = school.Name
            },
            Period = period,

            Grades = grades.Select(x => new GradeRedux()
            {
              Grade = x.Grade,
              Bucket = x.Bucket,
              Course = x.Course,
              Comments = x.Comments,
              Score = x.Score,
              CreateDate = x.CreateDate,
              ModificateDate = x.ModificateDate
            })
          };
      _context.ArchiveGrades.InsertOne(archive);
        });
      }); */
      // SaveCredits(2021, id);
      var updateSchool = Builders<School>.Update.Inc(x => x.CurrentYear, 1);
      _context.Schools.UpdateOne(x => x.Id == id, updateSchool);
      _context.Assignments.DeleteManyAsync(x => true);
      _context.Documents.DeleteManyAsync(x => x.Student != null);
      var usersFilter = Builders<User>.Filter.Where(x => x.Role.Code == (int)RoleEnum.Student);
      var usersUpdate = Builders<User>.Update.Set(x => x.Blocked, true);

      _context.Users.UpdateManyAsync(usersFilter, usersUpdate);
      var filter = Builders<Student>.Filter.Where(x => x.SchoolId == id);
      var update = Builders<Student>.Update.Set(x => x.Active, false).Set(x => x.Group, null);
      // ChangeLevel(school.CurrentYear + 1);

      /* DeleteFiles(); */
    }


    public void SaveStudentCredits(int year, string schoolId, string studentId)
    {
      var student = _context.Students.Find(x => x.Id == studentId).FirstOrDefault();
      var periods = _context.Periods.Find(x => true).ToList();

      if (student.Plan != null)
      {
        var courses = _context.Courses.Find(x => x.Plan.Id == student.Plan.Id && x.Active).ToList();
        courses.ForEach(course =>
        {
          periods.ForEach(period =>
          {
            var existing = _context.Credits.Find(x =>
              x.DocumentId == student.DocumentId &&
              x.Period == period.Sort &&
              x.Year == year &&
              x.Subject == course.Subject.Name.ToUpper()).FirstOrDefault();

            if (existing == null)
            {
              double totalWeighting = 0;
              double score = 0;
              var grades = _context.StudentGrades.Find(x => x.Student.Id == student.Id && x.Period.Id == period.Id && x.Course.Id == course.Id).ToList();
              if (grades.Any(x => x.Bucket.Id == 1))
              {
                totalWeighting = totalWeighting + course.Buckets.Find(x => x.Id == 1).Weighting;
              }
              if (grades.Any(x => x.Bucket.Id == 2))
              {
                totalWeighting = totalWeighting + course.Buckets.Find(x => x.Id == 2).Weighting;
              }
              if (grades.Any(x => x.Bucket.Id == 3))
              {
                totalWeighting = totalWeighting + course.Buckets.Find(x => x.Id == 3).Weighting;
              }

              if (grades.Count > 0)
              {
                course.Buckets.ForEach(bucket =>
                {
                  var current = grades.Where(x => x.Bucket.Id == bucket.Id).Select(x => x.Score).ToList();
                  if (current.Count > 0)
                  {
                    var weighting = current.Average();
                    score = score + (weighting * (bucket.Weighting / totalWeighting));
                  }
                });
              }
              var credit = new Credit()
              {
                Type = student.Plan.Degree.Name,
                Subject = course.Subject.Name.ToUpper(),
                DocumentId = student.DocumentId,
                Student = student.FullName.ToUpper(),
                Year = 2021,
                Level = student.Plan.Level.Ordinal,
                Degree = student.Plan.Name,
                Period = period.Sort,
                Grade = score
              };
              _context.Credits.InsertOne(credit);
            }
          });
        });

      }

    }

    public void SaveCredits(int year, string schoolId)
    {
      var students = _context.Students.Find(x => x.Active).ToList();
      var periods = _context.Periods.Find(x => true).ToList();
      students.ForEach(student =>
      {
        if (student.Plan != null)
        {
          var courses = _context.Courses.Find(x => x.Plan.Id == student.Plan.Id && x.Active).ToList();
          courses.ForEach(course =>
          {
            periods.ForEach(period =>
            {
              var existing = _context.Credits.Find(x =>
                x.DocumentId == student.DocumentId &&
                x.Period == period.Sort &&
                x.Year == year &&
                x.Subject == course.Subject.Name.ToUpper()).FirstOrDefault();

              if (existing == null)
              {
                double totalWeighting = 0;
                double score = 0;
                var grades = _context.StudentGrades.Find(x => x.Student.Id == student.Id && x.Period.Id == period.Id && x.Course.Id == course.Id).ToList();
                if (grades.Any(x => x.Bucket.Id == 1))
                {
                  totalWeighting = totalWeighting + course.Buckets.Find(x => x.Id == 1).Weighting;
                }
                if (grades.Any(x => x.Bucket.Id == 2))
                {
                  totalWeighting = totalWeighting + course.Buckets.Find(x => x.Id == 2).Weighting;
                }
                if (grades.Any(x => x.Bucket.Id == 3))
                {
                  totalWeighting = totalWeighting + course.Buckets.Find(x => x.Id == 3).Weighting;
                }

                if (grades.Count > 0)
                {
                  course.Buckets.ForEach(bucket =>
                  {
                    var current = grades.Where(x => x.Bucket.Id == bucket.Id).Select(x => x.Score).ToList();
                    if (current.Count > 0)
                    {
                      var weighting = current.Average();
                      score = score + (weighting * (bucket.Weighting / totalWeighting));
                    }
                  });
                }
                var credit = new Credit()
                {
                  Type = student.Plan.Degree.Name,
                  Subject = course.Subject.Name.ToUpper(),
                  DocumentId = student.DocumentId,
                  Student = student.FullName.ToUpper(),
                  Year = 2021,
                  Level = student.Plan.Level.Ordinal,
                  Degree = student.Plan.Name,
                  Period = period.Sort,
                  Grade = score
                };
                _context.Credits.InsertOne(credit);
              }
            });
          });

        }
      });
    }
    public void ChangeLevel(int currentYear)
    {
      var plans = _context.StudyPlans.Find(x => true).ToList();
      plans.ForEach(plan =>
      {
        UpdateDefinition<Student> update;
        var filter = Builders<Student>.Filter.Where(x => x.Plan.Id == plan.Id && x.CurrentYear != currentYear);
        var next = _context.StudyPlans.Find(x => x.Degree.Id == plan.Degree.Id && x.Level.Id == (plan.Level.Id + 1)).FirstOrDefault();
        if (next == null)
        {
          next = _context.StudyPlans.Find(x => x.Level.Id == (plan.Level.Id + 1)).FirstOrDefault();
        }

        update = Builders<Student>.Update.Set(x => x.Plan, next).Set(x => x.CurrentYear, currentYear);

        _context.Students.UpdateMany(filter, update);
      });
    }

    public async Task DeleteFiles()
    {
      var documents = await _context.Documents.Find(x => x.Student != null || x.Assignment != null).ToListAsync();
      documents.ForEach(async document =>
      {
        await _context.bucket.DeleteAsync(ObjectId.Parse(document.File.Id));
      });

      await _context.Documents.DeleteManyAsync(x => x.Student != null || x.Assignment != null);
    }

    public async Task FixPlan()
    {
      var students = await _context.Students.Find(x => true).ToListAsync();
      students.ForEach(student =>
      {
        var grade = _context.StudentGrades.Find(x => x.Student.Id == student.Id).FirstOrDefault();
        if (grade != null)
        {
          var plan = _context.StudyPlans.Find(x => x.Id == student.Plan.Id).FirstOrDefault();
          var update = Builders<Student>.Update.Set(x => x.Plan, plan);
          _context.Students.UpdateOne(x => x.Id == student.Id, update);
        }
      });
      /* 			var plans = await _context.StudyPlans.Find(x => true).ToListAsync();
						plans.ForEach(async plan =>
						{
							var items = await _context.ArchiveGrades.Find(x => x.Group.StudyPlan.Id == plan.Id).ToListAsync();
							var ids = items.Select(x => x.Student.Id).ToArray();
							var filter = Builders<Student>.Filter.In(x => x.Id, ids);
							var update = Builders<Student>.Update.Set(x => x.Plan, plan);

							await _context.Students.UpdateManyAsync(filter, update);
						}); */
    }

    public async Task ClearYear()
    {
      await _context.Assignments.DeleteManyAsync(x => true);
      await _context.CourseContents.DeleteManyAsync(x => true);
      await _context.CourseMessages.DeleteManyAsync(x => true);
      await _context.QuizAssignations.DeleteManyAsync(x => true);
      await _context.ExamAssignations.DeleteManyAsync(x => true);
      await _context.ExamResults.DeleteManyAsync(x => true);
      await _context.Forums.DeleteManyAsync(x => true);
      await _context.ForumPosts.DeleteManyAsync(x => true);
      await _context.QuizResults.DeleteManyAsync(x => true);
      await _context.Activities.DeleteManyAsync(x => true);
      await _context.Announcements.DeleteManyAsync(x => true);
      await _context.Documents.DeleteManyAsync(x => true);
      await _context.bucket.DropAsync();
      await _context.Grades.DeleteManyAsync(x => true);
      // await _context.StudentGrades.DeleteManyAsync(x => true);
    }
  }

  public interface ISchoolsService : IContextService<School, School>
  {
    Task<School> GetDefaultSchool();
    void SaveCredits(int year, string schoolId);
    void SaveStudentCredits(int year, string schoolId, string studentId);
    Task<IEnumerable<Student>> GetStudents(string id);
    Task<IEnumerable<Teacher>> GetTeachers(string id);
    Task SetStudents(string id);
    void CloseYear(string id);
    Task FixPlan();
    Task ClearYear();
  }
}