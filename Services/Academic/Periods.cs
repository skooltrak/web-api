using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
    public class PeriodsService : IPeriodsService
    {
        private readonly IDbContext _context;
        private readonly IStudentsService _studentsService;

        public PeriodsService(IDbContext context, IStudentsService studentsService)
        {
            _context = context;
            _studentsService = studentsService;
        }
        public Task ClosePeriod(string id)
        {
            throw new System.NotImplementedException();
        }

        public async Task<Period> Create(Period period, UserInfo user)
        {
            await _context.Periods.InsertOneAsync(period);
            return period;
        }

        public async Task GenerateRanking(string id)
        {
            var period = await _context.Periods.Find(x => x.Id == id).FirstOrDefaultAsync();

            var plans = await _context.StudyPlans.Find(x => !x.Preschool).ToListAsync();

            await _context.Rankings.DeleteManyAsync(x => x.Period.Id == id);

            plans.ForEach(plan =>
            {
                var groups = _context.Groups.Find(x => x.StudyPlan.Id == plan.Id).ToList();
                groups.ForEach(group =>
                {
                    var students = _context.Students.Find(x => x.Group.Id == group.Id && x.Active).ToList();
                    students.ForEach(student =>
                    {
                        var ranking = new Ranking()
                        {
                            Period = period,
                            Plan = plan,
                            Group = group,
                            Student = new StudentSummary()
                            {
                                Id = student.Id,
                                Name = student.Name,
                                FullName = student.FullName,
                                ShortName = student.ShortName,
                                DocumentId = student.DocumentId,
                                FirstName = student.FirstName,
                                MiddleName = student.MiddleName,
                                Surname = student.Surname,
                                SecondSurname = student.SecondSurname,
                                Gender = student.Gender != null ? student.Gender.Name : "",
                                Group = student.Group,
                            },
                            Grade = _studentsService.GetPeriodScore(student.Id, period.Id)
                        };
                        _context.Rankings.InsertOne(ranking);
                    });
                });
            });
        }

        public async Task<IEnumerable<Period>> Get(UserInfo user)
        {
            return await _context.Periods.Find(x => true).ToListAsync();
        }

        public async Task<Period> Get(string id, UserInfo user)
        {
            return await _context.Periods.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public Task<Period> GetActive()
        {
            throw new System.NotImplementedException();
        }

        public async Task Remove(string id, UserInfo user)
        {
            await _context.Periods.DeleteOneAsync(x => x.Id == id);
        }

        public async Task Update(string id, Period period, UserInfo user)
        {
            var update = Builders<Course>.Update.Set(x => x.CurrentPeriod, period);
            await _context.Periods.ReplaceOneAsync(x => x.Id == id, period);
            await _context.Courses.UpdateManyAsync(x => x.CurrentPeriod.Id == period.Id, update);
        }
    }

    public interface IPeriodsService : IContextService<Period, Period>
    {
        Task GenerateRanking(string id);
        Task ClosePeriod(string id);
        Task<Period> GetActive();
    }
}