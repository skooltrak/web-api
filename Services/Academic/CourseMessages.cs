using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;
using skooltrak_api.Shared;

namespace skooltrak_api.Services
{
	public class CourseMessagesService : ICourseMessagesService
	{
		private readonly IDbContext _context;
		private readonly Mailer mailer;

		public CourseMessagesService(IDbContext context)
		{
			_context = context;
			mailer = new Mailer();
		}

		public async Task<CourseMessage> Create(CourseMessage message, UserInfo user)
		{
			message.CreateUser = user;
			await _context.CourseMessages.InsertOneAsync(message);
			return message;
		}
		public async Task<IEnumerable<CourseMessage>> Get(UserInfo user)
		{
			return await _context.CourseMessages.Find(x => true).ToListAsync();
		}

		public async Task<CourseMessage> Get(string id, UserInfo user)
		{
			return await _context.CourseMessages.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.CourseMessages.DeleteOneAsync(x => x.Id == id);
		}

		public Task Update(string id, CourseMessage element, UserInfo user)
		{
			throw new System.NotImplementedException();
		}
	}

	public interface ICourseMessagesService : IContextService<CourseMessage, CourseMessage> { }
}