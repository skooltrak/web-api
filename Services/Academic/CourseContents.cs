using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class CourseContentsService : ICourseContentsService
	{
		private readonly IDbContext _context;

		public CourseContentsService(IDbContext context)
		{
			_context = context;
		}

		public async Task<CourseContent> Create(CourseContent content, UserInfo user)
		{
			content.CreateUser = user;
			await _context.CourseContents.InsertOneAsync(content);
			var activity = new Activity() {
				Type = (int)ActivityEnum.Content,
				Course = content.Course,
				Private = false,
				Description = "El docente " + content.CreateUser.DisplayName + " subió un nuevo contenido",
				CreatedBy = user
			};
			await _context.Activities.InsertOneAsync(activity);
			return content;
		}

		public async Task<IEnumerable<CourseContent>> Get(UserInfo user)
		{
			return await _context.CourseContents.Find(x => true).ToListAsync();
		}

		public async Task<CourseContent> Get(string id, UserInfo user)
		{
			return await _context.CourseContents.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.CourseContents.DeleteOneAsync(x => x.Id == id);
		}

		public async Task Update(string id, CourseContent content, UserInfo user)
		{
			var update = Builders<CourseContent>.Update
				.Set(x => x.Body, content.Body)
				.Set(x => x.Course, content.Course)
				.Set(x => x.Title, content.Title)
				.Set(x => x.ModificateDate, DateTime.Now);

			await _context.CourseContents.UpdateOneAsync(x => x.Id == id, update);
		}
	}

	public interface ICourseContentsService : IContextService<CourseContent, CourseContent> { }
}