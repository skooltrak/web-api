using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class CommentsService : ICommentsService
	{
		private readonly IDbContext _context;

		public CommentsService(IDbContext context)
		{
			_context = context;
		}

		public async Task<Comment> Create(Comment element, UserInfo user)
		{
			await _context.Comments.InsertOneAsync(element);
			return element;
		}

		public async Task<IEnumerable<Comment>> Get(UserInfo user)
		{
			return await _context.Comments.Find(x => true).ToListAsync();
		}

		public async Task<Comment> Get(string id, UserInfo user)
		{
			return await _context.Comments.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.Comments.DeleteOneAsync(x => x.Id == id);
		}

		public Task Update(string id, Comment element, UserInfo user)
		{
			throw new System.NotImplementedException();
		}
	}

	public interface ICommentsService : IContextService<Comment, Comment> { }
}