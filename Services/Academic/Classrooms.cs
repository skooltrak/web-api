using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{

    public class ClassroomsService : IClassroomsService
    {
        private readonly IDbContext _context;
        public ClassroomsService(IDbContext context)
        {
            _context = context;
        }

        public async Task<Classroom> Create(Classroom room, UserInfo user)
        {
            room.CreatedBy = user;
            await _context.Classrooms.InsertOneAsync(room);
            return room;
        }

        public async Task<IEnumerable<Classroom>> Get(UserInfo user)
        {
            return await _context.Classrooms.Find(x => x.Public).ToListAsync();
        }

        public async Task<Classroom> Get(string id, UserInfo user)
        {
            return await _context.Classrooms.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Classroom>> GetClassrooms()
        {
            return await _context.Classrooms.Find(x => true).ToListAsync();
        }

        public async Task Remove(string id, UserInfo user)
        {
            await _context.Classrooms.DeleteOneAsync(x => x.Id == id);
        }

        public async Task Update(string id, Classroom room, UserInfo user)
        {
            await _context.Classrooms.ReplaceOneAsync(x => x.Id == id, room);
        }
    }
    public interface IClassroomsService : IContextService<Classroom, Classroom>
    {
		Task<IEnumerable<Classroom>> GetClassrooms();
    }
}