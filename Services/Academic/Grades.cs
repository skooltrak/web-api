using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;
namespace skooltrak_api.Services
{
    public class GradesService : IGradesService
    {
        private readonly IDbContext _context;

        public GradesService(IDbContext context)
        {
            _context = context;
        }

        public async Task<Grade> Create(Grade grade, UserInfo user)
        {
            var course = await _context.Courses.Find(x => x.Id == grade.Course.Id).FirstOrDefaultAsync();
            grade.Period = course.CurrentPeriod;
            await _context.Grades.InsertOneAsync(grade);
            return grade;
        }

        public async Task<IEnumerable<Grade>> Get(UserInfo user)
        {
            return await _context.Grades.Find(x => true).ToListAsync();
        }

        public async Task<Grade> Get(string id, UserInfo user)
        {
            return await _context.Grades.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task MoveToGroup()
        {
            var grades = await _context.Grades.Find(x => true).ToListAsync();
            grades.ForEach(grade =>
            {
                var course = _context.Courses.Find(x => x.Id == grade.Course.Id).FirstOrDefault();
                var groups = _context.Groups.Find(x => x.StudyPlan.Id == course.Plan.Id).ToList();
                groups.ForEach(group =>
                {
                    var students = grade.StudentsGrades.FindAll(x => x.Group != null && x.Group.Id == group.Id);
                    grade.Groups.Add(new GradeGroup()
                    {
                        Group = new GroupReference()
                        {
                            Id = group.Id,
                            Name = group.Name,
                            Level = group.Level,
                        },
                        Students = students
                    });
                });
                _context.Grades.ReplaceOne(x => x.Id == grade.Id, grade);
            });
            var remove = Builders<Grade>.Update.Unset(x => x.StudentsGrades);
            _context.Grades.UpdateMany(x => true, remove);
        }

        public async Task PublishGrades(string id, UserInfo user)
        {
            var grade = await _context.Grades.Find(x => x.Id == id).FirstOrDefaultAsync();
            grade.Groups.ForEach(group =>
            {
                group.Students.ForEach(async current =>
                    {
                        if (current.Included)
                        {
                            var student = new StudentGrade()
                            {
                                Student = current.Student,
                                Comments = current.Comments,
                                Period = grade.Period,
                                Score = current.Score,
                                Course = grade.Course,
                                Grade = new Reference() { Id = grade.Id, Name = grade.Title },
                                Bucket = grade.Bucket,
                                CreateUser = user
                            };
                            await _context.StudentGrades.InsertOneAsync(student);
                        }

                    });
            });

            grade.Published = true;
            await _context.Grades.ReplaceOneAsync(x => x.Id == id, grade);
        }

        public async Task Remove(string id, UserInfo user)
        {
            await _context.StudentGrades.DeleteManyAsync(x => x.Grade.Id == id);
            await _context.Grades.DeleteOneAsync(x => x.Id == id);
        }

        public async Task SetGroup()
        {
            var students = await _context.Students.Find(x => x.Active).ToListAsync();

            students.ForEach(async student =>
            {
                var update = Builders<StudentGrade>.Update.Set(x => x.Group, student.Group);
                await _context.StudentGrades.UpdateManyAsync(x => x.Student.Id == student.Id, update);

                var updateGroup = Builders<Grade>.Update.Set(x => x.StudentsGrades[-1].Group, student.Group);

                var filter = Builders<Grade>.Filter.ElemMatch(x => x.StudentsGrades, x => x.Student.Id == student.Id);

                await _context.Grades.UpdateManyAsync(filter, updateGroup);
            });
        }

        public async Task SetPreviousGrades(string studentId)
        {
            var student = await _context.Students.Find(x => x.Id == studentId).FirstOrDefaultAsync();
            if (student == null)
            {
                return;
            }

            var courses = await _context.Courses.Find(x => x.Plan.Id == student.Plan.Id).ToListAsync();

            courses.ForEach(async course =>
            {
                var grades = await _context.Grades.Find(x => x.Course.Id == course.Id && !x.StudentsGrades.Any(y => y.Student.Id == student.Id)).ToListAsync();
                grades.ForEach(async grade =>
                {
                    var item = new GradeStudent()
                    {
                        Student = new Reference()
                        {
                            Id = student.Id,
                            Name = student.ShortName
                        },
                        Group = student.Group
                    };

                    var update = Builders<Grade>
                        .Update.Push(x => x.StudentsGrades, item);
                    await _context.Grades.UpdateManyAsync(x => x.Id == grade.Id, update);
                    if (grade.Published)
                    {
                        var student = new StudentGrade()
                        {
                            Student = item.Student,
                            Comments = item.Comments,
                            Period = grade.Period,
                            Score = item.Score,
                            Course = grade.Course,
                            Grade = new Reference() { Id = grade.Id, Name = grade.Title },
                            Bucket = grade.Bucket,
                            CreateUser = grade.CreateUser
                        };
                        await _context.StudentGrades.InsertOneAsync(student);
                    }
                });
            });
        }

        public async Task Update(string id, Grade grade, UserInfo user)
        {
            var old = await _context.Grades.Find(x => x.Id == id).FirstOrDefaultAsync();
            old.Groups.ForEach(group =>
            {
                group.Students.ForEach(async student =>
                    {
                        var current = grade.Groups.Find(x => x.Group.Id == group.Group.Id).Students.Find(x => x.Student.Id == student.Student.Id);
                        if (!current.Included && student.Included)
                        {
                            await _context.StudentGrades.DeleteOneAsync(x => x.Student.Id == current.Student.Id && x.Grade.Id == grade.Id);
                        }
                        else if (current.Included && !student.Included)
                        {
                            var newGrade = new StudentGrade()
                            {
                                Student = current.Student,
                                Comments = current.Comments,
                                Group = group.Group,
                                Period = grade.Period,
                                Score = current.Score,
                                Course = grade.Course,
                                Grade = new Reference() { Id = grade.Id, Name = grade.Title },
                                Bucket = grade.Bucket,
                                CreateUser = user
                            };
                            await _context.StudentGrades.InsertOneAsync(newGrade);
                        }
                        else
                        {
                            if (student.Score != current.Score || student.Comments != current.Comments)
                            {
                                var newGrade = Builders<StudentGrade>.Update
                                    .Set(x => x.ModificateDate, DateTime.Now)
                                    .Set(x => x.Score, current.Score)
                                    .Set(x => x.Comments, current.Comments);
                                await _context.StudentGrades.UpdateOneAsync(x => x.Student.Id == current.Student.Id && x.Grade.Id == grade.Id, newGrade);
                            };
                        }

                    });
            });

            if (old.Title != grade.Title || old.Bucket.Id != grade.Bucket.Id)
            {
                var updateTitle = Builders<StudentGrade>.Update
                .Set(x => x.Grade.Name, grade.Title)
                .Set(x => x.Bucket, grade.Bucket);
                await _context.StudentGrades.UpdateManyAsync(x => x.Grade.Id == grade.Id, updateTitle);
            }

            var update = Builders<Grade>.Update
                .Set(x => x.Date, grade.Date)
                .Set(x => x.Bucket, grade.Bucket)
                .Set(x => x.ModificateDate, DateTime.Now)
                .Set(x => x.Groups, grade.Groups)
                .Set(x => x.Title, grade.Title);
            await _context.Grades.UpdateOneAsync(x => x.Id == id, update);
        }
    }
    public interface IGradesService : IContextService<Grade, Grade>
    {
        Task PublishGrades(string id, UserInfo user);
        Task MoveToGroup();

        Task SetGroup();

        Task SetPreviousGrades(string studentId);
    }
}