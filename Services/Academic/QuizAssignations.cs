using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class QuizAssignationsService : IQuizAssignationsService
	{
		private readonly IDbContext _context;

		public QuizAssignationsService(IDbContext context)
		{
			_context = context;
		}

		public async Task<QuizAssignation> Create(QuizAssignation assignation, UserInfo user)
		{
			assignation.CreateUser = user;
			assignation.Course = assignation.Quiz.Course;
			await _context.QuizAssignations.InsertOneAsync(assignation);
			var quiz = await _context.Quizes.Find(x => x.Id == assignation.Quiz.Id).FirstOrDefaultAsync();
			var students = await _context.Students.Find(x => x.Group.Id == assignation.Group.Id).ToListAsync();
			students.ForEach(async s =>
			{
				var result = new QuizResult()
				{
					Assignation = new Reference()
					{
						Id = assignation.Id,
						Name = assignation.Title
					},
					Quiz = new Reference() { Id = assignation.Quiz.Id, Name = assignation.Quiz.Title },
					Course = assignation.Course,
					Student = new Reference()
					{
						Id = s.Id,
						Name = s.Surname + ", " + s.FirstName
					},
					TotalPoints = quiz.Questions.Sum(x => x.Points),
					StartDate = assignation.StartDate,
					EndDate = assignation.EndDate
				};

				await _context.QuizResults.InsertOneAsync(result);
			});
			return assignation;
		}

		public async Task<IEnumerable<QuizAssignation>> Get(UserInfo user)
		{
			return await _context.QuizAssignations.Find(x => true).ToListAsync();
		}

		public async Task<QuizAssignation> Get(string id, UserInfo user)
		{
			return await _context.QuizAssignations.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task<IEnumerable<QuizResult>> GetResults(string id)
		{
			return await _context.QuizResults.Find(x => x.Assignation.Id == id).SortByDescending(x => x.Status).ThenBy(x => x.Student.Name).ToListAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.QuizAssignations.DeleteOneAsync(x => x.Id == id);
			await _context.QuizResults.DeleteManyAsync(x => x.Assignation.Id == id);
		}

		public async Task Update(string id, QuizAssignation assignation, UserInfo user)
		{
			var update = Builders<QuizAssignation>.Update
				.Set(x => x.Title, assignation.Title)
				.Set(x => x.StartDate, assignation.StartDate)
				.Set(x => x.EndDate, assignation.EndDate);

			var result = Builders<QuizResult>.Update
				.Set(x => x.StartDate, assignation.StartDate)
				.Set(x => x.EndDate, assignation.EndDate);

			await _context.QuizAssignations.UpdateOneAsync(x => x.Id == id, update);
			await _context.QuizResults.UpdateManyAsync(x => x.Assignation.Id == id, result);

		}
	}

	public interface IQuizAssignationsService : IContextService<QuizAssignation, QuizAssignation>
	{
		Task<IEnumerable<QuizResult>> GetResults(string id);
	}
}