using System.Collections.Generic;
using System.Threading.Tasks;
using skooltrak_api.Models;
using MongoDB.Driver;

namespace skooltrak_api.Services
{
    public class SubjectsService : ISubjectsService
    {
        private readonly IDbContext _context;

        public SubjectsService(IDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Subject>> Get(UserInfo user)
        {
            return await _context.Subjects.Find(x => true).ToListAsync();
        }

        public async Task<Subject> Get(string id, UserInfo user)
        {
            return await _context.Subjects.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<Subject> Create(Subject subject, UserInfo user)
        {
            await _context.Subjects.InsertOneAsync(subject);
            return subject;
        }

        public async Task Update(string id, Subject subject, UserInfo user)
        {
            var update = Builders<Subject>.Update
                .Set(x => x.Name, subject.Name)
                .Set(x => x.ShortName, subject.ShortName)
                .Set(x => x.Code, subject.Code);

            var course = Builders<Course>.Update
                .Set(x => x.Subject, subject);

            var updateGrade = Builders<Grade>.Update
                .Set(x => x.Course.Subject, subject);
            var updateStudentGrade = Builders<StudentGrade>.Update
                .Set(x => x.Course.Subject, subject);

            var parent = Builders<Course>.Update.Set(x => x.ParentSubject, subject);
			var parentUpdateGrade = Builders<Grade>.Update
                .Set(x => x.Course.ParentSubject, subject);
			var parentUpdateStudentGrade = Builders<StudentGrade>.Update
                .Set(x => x.Course.ParentSubject, subject);
            await _context.Subjects.UpdateOneAsync(x => x.Id == id, update);
            await _context.Courses.UpdateManyAsync(x => x.Subject.Id == subject.Id, course);
            await _context.Courses.UpdateManyAsync(x => x.ParentSubject.Id == subject.Id, parent);
			await _context.Grades.UpdateManyAsync(x => x.Course.Subject.Id == subject.Id, updateGrade);
			await _context.StudentGrades.UpdateManyAsync(x => x.Course.Subject.Id == subject.Id, updateStudentGrade);
			await _context.Grades.UpdateManyAsync(x => x.Course.ParentSubject.Id == subject.Id, parentUpdateGrade);
			await _context.StudentGrades.UpdateManyAsync(x => x.Course.ParentSubject.Id == subject.Id, parentUpdateStudentGrade);
        }

        public async Task Remove(string id, UserInfo user)
        {
            await _context.Subjects.DeleteOneAsync(x => x.Id == id);
        }
    }

    public interface ISubjectsService : IContextService<Subject, Subject>
    {

    }

}