using System;
using System.Collections.Generic;
using MongoDB.Driver;
using System.Threading.Tasks;
using skooltrak_api.Models;
using System.Linq;

namespace skooltrak_api.Services
{
    public class ExamResultsService : IExamResultsService
    {
        private readonly IDbContext _context;

        public ExamResultsService(IDbContext context)
        {
            _context = context;
        }

        public Task<ExamResult> Create(ExamResult element, UserInfo user)
        {
            throw new System.NotImplementedException();
        }

        public Task<IEnumerable<ExamResult>> Get(UserInfo user)
        {
            throw new System.NotImplementedException();
        }

        public async Task<ExamResult> Get(string id, UserInfo user)
        {
            return await _context.ExamResults.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public Task Remove(string id, UserInfo user)
        {
            throw new System.NotImplementedException();
        }

        public async Task SetResults()
        {
            var assignations = await _context.ExamAssignations.Find(x => true).ToListAsync();
            assignations.ForEach(async assignation =>
            {
                var exam = await _context.Exams.Find(x => x.Id == assignation.Exam.Id).FirstOrDefaultAsync();
                var students = await _context.Students.Find(x => x.Group.Id == assignation.Group.Id).ToListAsync();
                students.ForEach(async s =>
                {
                    var result = new ExamResult()
                    {
                        Assignation = new Reference()
                        {
                            Id = assignation.Id,
                            Name = assignation.Title
                        },
                        Exam = new Reference() { Id = assignation.Exam.Id, Name = assignation.Exam.Title },
                        Course = assignation.Course,
                        Student = new Reference()
                        {
                            Id = s.Id,
                            Name = s.Surname + ", " + s.FirstName
                        },
                        TotalPoints = exam.Questions.Sum(x => x.Points),
                        StartDate = assignation.StartDate,
                        EndDate = assignation.EndDate
                    };
                    await _context.ExamResults.InsertOneAsync(result);
                });
            });
        }

        public async Task Update(string id, ExamResult result, UserInfo user)
        {
            result.ModificateDate = DateTime.Now;
            double points = 0;
            result.Answers.ForEach(answer =>
            {
                switch (answer.Question.Type.Code)
                {
                    case (int)QuestionEnum.TrueFalse:
                        if (answer.Question.TrueFalse == answer.ResponseBoolean)
                        {
                            points = points + answer.Question.Points;
                            answer.Points = answer.Question.Points;
                        }
                        break;
                    case (int)QuestionEnum.Selection:

                        if (answer.Selection != null && answer.Selection.IsCorrect)
                        {
                            points = points + answer.Question.Points;
                            answer.Points = answer.Question.Points;
                        }
                        break;
                    case (int)QuestionEnum.Match:
                        answer.Points = 0;
                        answer.MatchList.ForEach(match =>
                        {
                            if (match.SelectedMatch.Count > 0)
                            {
                                if (match.CorrectMatch == match.SelectedMatch[0])
                                {
                                    answer.Points = answer.Points + Math.Round((answer.Question.Points / answer.MatchList.Count), 0);
                                }
                            }
                        });
                        points = points + answer.Points;
                        break;
                    default:
                        points = points + answer.Points;
                        break;
                }
            });
            result.Points = Math.Round(points, 0);
            await _context.ExamResults.ReplaceOneAsync(x => x.Id == id, result);
            var assignation = await _context.ExamAssignations.Find(x => x.Id == result.Assignation.Id).FirstOrDefaultAsync();
            if (assignation == null || assignation.Grade == null)
            {
                return;
            }
            var grade = await _context.Grades.Find(x => x.Id == assignation.Grade.Id).FirstOrDefaultAsync();

            var filter = Builders<Grade>.Filter.And(
                Builders<Grade>.Filter.Eq(x => x.Id, assignation.Grade.Id),
                Builders<Grade>.Filter.ElemMatch(x => x.StudentsGrades, x => x.Student.Id == result.Student.Id)
            );
            var update = Builders<Grade>.Update.Set(x => x.StudentsGrades[-1].Score, Shared.Util.Grade(result.TotalPoints, result.Points));
            await _context.Grades.UpdateOneAsync(filter, update);
            if (grade.Published)
            {
                var scoreUpdate = Builders<StudentGrade>.Update.Set(x => x.Score, Shared.Util.Grade(result.TotalPoints, result.Points));
                await _context.StudentGrades.UpdateOneAsync(x => x.Grade.Id == grade.Id && x.Student.Id == result.Student.Id, scoreUpdate);
            }
        }
    }

    public interface IExamResultsService : IContextService<ExamResult, ExamResult>
    {
        Task SetResults();
    }
}