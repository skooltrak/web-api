using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using System.Threading.Tasks;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class QuizResultsService : IQuizResultsService
	{
		private readonly IDbContext _context;

		public QuizResultsService(IDbContext context)
		{
			_context = context;
		}

		public Task<QuizResult> Create(QuizResult element, UserInfo user)
		{
			throw new System.NotImplementedException();
		}

		public Task<IEnumerable<QuizResult>> Get(UserInfo user)
		{
			throw new System.NotImplementedException();
		}

		public async Task<QuizResult> Get(string id, UserInfo user)
		{
			return await _context.QuizResults.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public Task Remove(string id, UserInfo user)
		{
			throw new System.NotImplementedException();
		}

		public async Task Update(string id, QuizResult result, UserInfo user)
		{
			result.ModificateDate = DateTime.Now;
			result.Points = result.Answers.Where(x => x.Selected.IsCorrect).Sum(x => x.Question.Points);
			await _context.QuizResults.ReplaceOneAsync(x => x.Id == id, result);
		}
	}

	public interface IQuizResultsService : IContextService<QuizResult, QuizResult> { }
}