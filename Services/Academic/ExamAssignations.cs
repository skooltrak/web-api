using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class ExamAssignationsService : IExamsAssignationsService
	{
		private readonly IDbContext _context;

		public ExamAssignationsService(IDbContext context)
		{
			_context = context;
		}

		public async Task<ExamAssignation> Create(ExamAssignation assignation, UserInfo user)
		{
			assignation.CreateUser = user;
			assignation.Course = assignation.Exam.Course;
			await _context.ExamAssignations.InsertOneAsync(assignation);
			var exam = await _context.Exams.Find(x => x.Id == assignation.Exam.Id).FirstOrDefaultAsync();
			var students = await _context.Students.Find(x => x.Group.Id == assignation.Group.Id).ToListAsync();
			students.ForEach(async s =>
			{
				var result = new ExamResult()
				{
					Assignation = new Reference()
					{
						Id = assignation.Id,
						Name = assignation.Title
					},
					Exam = new Reference() { Id = assignation.Exam.Id, Name = assignation.Exam.Title },
					Course = assignation.Course,
					Minutes = assignation.Minutes,
					Student = new Reference()
					{
						Id = s.Id,
						Name = s.Surname + ", " + s.FirstName
					},
					TotalPoints = exam.Questions.Sum(x => x.Points),
					StartDate = assignation.StartDate,
					EndDate = assignation.EndDate
				};
				await _context.ExamResults.InsertOneAsync(result);
			});

			return assignation;
		}

		public async Task<IEnumerable<ExamAssignation>> Get(UserInfo user)
		{
			return await _context.ExamAssignations.Find(x => true).ToListAsync();
		}

		public async Task<ExamAssignation> Get(string id, UserInfo user)
		{
			return await _context.ExamAssignations.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task<IEnumerable<ExamResult>> GetResults(string id)
		{
			return await _context.ExamResults.Find(x => x.Assignation.Id == id).SortByDescending(x => x.Status).ThenBy(x => x.Student).ToListAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.ExamResults.DeleteManyAsync(x => x.Assignation.Id == id);
			await _context.ExamAssignations.DeleteOneAsync(x => x.Id == id);
		}

		public async Task Update(string id, ExamAssignation element, UserInfo user)
		{
			var update = Builders<ExamAssignation>.Update
				.Set(x => x.Minutes, element.Minutes)
				.Set(x => x.Title, element.Title)
				.Set(x => x.Exam, element.Exam)
				.Set(x => x.StartDate, element.StartDate)
				.Set(x => x.Group, element.Group)
				.Set(x => x.EndDate, element.EndDate);

			var result = Builders<ExamResult>.Update
				.Set(x => x.StartDate, element.StartDate)
				.Set(x => x.EndDate, element.EndDate)
				.Set(x => x.Minutes, element.Minutes);
			await _context.ExamAssignations.UpdateOneAsync(x => x.Id == id, update);
			await _context.ExamResults.UpdateManyAsync(x => x.Assignation.Id == element.Id, result);
		}
	}

	public interface IExamsAssignationsService : IContextService<ExamAssignation, ExamAssignation>
	{
		Task<IEnumerable<ExamResult>> GetResults(string id);
	}
}