using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using skooltrak_api.Models;
using MongoDB.Driver;
using skooltrak_api.Shared;

namespace skooltrak_api.Services
{
    public class TeachersService : ITeachersService
    {
        private readonly IDbContext _context;
        private readonly Mailer mailer;
        public TeachersService(IDbContext context)
        {
            _context = context;
            mailer = new Mailer();
        }

        public async Task<Teacher> Create(Teacher teacher, UserInfo user)
        {
            User teacherUser = new User()
            {
                DisplayName = teacher.FirstName + " " + teacher.Surname,
                Email = teacher.Email,
                UserName = "prof." + teacher.FirstName.ToLower() + "." + teacher.Surname.ToLower(),
                Role = await _context.Roles.Find(x => x.Code == (int)RoleEnum.Teacher).FirstOrDefaultAsync()
            };
            await _context.Users.InsertOneAsync(teacherUser);
            _ = mailer.WelcomeMessage(teacherUser);
            teacher.UserId = teacherUser.Id;
            await _context.Teachers.InsertOneAsync(teacher);
            return teacher;
        }

        public async Task<Teacher> Get(string id, UserInfo user)
        {
            return await _context.Teachers.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Teacher>> Get(UserInfo user)
        {
            return await _context.Teachers.Find(x => true).ToListAsync();
        }

        public async Task<IEnumerable<Activity>> GetActivities(string id)
        {
            var courses = await _context.Courses.Find(x => x.Teachers.Any(y => y.Id == id)).ToListAsync();
            var ids = courses.Select(x => x.Id).ToArray();
            return await _context.Activities.Find(x => ids.Contains(x.Course.Id)).SortByDescending(x => x.CreateDate).ToListAsync();
        }

        public async Task<IEnumerable<Assignment>> GetAssignments(string id)
        {
            return await _context.Assignments.Find(x => x.Teacher.Id == id).ToListAsync();
        }

        public async Task<IEnumerable<ClassDay>> GetClassDays(string id)
        {
            var teacher = await _context.Teachers.Find(x => x.Id == id).ToListAsync();
            var courses = await _context.Courses.Find(x => x.Teachers.Any(y => y.Id == id) && x.Active).ToListAsync();
            var plansIds = courses.Select(x => x.Plan.Id).ToList();
            var groups = await _context.Groups.Find(x => plansIds.Contains(x.StudyPlan.Id)).ToListAsync();
            List<ClassDay> days = new List<ClassDay>();

            var week = new List<string>() { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday" };
            int i = 0;
            week.ForEach(day =>
            {
                days.Add(new ClassDay()
                {
                    Day = i,
                    Name = day,
                    ClassHours = new List<ClassHour>()
                });
                i++;
            });

            groups.ForEach(group =>
            {
                group.Schedule.ForEach(day =>
                {
                    day.ClassHours.ForEach(hour =>
                    {
                        if (courses.Any(x => x.Id == hour.Course.Id))
                        {
                            day.ClassHours.Add(hour);
                        }
                    });
                });
            });

            return days;
        }

        public async Task<IEnumerable<Course>> GetCourses(string id)
        {
            return await _context.Courses.Find(x => x.Teachers.Any(y => y.Id == id) && x.Active).SortBy(x => x.Subject.Name).ThenBy(x => x.Plan.Level.Id).ToListAsync();
        }

        public async Task<IEnumerable<Document>> GetDocuments(string id)
        {
            var teacher = await _context.Teachers.Find(x => x.Id == id).FirstOrDefaultAsync();
            return await _context.Documents.Find(x => x.CreateUser.Id == teacher.UserId).SortByDescending(x => x.CreateDate).ToListAsync();
        }

        public async Task<IEnumerable<ExamAssignation>> GetExamAssignations(string id)
        {
            return await _context.ExamAssignations.Find(x => x.Teacher.Id == id).SortByDescending(x => x.Group.Level.Ordinal).ThenByDescending(x => x.CreateDate).ToListAsync();
        }

        public async Task<IEnumerable<ExamReference>> GetExams(string id)
        {
            return await _context.Exams.Find(x => x.Teacher.Id == id).Project<ExamReference>(Builders<Exam>.Projection.Expression(x => new ExamReference()
            {
                Id = x.Id,
                Title = x.Title,
                Description = x.Description,
                Course = x.Course,
                Teacher = x.Teacher,
                CreateDate = x.CreateDate,
                CreateUser = x.CreateUser,
                ModificateDate = x.ModificateDate
            })).ToListAsync();
        }

        public async Task<IEnumerable<Forum>> GetForums(string id)
        {
            var courses = await _context.Courses.Find(x => x.Teachers.Any(y => y.Id == id)).ToListAsync();
            var ids = courses.Select(x => x.Id).ToArray();
            return await _context.Forums.Find(x => ids.Contains(x.Course.Id)).SortByDescending(x => x.LastPost).ToListAsync();
        }

        public async Task<IEnumerable<ClassGroup>> GetGroups(string id)
        {
            return await _context.Groups.Find(x => x.Counselor.Id == id).ToListAsync();
        }

        public async Task<IEnumerable<Activity>> GetLastActivities(string id)
        {
            var courses = await _context.Courses.Find(x => x.Teachers.Any(y => y.Id == id)).ToListAsync();
            var ids = courses.Select(x => x.Id).ToArray();
            return await _context.Activities.Find(x => ids.Contains(x.Course.Id)).SortByDescending(x => x.CreateDate).Limit(15).ToListAsync();
        }

        public async Task<IEnumerable<QuizAssignation>> GetQuizAssignations(string id)
        {
            return await _context.QuizAssignations.Find(x => x.Quiz.Teacher.Id == id).SortByDescending(x => x.EndDate).ToListAsync();
        }

        public async Task<IEnumerable<Quiz>> GetQuizes(string id)
        {
            return await _context.Quizes.Find(x => x.Teacher.Id == id).ToListAsync();
        }

        public async Task<IEnumerable<TeacherDayClass>> GetTeacherDays(string id)
        {
            var list = new List<TeacherDayClass>();
            var courses = await _context.Courses.Find(x => x.Teachers.Any(y => y.Id == id)).ToListAsync();
            var ids = courses.Select(x => x.Id).ToList();
            var week = new List<string>() { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday" };
            int i = 1;
            week.ForEach(day =>
            {
                var current = new TeacherDayClass()
                {
                    Day = new DayClass()
                    {
                        Day = i,
                        Name = day
                    },
                    Classes = new List<TeacherClass>()
                };
                var classes = _context.TeacherClasses.Find(x => x.Day.Day == i && ids.Contains(x.Class.Course.Id))
                    .SortBy(x => x.Class.StartTime.Hour)
                    .ThenBy(x => x.Class.StartTime.Minute).ToList();
                current.Classes.AddRange(classes);
                list.Add(current);
                i++;
            });
            return list;
        }

        public async Task<IEnumerable<Topic>> GetTopics(string id)
        {
            return await _context.Topics.Find(x => x.Teacher.Id == id).ToListAsync();
        }

        public async Task<IEnumerable<Video>> GetVideos(string id)
        {
            var teacher = await _context.Teachers.Find(x => x.Id == id).FirstOrDefaultAsync();
            return await _context.Videos.Find(x => x.UploadedBy.Id == teacher.UserId).SortByDescending(x => x.CreatedDate).ToListAsync();
        }

        public async Task Remove(string id, UserInfo user)
        {
            var update = Builders<Course>.Update.PullFilter(x => x.Teachers, y => y.Id == id);
            await _context.Courses.UpdateOneAsync<Course>(x => x.Teachers.Any(t => t.Id == id), update);
            await _context.Teachers.DeleteOneAsync(x => x.Id == id);
        }

        public async Task Update(string id, Teacher teacher, UserInfo user)
        {
            var update = Builders<Teacher>.Update
                .Set(x => x.FirstName, teacher.FirstName)
                .Set(x => x.MiddleName, teacher.MiddleName)
                .Set(x => x.SecondSurname, teacher.SecondSurname)
                .Set(x => x.Surname, teacher.Surname)
                .Set(x => x.SchoolId, teacher.SchoolId)
                .Set(x => x.Subjects, teacher.Subjects)
                .Set(x => x.Gender, teacher.Gender)
                .Set(x => x.BirthDate, teacher.BirthDate)
                .Set(x => x.ModificateDate, DateTime.Now);
            await _context.Teachers.UpdateOneAsync(x => x.Id == id, update);
        }
    }

    public interface ITeachersService : IContextService<Teacher, Teacher>
    {

        Task<IEnumerable<ExamReference>> GetExams(string id);
        Task<IEnumerable<ClassDay>> GetClassDays(string id);
        Task<IEnumerable<TeacherDayClass>> GetTeacherDays(string id);
        Task<IEnumerable<ExamAssignation>> GetExamAssignations(string id);
        Task<IEnumerable<Activity>> GetActivities(string id);
        Task<IEnumerable<Activity>> GetLastActivities(string id);
        Task<IEnumerable<Document>> GetDocuments(string id);
        Task<IEnumerable<ClassGroup>> GetGroups(string id);
        Task<IEnumerable<Video>> GetVideos(string id);
        Task<IEnumerable<Quiz>> GetQuizes(string id);
        Task<IEnumerable<QuizAssignation>> GetQuizAssignations(string id);
        Task<IEnumerable<Course>> GetCourses(string id);
        Task<IEnumerable<Assignment>> GetAssignments(string id);
        Task<IEnumerable<Topic>> GetTopics(string id);
        Task<IEnumerable<Forum>> GetForums(string id);
    }
}