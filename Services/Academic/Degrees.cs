using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class DegreesService : IDegreesService
	{
		private readonly IDbContext _context;

		public DegreesService(IDbContext context)
		{
			_context = context;
		}
		public async Task<Degree> Create(Degree degree, UserInfo user)
		{
			await _context.Degrees.InsertOneAsync(degree);
			return degree;
		}

		public async Task<IEnumerable<Degree>> Get(UserInfo user)
		{
			return await _context.Degrees.Find(x => true).ToListAsync();
		}

		public async Task<Degree> Get(string id, UserInfo user)
		{
			return await _context.Degrees.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.Degrees.DeleteOneAsync(x => x.Id == id);
		}

		public async Task Update(string id, Degree degree, UserInfo user)
		{
			await _context.Degrees.ReplaceOneAsync(x => x.Id == id, degree);
		}
	}

	public interface IDegreesService : IContextService<Degree, Degree> { }
}