using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using skooltrak_api.Models;
using MongoDB.Driver;
using System.Linq;

namespace skooltrak_api.Services
{
  public class ClassGroupsService : IClassGroupsService
  {
    private readonly IDbContext _context;

    public ClassGroupsService(IDbContext context)
    {
      _context = context;
    }

    public async Task<ClassGroup> Create(ClassGroup group, UserInfo user)
    {
      await _context.Groups.InsertOneAsync(group);
      return group;
    }

    public async Task<ClassGroup> Get(string id, UserInfo user)
    {
      return await _context.Groups.Find(x => x.Id == id).SortBy(x => x.Level.Id).FirstOrDefaultAsync();
    }

    public long GetCount(string id)
    {
      return _context.Students.Find(x => x.Group.Id == id && x.Active).CountDocuments();
    }

    public async Task<IEnumerable<ClassGroup>> Get(UserInfo user)
    {
      return await _context.Groups.Find(x => true).Project<ClassGroup>(Builders<ClassGroup>.Projection.Expression(item => new ClassGroup()
      {
        Id = item.Id,
        SchoolId = item.SchoolId,
        Level = item.Level,
        Name = item.Name,
        Counselor = item.Counselor,
        StudyPlan = item.StudyPlan,
        CreateDate = item.CreateDate,
        ModificateDate = item.ModificateDate,
        Schedule = item.Schedule,
        StudentsCount = GetCount(item.Id)
      })).ToListAsync();
    }

    public Task<IEnumerable<Assignment>> GetAssignments(string id)
    {
      throw new NotImplementedException();
    }

    public async Task<IEnumerable<Attendance>> GetAttendances(string id)
    {
      return await _context.Attendance.Find(x => x.Group.Id == id).ToListAsync();
    }

    public async Task<IEnumerable<Student>> GetStudents(string id)
    {
      return await _context.Students.Find(x => x.Group.Id == id && x.Active).SortBy(x => x.Surname).ThenBy(x => x.FirstName).ToListAsync();
    }

    public async Task Remove(string id, UserInfo user)
    {
      await _context.Groups.DeleteOneAsync(x => x.Id == id);
    }

    public async Task Update(string id, ClassGroup group, UserInfo user)
    {
      var update = Builders<ClassGroup>.Update
              .Set(x => x.Name, group.Name)
              .Set(x => x.Level, group.Level)
              .Set(x => x.SchoolId, group.SchoolId)
              .Set(x => x.StudyPlan, group.StudyPlan)
              .Set(x => x.Counselor, group.Counselor)
              .Set(x => x.Schedule, group.Schedule)
              .Set(x => x.ModificateDate, DateTime.Now);


      await _context.Groups.UpdateOneAsync(x => x.Id == id, update);
      await SetTeacherClasses();
    }

    public async Task<IEnumerable<Course>> GetCourses(string id)
    {
      var group = await _context.Groups.Find(x => x.Id == id).FirstOrDefaultAsync();
      return await _context.Courses.Find(x => x.Plan.Id == group.StudyPlan.Id).SortBy(x => x.Subject.Name).ToListAsync();
    }

    public async Task SetTeacherClasses()
    {
      await _context.TeacherClasses.DeleteManyAsync(x => true);
      var groups = await _context.Groups.Find(x => true).ToListAsync();
      groups.ForEach(group =>
      {
        if (group.Schedule == null)
        {
          return;
        }
        group.Schedule.ForEach(day =>
              {
            if (day.ClassHours == null)
            {
              return;
            }
            var i = 0;
            day.ClassHours.ForEach(hour =>
                  {
                var value = new TeacherClass()
                {
                  Day = new DayClass()
                  {
                    Day = day.Day,
                    Name = day.Name
                  },
                  Group = group == null ? null : group,
                  Class = hour == null ? null : hour,
                };
                value.Class.EndTime = group.Schedule[0].ClassHours[i].EndTime;
                value.Class.StartTime = group.Schedule[0].ClassHours[i].StartTime;
                _context.TeacherClasses.InsertOne(value);
                i++;
              });
          });
      });
    }

    public async Task<IEnumerable<Classroom>> GetClassrooms(string id)
    {
      return await _context.Classrooms.Find(x => x.Groups.Any(y => y.Id == id) || x.Public).SortBy(x => x.Name).ToListAsync();
    }

    public async Task<IEnumerable<Ranking>> GetRankings(string id, string periodId)
    {
      return await _context.Rankings.Find(x => x.Group.Id == id && x.Period.Id == periodId).SortByDescending(x => x.Grade).ToListAsync();
    }

    public async Task<IEnumerable<User>> GetUsers(string id)
    {
      var students = await _context.Students.Find(x => x.Group.Id == id).ToListAsync();
      return await _context.Users.Find(x => students.Any(y => y.UserId == x.Id)).ToListAsync();
    }
  }

  public interface IClassGroupsService : IContextService<ClassGroup, ClassGroup>
  {
    Task<IEnumerable<Ranking>> GetRankings(string id, string periodId);
    Task<IEnumerable<Student>> GetStudents(string id);
    Task<IEnumerable<User>> GetUsers(string id);
    Task<IEnumerable<Classroom>> GetClassrooms(string id);
    Task SetTeacherClasses();
    Task<IEnumerable<Course>> GetCourses(string id);
    Task<IEnumerable<Assignment>> GetAssignments(string id);
    Task<IEnumerable<Attendance>> GetAttendances(string id);
  }
}