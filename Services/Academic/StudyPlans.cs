using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using skooltrak_api.Models;
using MongoDB.Driver;

namespace skooltrak_api.Services
{
    public class StudyPlansService : IStudyPlansService
    {
        private readonly IDbContext _context;

        public StudyPlansService(IDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<StudyPlan>> Get(UserInfo user)
        {
            return await _context.StudyPlans.Find(x => true).ToListAsync();
        }

        public async Task<StudyPlan> Get(string id, UserInfo user)
        {
            return await _context.StudyPlans.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<StudyPlan> Create(StudyPlan plan, UserInfo user)
        {
            await _context.StudyPlans.InsertOneAsync(plan);
            return plan;
        }

        public async Task Update(string id, StudyPlan plan, UserInfo user)
        {
            var update = Builders<StudyPlan>.Update
                .Set(x => x.Name, plan.Name)
                .Set(x => x.Level, plan.Level)
                .Set(x => x.Degree, plan.Degree)
                .Set(x => x.Preschool, plan.Preschool)
                .Set(x => x.Description, plan.Description)
                .Set(x => x.Active, plan.Active)
                .Set(x => x.Skills, plan.Skills)
                .Set(x => x.EnrollCharges, plan.EnrollCharges)
                .Set(x => x.MonthlyCost, plan.MonthlyCost)
                .Set(x => x.HasUser, plan.HasUser)
                .Set(x => x.ModificateDate, DateTime.Now);

            var updateCourse = Builders<Course>.Update.Set(x => x.Plan, plan);

            await _context.StudyPlans.UpdateOneAsync(x => x.Id == plan.Id, update);
            await _context.Courses.UpdateManyAsync(x => x.Plan.Id == id, updateCourse);
        }

        public async Task Remove(string id, UserInfo user)
        {
            await _context.StudyPlans.DeleteOneAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Course>> GetCourses(string id)
        {
            return await _context.Courses.Find(x => x.Plan.Id == id).ToListAsync();
        }

        public async Task<IEnumerable<ClassGroup>> GetGroups(string id)
        {
            return await _context.Groups.Find(x => x.StudyPlan.Id == id).Project<ClassGroup>(Builders<ClassGroup>.Projection.Expression(item => new ClassGroup()
            {
                Id = item.Id,
                SchoolId = item.SchoolId,
                Level = item.Level,
                Name = item.Name,
                Counselor = item.Counselor,
                StudyPlan = item.StudyPlan,
                CreateDate = item.CreateDate,
                ModificateDate = item.ModificateDate,
                Schedule = item.Schedule,
                StudentsCount = GetCount(item.Id)
            })).ToListAsync();
        }

        public long GetCount(string id)
        {
            return _context.Students.Find(x => x.Group.Id == id).CountDocuments();
        }

        public async Task CopyCourses(string id, string sourceId)
        {
            var plan = await _context.StudyPlans.Find(x => x.Id == id).FirstOrDefaultAsync();
            var courses = await _context.Courses.Find(x => x.Plan.Id == sourceId).Project<Course>(Builders<Course>.Projection.Expression(x => new Course
            {
                Subject = x.Subject,
                Plan = plan,
                WeeklyHours = x.WeeklyHours,
                ParentSubject = x.ParentSubject

            })).ToListAsync();
            await _context.Courses.DeleteManyAsync(x => x.Plan.Id == id);
            await _context.Courses.InsertManyAsync(courses);
        }

        public async Task CopyCharges(string id, string sourceId)
        {
            var sourcePlan = await _context.StudyPlans.Find(x => x.Id == sourceId).FirstOrDefaultAsync();
            var update = Builders<StudyPlan>.Update
                .Set(x => x.EnrollCharges, sourcePlan.EnrollCharges);

            await _context.StudyPlans.UpdateOneAsync(x => x.Id == id, update);
        }

        public async Task<IEnumerable<EvaluationArea>> GetEvaluations(string id)
        {
            return await _context.EvaluationAreas.Find(x => x.Plan.Id == id).ToListAsync();
        }

        public async Task UpdateArea(string id, EvaluationArea area)
        {
            var update = Builders<EvaluationArea>.Update
                        .Set(x => x.Name, area.Name)
                        .Set(x => x.Description, area.Description)
                        .Set(x => x.Color, area.Color)
                        .Set(x => x.Icon, area.Icon)
                        .Set(x => x.Items, area.Items);
            var evaluationUpdate = Builders<PreScholarEvaluation>.Update.Set(x => x.Area, new ReferenceArea()
            {
                Id = area.Id,
                Name = area.Name,
                Description = area.Description,
                Color = area.Color,
                Icon = area.Icon
            });
            await _context.EvaluationAreas.UpdateOneAsync(x => x.Id == id, update);
            await _context.PreScholarEvaluations.UpdateManyAsync(x => x.Area.Id == id, evaluationUpdate);
        }

        public async Task AddArea(string id, EvaluationArea area)
        {
            var plan = await _context.StudyPlans.Find(x => x.Id == id).FirstOrDefaultAsync();
            if (plan == null)
            {
                return;
            }

            area.Plan = new Reference() { Id = plan.Id, Name = plan.Name };
            await _context.EvaluationAreas.InsertOneAsync(area);
        }

        public async Task DeleteArea(string id)
        {
            await _context.EvaluationAreas.DeleteOneAsync(x => x.Id == id);
        }
    }

    public interface IStudyPlansService : IContextService<StudyPlan, StudyPlan>
    {
        Task<IEnumerable<EvaluationArea>> GetEvaluations(string id);
        Task UpdateArea(string id, EvaluationArea area);
        Task AddArea(string id, EvaluationArea area);
        Task DeleteArea(string id);
        Task<IEnumerable<Course>> GetCourses(string id);
        Task<IEnumerable<ClassGroup>> GetGroups(string id);
        Task CopyCourses(string id, string sourceId);
        Task CopyCharges(string id, string sourceId);
    }
}