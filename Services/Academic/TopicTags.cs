using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class TopicTagsService : ITopicTagsService
	{
		private readonly IDbContext _context;

		public TopicTagsService(IDbContext context)
		{
			_context = context;
		}

		public async Task<TopicTag> Create(TopicTag tag, UserInfo user)
		{
			if (await ExistTag(tag.Name))
			{
				return (await GetByName(tag.Name));
			}
			tag.Name = tag.Name.ToLower();
			await _context.Tags.InsertOneAsync(tag);
			return tag;
		}

		public async Task<IEnumerable<TopicTag>> Get(UserInfo user)
		{
			return await _context.Tags.Find(x => true).ToListAsync();
		}

		public async Task<TopicTag> Get(string id, UserInfo user)
		{
			return await _context.Tags.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task<TopicTag> GetByName(string text)
		{
			text = text.ToLower();
			return await _context.Tags.Find(x => x.Name == text).FirstOrDefaultAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.Tags.DeleteOneAsync(x => x.Id == id);
		}

		public async Task<IEnumerable<TopicTag>> SearchByName(string text)
		{
			return await _context.Tags.Find(x => x.Name.Contains(text.ToLower())).ToListAsync();
		}

		public async Task Update(string id, TopicTag tag, UserInfo user)
		{
			tag.Name = tag.Name.ToLower();
			await _context.Tags.ReplaceOneAsync(x => x.Id == id, tag);
		}

		async Task<bool> ExistTag(string text)
		{
			text = text.ToLower();
			return await _context.Tags.Find(x => x.Name == text).AnyAsync();
		}
	}

	public interface ITopicTagsService : IContextService<TopicTag, TopicTag>
	{
		Task<IEnumerable<TopicTag>> SearchByName(string text);
		Task<TopicTag> GetByName(string text);
	}
}