using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class AssignmentTypesService : IAssignmentTypesService
	{
		private readonly IDbContext _context;

		public AssignmentTypesService(IDbContext context)
		{
			_context = context;
		}

		public async Task<AssignmentType> Create(AssignmentType type, UserInfo user)
		{
			await _context.AssignmentTypes.InsertOneAsync(type);
			return type;
		}

		public async Task<IEnumerable<AssignmentType>> Get(UserInfo user)
		{
			return await _context.AssignmentTypes.Find(x => true).ToListAsync();
		}

		public async Task<AssignmentType> Get(string id, UserInfo user)
		{
			return await _context.AssignmentTypes.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.AssignmentTypes.DeleteOneAsync(x => x.Id == id);
		}

		public async Task Update(string id, AssignmentType type, UserInfo user)
		{
			await _context.AssignmentTypes.ReplaceOneAsync(x => x.Id == id, type);
		}
	}

	public interface IAssignmentTypesService : IContextService<AssignmentType, AssignmentType> { }
}