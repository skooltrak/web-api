using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using MongoDB.Driver;
using skooltrak_api.Models;
using skooltrak_api.Shared;

namespace skooltrak_api.Services
{
    public class ActivitiesService : IActivitiesService
    {
        private readonly IDbContext _context;
        private readonly Mailer mailer;
        private readonly IHubContext<ActivityHub> _hub;
        public ActivitiesService(IDbContext context, IHubContext<ActivityHub> hub)
        {
            _context = context;
            _hub = hub;
            mailer = new Mailer();
        }

        public Task<Activity> Create(Activity element, UserInfo user)
        {
            throw new System.NotImplementedException();
        }

        public Task<IEnumerable<Activity>> Get(UserInfo user)
        {
            throw new System.NotImplementedException();
        }

        public Task<Activity> Get(string id, UserInfo user)
        {
            throw new System.NotImplementedException();
        }


        public async Task NewDocument(Document document)
        {
            Course course = await _context.Courses.Find(x => x.Id == document.Course.Id).FirstOrDefaultAsync();
            course.Teachers.ForEach(async teacher =>
            {
                var info = await _context.Teachers.Find(x => x.Id == teacher.Id).FirstOrDefaultAsync();
                var user = await GetTeacherUser(teacher.Id);
                Activity activity = new Activity()
                {
                    Course = new CourseReference()
                    {
                        Id = course.Id,
                        Subject = course.Subject,
                        ParentSubject = course.ParentSubject,
                        Color = course.Color,
                        Icon = course.Icon
                    },
                    CreatedBy = document.CreateUser,
                    Type = (int)ActivityEnum.Document,
                    Private = false,
                    CreateDate = document.CreateDate,
                    Description = document.CreateUser.DisplayName + " subió un archivo"
                };
                if (document.CreateUser.Role.Code == (int)RoleEnum.Student)
                {
                    activity.Private = true;
                }
                await _context.Activities.InsertOneAsync(activity);
            });
        }

        public async Task NewContent(CourseContent content)
        {
            Course course = await _context.Courses.Find(x => x.Id == content.Course.Id).FirstOrDefaultAsync();
            course.Teachers.ForEach(async teacher =>
            {
                Activity activity = new Activity()
                {
                    Course = new CourseReference()
                    {
                        Id = course.Id,
                        Subject = course.Subject,
                        ParentSubject = course.ParentSubject,
                        Color = course.Color,
                        Icon = course.Icon
                    },
                    CreatedBy = content.CreateUser,
                    Type = (int)ActivityEnum.Content,
                    Private = false,
                    CreateDate = content.CreateDate,
                    Description = "El docente " + content.CreateUser.DisplayName + " subió un nuevo contenido"
                };
                await _context.Activities.InsertOneAsync(activity);
            });
        }

        public Task Remove(string id, UserInfo user)
        {
            throw new System.NotImplementedException();
        }

        private async Task<User> GetTeacherUser(string id)
        {
            var teacher = await _context.Teachers.Find(x => x.Id == id).FirstOrDefaultAsync();
            return await _context.Users.Find(x => x.Id == teacher.UserId).FirstOrDefaultAsync();
        }

        public Task Update(string id, Activity element, UserInfo user)
        {
            throw new System.NotImplementedException();
        }

        public async Task ReloadActivity()
        {
            /* var documents = await _context.Documents.Find(x => x.Course != null).ToListAsync();
			documents.ForEach(document =>
			{
				NewDocument(document);
			}); */

            var contents = await _context.CourseContents.Find(x => true).ToListAsync();
            contents.ForEach(content =>
            {
                _ = NewContent(content);
            });
        }
    }

    public interface IActivitiesService : IContextService<Activity, Activity>
    {
        Task NewDocument(Document document);
        Task ReloadActivity();
    }
}