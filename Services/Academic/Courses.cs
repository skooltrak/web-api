using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using skooltrak_api.Models;
using MongoDB.Driver;
using System.Linq;

namespace skooltrak_api.Services
{
  public class CoursesService : ICoursesService
  {
    private readonly IDbContext _context;

    public CoursesService(IDbContext context)
    {
      _context = context;
    }

    public async Task<Course> Create(Course course, UserInfo user)
    {
      var period = await _context.Periods.Find(x => true).SortBy(x => x.Sort).FirstOrDefaultAsync();
      course.CurrentPeriod = period;
      await _context.Courses.InsertOneAsync(course);
      return course;
    }

    public async Task<Course> Get(string id, UserInfo user)
    {
      return await _context.Courses.Find(x => x.Id == id).FirstOrDefaultAsync();
    }

    public async Task<IEnumerable<Course>> Get(UserInfo user)
    {
      return await _context.Courses.Find(x => true).ToListAsync();
    }

    public async Task<IEnumerable<Assignment>> GetAssignments(string id)
    {
      return await _context.Assignments.Find(x => x.Course.Id == id).ToListAsync();
    }

    public async Task<IEnumerable<Assignment>> GetAssignments(string id, DateTime startDate, DateTime endDate)
    {
      return await _context.Assignments.Find(x => x.Course.Id == id && (x.StartDate >= startDate && x.StartDate <= endDate) || (x.DueDate >= endDate && x.DueDate <= endDate)).ToListAsync();
    }

    public async Task<IEnumerable<Attendance>> GetAttendances(string id, int year = 2022)
    {
      return await _context.Attendance.Find(x => x.Course.Id == id && x.Year == year).ToListAsync();
    }

    public async Task<IEnumerable<CourseContent>> GetContent(string id)
    {
      return await _context.CourseContents.Find(x => x.Course.Id == id).SortByDescending(x => x.CreateDate).ToListAsync();
    }

    public async Task<IEnumerable<Document>> GetDocuments(string id, UserInfo user)
    {
      if (user.Role.Code == (int)RoleEnum.Student || user.Role.Code == (int)RoleEnum.Parent)
      {
        return await _context.Documents.Find(x => x.Course.Id == id && (x.CreateUser.Role.Code == (int)RoleEnum.Teacher || x.CreateUser.Id == user.Id)).SortByDescending(x => x.CreateDate).ToListAsync();
      }
      return await _context.Documents.Find(x => x.Course.Id == id).SortByDescending(x => x.CreateDate).ToListAsync();
    }

    public async Task<IEnumerable<Forum>> GetForums(string id)
    {
      return await _context.Forums.Find(x => x.Course.Id == id).SortByDescending(x => x.LastPost).ToListAsync();
    }

    public async Task<IEnumerable<Grade>> GetGrades(string id)
    {
      return await _context.Grades.Find(x => x.Course.Id == id && x.Period.Id == null).SortByDescending(x => x.CreateDate).ToListAsync();
    }

    public async Task<IEnumerable<ClassGroup>> GetGroups(string id)
    {
      var course = await _context.Courses.Find(x => x.Id == id).FirstOrDefaultAsync();
      return await _context.Groups.Find(x => x.StudyPlan.Id == course.Plan.Id).ToListAsync();
    }

    public async Task<IEnumerable<CourseMessage>> GetMessages(string id)
    {
      return await _context.CourseMessages.Find(x => x.Course.Id == id).SortByDescending(x => x.CreateDate).ToListAsync();
    }

    public double GetScore(string courseId, string studentId)
    {
      var periods = _context.Periods.Find(x => true).ToList();
      var scores = new List<double>();
      periods.ForEach(period =>
      {
        var score = GetPeriodScore(courseId, studentId, period.Id);
        if (score > 0)
        {
          scores.Add(score);
        }
      });

      if (scores.Count > 0)
      {
        return scores.Average();
      }
      return 0;
    }

    public double GetPeriodScore(string courseId, string studentId, string periodId)
    {
      var course = _context.Courses.Find(x => x.Id == courseId).FirstOrDefault();
      double totalWeighting = 0;
      double score = 0;
      var grades = _context.StudentGrades.Find(x => x.Student.Id == studentId && x.Course.Id == courseId && x.Period.Id == periodId).ToList();
      if (grades.Any(x => x.Bucket.Id == 1))
      {
        totalWeighting = totalWeighting + course.Buckets.Find(x => x.Id == 1).Weighting;
      }
      if (grades.Any(x => x.Bucket.Id == 2))
      {
        totalWeighting = totalWeighting + course.Buckets.Find(x => x.Id == 2).Weighting;
      }
      if (grades.Any(x => x.Bucket.Id == 3))
      {
        totalWeighting = totalWeighting + course.Buckets.Find(x => x.Id == 3).Weighting;
      }
      if (grades.Count > 0)
      {
        course.Buckets.ForEach(bucket =>
        {
          var current = grades.Where(x => x.Bucket.Id == bucket.Id).Select(x => x.Score).ToList();
          if (current.Count > 0)
          {
            var weighting = current.Average();
            score = score + (weighting * (bucket.Weighting / totalWeighting));
          }
        });
      }
      return score;
    }

    public async Task<IEnumerable<StudentGrade>> GetStudentGrades(string id, string periodId)
    {
      return await _context.StudentGrades.Find(x => x.Course.Id == id && x.Period.Id == periodId).SortBy(x => x.Bucket.Id).ThenBy(x => x.CreateDate).ThenBy(x => x.Student.Name).ToListAsync();
    }

    public async Task<IEnumerable<Student>> GetStudents(string id)
    {
      var course = await _context.Courses.Find(x => x.Id == id).FirstOrDefaultAsync();
      var periods = await _context.Periods.Find(x => true).ToListAsync();
      var students = await _context.Students.Find(x => x.Plan.Id == course.Plan.Id && !x.Temporary && x.Active).SortBy(x => x.Surname).ThenBy(x => x.FirstName).ToListAsync();
      students.AddRange(await _context.Students.Find(x => x.Courses.Any(y => y.Id == id)).ToListAsync());
      students.ForEach(student =>
      {
        var scores = new List<double>();
        periods.ForEach(period =>
        {
          var score = GetScore(course.Id, student.Id, period.Id);
          if (score > 0)
          {
            scores.Add(score);
          }
        });
        if (scores.Count > 0)
        {
          student.Score = TruncateDecimal(scores.Average(), 1);
        }
      });
      return students;
    }

    public double GetScore(string courseId, string studentId, string periodId)
    {
      var course = _context.Courses.Find(x => x.Id == courseId).FirstOrDefault();
      double totalWeighting = 0;
      double score = 0;
      List<StudentGrade> grades;
      if (periodId == "undefined" || periodId == null)
      {
        grades = _context.StudentGrades.Find(x => x.Student.Id == studentId && x.Course.Id == courseId).ToList();
      }
      else
      {
        grades = _context.StudentGrades.Find(x => x.Student.Id == studentId && x.Course.Id == courseId && x.Period.Id == periodId).ToList();
      }
      if (grades.Any(x => x.Bucket.Id == 1))
      {
        totalWeighting = totalWeighting + course.Buckets.Find(x => x.Id == 1).Weighting;
      }
      if (grades.Any(x => x.Bucket.Id == 2))
      {
        totalWeighting = totalWeighting + course.Buckets.Find(x => x.Id == 2).Weighting;
      }
      if (grades.Any(x => x.Bucket.Id == 3))
      {
        totalWeighting = totalWeighting + course.Buckets.Find(x => x.Id == 3).Weighting;
      }
      if (grades.Count > 0)
      {
        course.Buckets.ForEach(bucket =>
        {
          var current = grades.Where(x => x.Bucket.Id == bucket.Id).Select(x => x.Score).ToList();
          if (current.Count > 0)
          {
            var weighting = current.Average();
            score = score + (weighting * (bucket.Weighting / totalWeighting));
          }
        });
      }
      return TruncateDecimal(score, 1);
    }

    private double TruncateDecimal(double value, int precision)
    {
      double step = Math.Pow(10, precision);
      double tmp = Math.Truncate(step * value);
      return tmp / step;
    }

    public async Task<IEnumerable<Topic>> GetTopics(string id)
    {
      return await _context.Topics.Find(x => x.Course.Id == id).ToListAsync();
    }

    public async Task<IEnumerable<Video>> GetVideos(string id, UserInfo user)
    {
      return await _context.Videos.Find(x => x.Courses.Any(y => y.Id == id) && (user.Role.Code == (int)RoleEnum.Teacher || (x.Published))).SortByDescending(x => x.CreatedDate).ToListAsync();
    }

    public async Task Remove(string id, UserInfo user)
    {
      await _context.Courses.DeleteOneAsync(x => x.Id == id);
    }

    public async Task SetPeriod(Period period)
    {
      if (period.Id == null)
      {
        period = await _context.Periods.Find(x => true).SortBy(x => x.Sort).FirstOrDefaultAsync();
      }
      var update = Builders<Course>.Update.Set(x => x.CurrentPeriod, period);
      await _context.Courses.UpdateManyAsync(x => true, update);
    }

    public async Task<Course> ClosePeriod(Course course)
    {
      var newPeriod = await _context.Periods.Find(x => x.Sort == course.CurrentPeriod.Sort + 1).FirstOrDefaultAsync();
      if (newPeriod != null)
      {
        var updateCourse = Builders<Course>.Update.Set(x => x.CurrentPeriod, newPeriod);
        await _context.Courses.UpdateOneAsync(x => x.Id == course.Id, updateCourse);
      }
      return course;
    }

    public async Task Update(string id, Course course, UserInfo user)
    {
      var old = await _context.Courses.Find(x => x.Id == id).FirstOrDefaultAsync();
      course.Buckets.ForEach(bucket =>
      {
        if (bucket.Weighting != old.Buckets.Find(x => x.Id == bucket.Id).Weighting)
        {
          var updateStudentsGrade = Builders<StudentGrade>.Update.Set(x => x.Bucket, bucket);
          _context.StudentGrades.UpdateManyAsync(x => x.Course.Id == course.Id && x.Bucket.Id == bucket.Id, updateStudentsGrade);
          var updateGrade = Builders<Grade>.Update.Set(x => x.Bucket, bucket);
          _context.Grades.UpdateManyAsync(x => x.Course.Id == course.Id && x.Bucket.Id == bucket.Id, updateGrade);
        }
      });
      var update = Builders<Course>.Update
        .Set(x => x.Subject, course.Subject)
        .Set(x => x.Plan, course.Plan)
        .Set(x => x.RelatedCourse, course.RelatedCourse)
        .Set(x => x.Buckets, course.Buckets)
        .Set(x => x.ParentSubject, course.ParentSubject)
        .Set(x => x.Teachers, course.Teachers)
        .Set(x => x.WeeklyHours, course.WeeklyHours)
        .Set(x => x.Active, course.Active)
        .Set(x => x.ModificateDate, DateTime.Now);
      await _context.Courses.UpdateOneAsync(x => x.Id == id, update);

    }

    public async Task<IEnumerable<Grade>> GetClosedGrades(string id, string periodId)
    {
      return await _context.Grades.Find(x => x.Course.Id == id && x.Period.Id == periodId).ToListAsync();
    }

    public async Task SetGradePeriod()
    {
      var courses = await _context.Courses.Find(x => true).ToListAsync();
      courses.ForEach(async course =>
      {
        var update = Builders<Grade>.Update.Set(x => x.Period, course.CurrentPeriod);
        var studentGrades = Builders<StudentGrade>.Update.Set(x => x.Period, course.CurrentPeriod);
        await _context.Grades.UpdateManyAsync(x => x.Course.Id == course.Id && x.Period.Id == null, update);
        await _context.StudentGrades.UpdateManyAsync(x => x.Course.Id == course.Id && x.Period.Id == null, studentGrades);
      });
    }

    public async Task ChangeColor(string courseId, string color)
    {
      var update = Builders<Course>.Update.Set(x => x.Color, color);
      var assignments = Builders<Assignment>.Update.Set(x => x.Course.Color, color);
      await _context.Courses.UpdateOneAsync(x => x.Id == courseId, update);
      await _context.Assignments.UpdateManyAsync(x => x.Course.Id == courseId, assignments);
    }

    public async Task ChangeIcon(string courseId, string icon)
    {
      var update = Builders<Course>.Update.Set(x => x.Icon, icon);
      var assignments = Builders<Assignment>.Update.Set(x => x.Course.Icon, icon);
      await _context.Courses.UpdateOneAsync(x => x.Id == courseId, update);
      await _context.Assignments.UpdateManyAsync(x => x.Course.Id == courseId, assignments);
    }
    public async Task OpenPeriod(string id, Period period)
    {
      var update = Builders<Course>.Update.Set(x => x.CurrentPeriod, period);
      await _context.Courses.UpdateOneAsync(x => x.Id == id, update);
    }

    public async Task SetPlan()
    {
      var courses = await _context.Courses.Find(x => true).ToListAsync();
      courses.ForEach(async course =>
      {
        var plan = await _context.StudyPlans.Find(x => x.Id == course.Plan.Id).FirstOrDefaultAsync();
        var update = Builders<Course>.Update.Set(x => x.Plan, plan);

        await _context.Courses.UpdateOneAsync(x => x.Id == course.Id, update);
      });
    }
  }

  public interface ICoursesService : IContextService<Course, Course>
  {

    Task<IEnumerable<Topic>> GetTopics(string id);
    Task SetPlan();
    double GetScore(string courseId, string studentId);
    double GetPeriodScore(string courseId, string studentId, string periodId);
    Task<IEnumerable<Video>> GetVideos(string id, UserInfo user);
    Task<IEnumerable<CourseMessage>> GetMessages(string id);
    Task<IEnumerable<Assignment>> GetAssignments(string id);
    Task<IEnumerable<Assignment>> GetAssignments(string id, DateTime startDate, DateTime endDate);
    Task<IEnumerable<Student>> GetStudents(string id);
    Task<IEnumerable<CourseContent>> GetContent(string id);
    Task OpenPeriod(string id, Period period);
    Task<IEnumerable<ClassGroup>> GetGroups(string id);
    Task<IEnumerable<Grade>> GetGrades(string id);
    Task<IEnumerable<Grade>> GetClosedGrades(string id, string periodId);
    Task SetPeriod(Period period);
    Task<Course> ClosePeriod(Course course);
    Task ChangeColor(string courseId, string color);
    Task ChangeIcon(string courseId, string icon);
    Task SetGradePeriod();
    Task<IEnumerable<StudentGrade>> GetStudentGrades(string id, string periodId);
    Task<IEnumerable<Document>> GetDocuments(string id, UserInfo user);
    Task<IEnumerable<Attendance>> GetAttendances(string id, int year = 2022);
    Task<IEnumerable<Forum>> GetForums(string id);
  }
}