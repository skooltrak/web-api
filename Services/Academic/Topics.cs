using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using skooltrak_api.Models;
using MongoDB.Driver;

namespace skooltrak_api.Services
{
	public class TopicsService : ITopicsService
	{
		private readonly IDbContext _context;

		public TopicsService(IDbContext context)
		{
			_context = context;
		}

		public async Task<Topic> Create(Topic topic, UserInfo user)
		{
			await _context.Topics.InsertOneAsync(topic);
			return topic;
		}

		public async Task<Topic> Get(string id, UserInfo user)
		{
			return await _context.Topics.Find(x => x.Id == id).FirstOrDefaultAsync();
		}

		public async Task<IEnumerable<Topic>> Get(UserInfo user)
		{
			return await _context.Topics.Find(x => true).ToListAsync();
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.Topics.DeleteOneAsync(x => x.Id == id);
		}

		public async Task Update(string id, Topic topic, UserInfo user)
		{
			var update = Builders<Topic>.Update
					.Set(x => x.DueDate, topic.DueDate)
					.Set(x => x.Course.Id, topic.Course.Id)
					.Set(x => x.Description, topic.Description)
					.Set(x => x.StartDate, topic.StartDate)
					.Set(x => x.Teacher.Id, topic.Teacher.Id)
					.Set(x => x.Name, topic.Name)
					.Set(x => x.ModificateDate, DateTime.Now);

			await _context.Topics.UpdateOneAsync(x => x.Id == id, update);
		}
	}

	public interface ITopicsService : IContextService<Topic, Topic>
	{
	}
}