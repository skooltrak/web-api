using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
    public class CleaningService : ICleaningService
    {
        private readonly IDbContext _context;

        public CleaningService(IDbContext context)
        {
            _context = context;
        }

        public async Task ExecuteClean(List<CleaningItem> items)
        {
            if (items.Exists(x => x.Code == "files"))
            {
                await _context.Documents.DeleteManyAsync(x => true);
				await _context.Videos.DeleteManyAsync(x => true);
                await _context.bucket.DropAsync();
            }
            if (items.Exists(x => x.Code == "assignments"))
            {
                await _context.Assignments.DeleteManyAsync(x => true);
            }
            if (items.Exists(x => x.Code == "courseContents"))
            {
                await _context.CourseContents.DeleteManyAsync(x => true);
            }
            if (items.Exists(x => x.Code == "courseMessages"))
            {
                await _context.CourseMessages.DeleteManyAsync(x => true);
            }

            if (items.Exists(x => x.Code == "examAssignations"))
            {
                await _context.ExamAssignations.DeleteManyAsync(x => true);
                await _context.ExamResults.DeleteManyAsync(x => true);
            }
            if (items.Exists(x => x.Code == "grades"))
            {
                await _context.Grades.DeleteManyAsync(x => true);
                await _context.StudentGrades.DeleteManyAsync(x => true);
            }
            if (items.Exists(x => x.Code == "forums"))
            {
                await _context.ForumPosts.DeleteManyAsync(x => true);
                await _context.Forums.DeleteManyAsync(x => true);
            }

            if (items.Exists(x => x.Code == "periods"))
            {
                var period = await _context.Periods.Find(x => true).SortBy(x => x.Sort).FirstOrDefaultAsync();
                var update = Builders<Course>.Update.Set(x => x.CurrentPeriod, period);
                await _context.Courses.UpdateManyAsync(x => true, update);
            }


			if (items.Exists(x => x.Code == "mail"))
            {
                await _context.MessageInboxes.DeleteManyAsync(x => true);
                await _context.Messages.DeleteManyAsync(x => true);
            }
        }

        public async Task<IEnumerable<CleaningItem>> GetItems()
        {
            var list = new List<CleaningItem>();
            list.Add(new CleaningItem()
            {
                Code = "files",
                Description = "Files",
                Value = await _context.Documents.Find(x => true).CountDocumentsAsync()
            });

			list.Add(new CleaningItem()
            {
                Code = "mail",
                Description = "Messaging",
                Value = await _context.Messages.Find(x => true).CountDocumentsAsync()
            });

            list.Add(new CleaningItem()
            {
                Code = "assignments",
                Description = "Assignments",
                Value = await _context.Assignments.Find(x => true).CountDocumentsAsync()
            });

            list.Add(new CleaningItem()
            {
                Code = "courseContents",
                Description = "Course contents",
                Value = await _context.CourseContents.Find(x => true).CountDocumentsAsync()
            });

            list.Add(new CleaningItem()
            {
                Code = "courseMessages",
                Description = "Course messages",
                Value = await _context.CourseMessages.Find(x => true).CountDocumentsAsync()
            });

            list.Add(new CleaningItem()
            {
                Code = "examAssignations",
                Description = "Exam assignations",
                Value = await _context.ExamAssignations.Find(x => true).CountDocumentsAsync()
            });

            list.Add(new CleaningItem()
            {
                Code = "forums",
                Description = "Forums",
                Value = await _context.ForumPosts.Find(x => true).CountDocumentsAsync()
            });

            list.Add(new CleaningItem()
            {
                Code = "periods",
                Description = "Periods",
                Value = 3
            });

            list.Add(new CleaningItem()
            {
                Code = "grades",
                Description = "Grades",
                Value = await _context.Grades.Find(x => true).CountDocumentsAsync()
            });

            return list;
        }
    }

    public interface ICleaningService
    {
        Task<IEnumerable<CleaningItem>> GetItems();

        Task ExecuteClean(List<CleaningItem> items);
    }

    public class CleaningItem
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public double Value { get; set; }
    }
}