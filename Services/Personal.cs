using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class PersonalService : IPersonalService
	{
		private readonly IDbContext _context;

		public PersonalService(IDbContext context)
		{
			_context = context;
		}

		public async Task<IEnumerable<CourseContent>> GetContents(UserInfo user)
		{
			return await _context.CourseContents.Find(x => x.CreateUser.Id == user.Id).ToListAsync();
		}

		public async Task<IEnumerable<Document>> GetDocuments(UserInfo user)
		{
			return await _context.Documents.Find(x => x.CreateUser.Id == user.Id).SortByDescending(x => x.CreateDate).ToListAsync();
		}

		public async Task<IEnumerable<ForumPost>> GetPosts(UserInfo user)
		{
			return await _context.ForumPosts.Find(x => x.CreatedBy.Id == user.Id).SortByDescending(x => x.CreateDate).ToListAsync();
		}


	}

	public interface IPersonalService
	{
		Task<IEnumerable<Document>> GetDocuments(UserInfo user);
		Task<IEnumerable<ForumPost>> GetPosts(UserInfo user);
		Task<IEnumerable<CourseContent>> GetContents(UserInfo user);
	}
}