using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
    public class PreScholarService : IPreScholarService
    {
        private readonly IDbContext _context;
        public PreScholarService(IDbContext context)
        {
            _context = context;
        }

        public Task<PreScholarEvaluation> Create(PreScholarEvaluation element, UserInfo user)
        {
            throw new System.NotImplementedException();
        }

        public async Task<List<PreScholarEvaluation>> GetEvaluations(string id)
        {
            return await _context.PreScholarEvaluations.Find(x => x.Student.Id == id).ToListAsync();
        }

        public Task<IEnumerable<PreScholarEvaluation>> Get(UserInfo user)
        {
            throw new System.NotImplementedException();
        }

        public Task<PreScholarEvaluation> Get(string id, UserInfo user)
        {
            throw new System.NotImplementedException();
        }

        public void InitValues()
        {
            var plans = _context.StudyPlans.Find(x => x.Preschool).ToList();
            var periods = _context.Periods.Find(x => true).ToList();

            plans.ForEach(plan =>
            {
                var students = _context.Students.Find(x => x.Plan.Id == plan.Id && x.Active).ToList();
                var areas = _context.EvaluationAreas.Find(x => x.Plan.Id == plan.Id).ToList();
                students.ForEach(student =>
                {
                    areas.ForEach(area =>
                    {
                        area.Items.ForEach(item =>
                        {
                            var current = _context.PreScholarEvaluations.
                                Find(x => x.Item.Name == item.Name
                                && x.Area.Id == area.Id && x.Student.Id == student.Id)
                                .FirstOrDefault();

                            if (current == null)
                            {
                                var newItem = new PreScholarEvaluation()
                                {
                                    Area = new ReferenceArea()
                                    {
                                        Id = area.Id,
                                        Name = area.Name,
                                        Color = area.Color,
                                        Description = area.Description,
                                        Icon = area.Icon
                                    },
                                    Student = new Reference()
                                    {
                                        Id = student.Id,
                                        Name = student.Name
                                    },
                                    Item = item
                                };
                                periods.ForEach(period =>
                                {
                                    newItem.Periods.Add(new EvaluationPeriod()
                                    {
                                        Period = period,
                                        Value = null
                                    });
                                });
                                _context.PreScholarEvaluations.InsertOne(newItem);
                            }
                        });
                    });
                });
            });
        }

        public Task Remove(string id, UserInfo user)
        {
            throw new System.NotImplementedException();
        }

        public async Task SetValue(EvaluationValue item)
        {
            var filter = Builders<PreScholarEvaluation>.Filter.And(
                Builders<PreScholarEvaluation>.Filter.Eq(x => x.Student.Id, item.StudentId),
                Builders<PreScholarEvaluation>.Filter.Eq(x => x.Area.Id, item.AreaId),
                Builders<PreScholarEvaluation>.Filter.Eq(x => x.Item.Name, item.ItemName),
                Builders<PreScholarEvaluation>.Filter.ElemMatch(x => x.Periods, x => x.Period.Id == item.PeriodId)
            );

            var update = Builders<PreScholarEvaluation>.Update.Set(x => x.Periods[-1].Value, item.Value);
            await _context.PreScholarEvaluations.UpdateOneAsync(filter, update);
        }

        public Task Update(string id, PreScholarEvaluation element, UserInfo user)
        {
            throw new System.NotImplementedException();
        }
    }

    public interface IPreScholarService : IContextService<PreScholarEvaluation, PreScholarEvaluation>
    {
        Task SetValue(EvaluationValue item);
        Task<List<PreScholarEvaluation>> GetEvaluations(string id);
        void InitValues();
    }

    public class EvaluationValue
    {
        public string StudentId { get; set; }
        public string AreaId { get; set; }
        public string ItemName { get; set; }
        public string PeriodId { get; set; }
        public string Value { get; set; }
    }
}