using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
	public class MessageInboxService : IMessageInboxService
	{
		private readonly IDbContext _context;

		public MessageInboxService(IDbContext context)
		{
			_context = context;
		}

		public Task<MessageInbox> Create(MessageInbox element, UserInfo user)
		{
			throw new System.NotImplementedException();
		}

		public async Task<IEnumerable<MessageInbox>> Get(UserInfo user)
		{
			return await _context.MessageInboxes.Find(x => x.Receiver.Id == user.Id).SortByDescending(x => x.ArrivalDate).ToListAsync();
		}

		public Task<MessageInbox> Get(string id, UserInfo user)
		{
			throw new System.NotImplementedException();
		}

		public async Task MarkAsRead(string id)
		{
			var update = Builders<MessageInbox>.Update.Set(x => x.Read, true);
			await _context.MessageInboxes.UpdateOneAsync(x => x.Id.ToString() == id, update);
		}

		public async Task Remove(string id, UserInfo user)
		{
			await _context.MessageInboxes.DeleteOneAsync(x => x.Id.ToString() == id);
		}

		public Task Update(string id, MessageInbox element, UserInfo user)
		{
			throw new System.NotImplementedException();
		}
	}

	public interface IMessageInboxService : IContextService<MessageInbox, MessageInbox>
	{
		Task MarkAsRead(string id);
	}
}