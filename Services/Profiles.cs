using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
    public class ProfilesService : IProfilesService
    {
        private readonly IDbContext _context;

        public ProfilesService(IDbContext context)
        {
            _context = context;
        }

        public async Task<IPerson> GetPerson(string id)
        {
            var user = await _context.Users.Find(x => x.Id == id).FirstOrDefaultAsync();
            if (user == null)
            {
                return new Teacher();
            }
            if (user.Role.Code == (int)RoleEnum.Student)
            {
                return await _context.Students.Find(x => x.UserId == user.Id).FirstOrDefaultAsync();
            }
            if (user.Role.Code == (int)RoleEnum.Teacher)
            {
                return await _context.Teachers.Find(x => x.UserId == user.Id).FirstOrDefaultAsync();
            }
            return new Teacher() { Email = user.Email };
        }
    }

    public interface IProfilesService
    {
        Task<IPerson> GetPerson(string user);

    }
}