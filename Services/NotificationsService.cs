using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using skooltrak_api.Models;
using skooltrak_api.Shared;

namespace skooltrak_api.Services
{
	public class NotificationsService
	{
		private readonly IHubContext<ActivityHub> _context;
		private readonly IHubContext<MessageHub> _messageHub;

		public NotificationsService()
		{
			_context = Startup.activityHubContext;
			_messageHub = Startup.messageHubContext;
		}

		public async Task SendMessage(MessageInbox message)
		{
			await Startup.messageHubContext.Clients.All.SendAsync(message.Receiver.Id, message);
		}
	}
}