using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace skooltrak_api.Services
{
    public class FilesService : IFilesService
    {
        private readonly IDbContext _context;

        public FilesService(IDbContext context)
        {
            _context = context;
        }

        public async Task<FileObject> Upload(IFormFile file)
        {
            var options = new GridFSUploadOptions()
            {
                Metadata = new BsonDocument()
                {
                    { "upload_date", DateTime.Now },
                    { "type", file.ContentType }
                }
            };


            using (var reader = new StreamReader((Stream)file.OpenReadStream()))
            {
                var stream = reader.BaseStream;
                var fileId = await _context.bucket.UploadFromStreamAsync(file.FileName, stream, options);
                var result = new FileObject { FileName = file.FileName, Id = fileId.ToString(), Size = file.Length, Type = file.ContentType };
                return result;
            }
        }

        public async Task<FileResponse> GetFile(string fileId)
        {
            var filter = Builders<GridFSFileInfo>.Filter.Eq("_id", ObjectId.Parse(fileId));
            var info = await (await _context.bucket.FindAsync(filter)).FirstOrDefaultAsync();
            if (info == null)
            {
                return new FileResponse { File = new byte[0] };
            }
            var content = await _context.bucket.DownloadAsBytesAsync(ObjectId.Parse(fileId));
            return new FileResponse { Info = info, File = content };
        }

        public async Task DeleteFile(string fileId)
        {
            await _context.bucket.DeleteAsync(ObjectId.Parse(fileId));
        }
    }

    public interface IFilesService
    {
        Task<FileObject> Upload(IFormFile file);
        Task<FileResponse> GetFile(string fileId);
        Task DeleteFile(string fileId);
    }

    public class FileResponse
    {
        public GridFSFileInfo Info { get; set; }
        public byte[] File { get; set; }
    }

    public class FileObject
    {
        public string FileName { get; set; }
        public string Id { get; set; }
        public long Size { get; set; }
        public string Type { get; set; }
    }
}