using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
    public class ChargesService : IChargesService
    {
        private readonly IDbContext _context;
        public ChargesService(IDbContext context)
        {
            _context = context;
        }

        public async Task<Charge> Create(Charge charge, UserInfo user)
        {
            charge.Balance = charge.Amount;
            await _context.Charges.InsertOneAsync(charge);
            _ = SetStudentBalances(charge.Student.Id);
            return charge;
        }

        public async Task<IEnumerable<Charge>> Get(UserInfo user)
        {
            return await _context.Charges.Find(x => true).ToListAsync();
        }

        public async Task<Charge> Get(string id, UserInfo user)
        {
            return await _context.Charges.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public IEnumerable<object> GetBalances()
        {
            var aggregate = _context.Charges.AsQueryable()
            .Where(x => x.Balance > 0 && (x.DueDate < DateTime.Now || x.StartDate < DateTime.Now))
            .GroupBy(x => x.Student)
            .Select(y => new { y.Key, sum = y.Sum(c => c.Balance) })
            .OrderBy(a => a.Key.Name).ToList();
            return aggregate;
        }

        public IEnumerable<object> GetDue()
        {
            var aggregate = _context.Charges.AsQueryable()
            .Where(x => x.Balance > 0 && x.DueDate < DateTime.Now)
            .GroupBy(x => x.Student)
            .Select(y => new { y.Key, sum = y.Sum(c => c.Balance) })
            .OrderByDescending(a => a.sum).ToList();
            return aggregate;
        }

        public double GetDueCharges()
        {
            return _context.Charges.AsQueryable()
                .Where(x => x.Balance > 0 && (x.DueDate < DateTime.Now || x.StartDate < DateTime.Now))
                .GroupBy(x => true)
                .Select(x => x.Sum(y => y.Balance))
                .FirstOrDefault();
        }

        public double GetTotalCharges(string id)
        {
            return _context.Charges.AsQueryable()
                .Where(x => x.Balance > 0 && (x.DueDate < DateTime.Now || x.StartDate < DateTime.Now) && x.Student.Id == id)
                .GroupBy(x => x.Student)
                .Select(x => x.Sum(y => y.Balance))
                .FirstOrDefault();
        }
        public double GetCurrentCharges(string id)
        {
            return _context.Charges.AsQueryable()
                .Where(x => x.Balance > 0 && (x.DueDate > DateTime.Now && x.StartDate < DateTime.Now) && x.Student.Id == id)
                .GroupBy(x => x.Student)
                .Select(x => x.Sum(y => y.Balance))
                .FirstOrDefault();
        }

        public double GetCurrentCharges()
        {
            return _context.Charges.AsQueryable()
                .Where(x => x.Balance > 0 && (x.DueDate > DateTime.Now && x.StartDate < DateTime.Now))
                .GroupBy(x => true)
                .Select(x => x.Sum(y => y.Balance))
                .FirstOrDefault();
        }

        public async Task Remove(string id, UserInfo user)
        {
            var charge = await _context.Charges.Find(x => x.Id == id).FirstOrDefaultAsync();
            _ = SetStudentBalances(charge.Student.Id);
            await _context.Charges.DeleteOneAsync(x => x.Id == id);
        }

        public async Task Update(string id, Charge charge, UserInfo user)
        {
            await _context.Charges.ReplaceOneAsync(x => x.Id == id, charge);
        }

        public async Task<IEnumerable<StudentBalance>> GetStudentBalances()
        {
            return await _context.Balances.Find(x => true).ToListAsync();
    
        }
        public async Task SetStudentBalances()
        {
            var items = await _context.Students.Find(x => x.Active).Project<StudentBalance>(Builders<Student>.Projection.Expression(item => new StudentBalance()
            {
                Student = new Reference()
                {
                    Id = item.Id,
                    Name = item.FullName,
                },
                Group = new GroupReference()
                {
                    Id = item.Group.Id,
                    Name = item.Group.Name,
                    Level = item.Group.Level,
                },
                Plan = item.Plan,
                DueAmount = GetTotalCharges(item.Id),
                CurrentAmount = GetCurrentCharges(item.Id)
            })).SortBy(x => x.Surname)
                .ThenBy(x => x.SecondSurname)
                .ThenBy(x => x.FirstName)
                .ThenBy(x => x.MiddleName).ToListAsync();

            _ = _context.Balances.InsertManyAsync(items);
        }

        public async Task SetStudentBalances(string id)
        {
            var student = await _context.Students.Find(x => x.Id == id).Project<StudentBalance>(Builders<Student>.Projection.Expression(item => new StudentBalance()
            {
                Student = new Reference()
                {
                    Id = item.Id,
                    Name = item.FullName,
                },
                Group = new GroupReference()
                {
                    Id = item.Group.Id,
                    Name = item.Group.Name,
                    Level = item.Group.Level,
                },
                Plan = item.Plan,
                DueAmount = GetTotalCharges(item.Id),
                CurrentAmount = GetCurrentCharges(item.Id)
            })).SortBy(x => x.Surname)
                .ThenBy(x => x.SecondSurname)
                .ThenBy(x => x.FirstName)
                .ThenBy(x => x.MiddleName).FirstOrDefaultAsync();
            if (student == null)
            {
                return;
            }

            await _context.Balances.ReplaceOneAsync(x => x.Student.Id == id, student);
        }
    }

    public interface IChargesService : IContextService<Charge, Charge>
    {
        IEnumerable<object> GetBalances();
        IEnumerable<object> GetDue();
        double GetCurrentCharges();
        double GetDueCharges();
        Task<IEnumerable<StudentBalance>> GetStudentBalances();

        Task SetStudentBalances();
        Task SetStudentBalances(string id);
    }
}