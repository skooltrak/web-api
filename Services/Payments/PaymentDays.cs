using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
    public class PaymentDaysService : IPaymentDaysService
    {
        private readonly IDbContext _context;
        public PaymentDaysService(IDbContext context)
        {
            _context = context;
        }

        public async Task<PaymentDay> Create(PaymentDay day, UserInfo user)
        {
            await _context.PaymentDays.InsertOneAsync(day);
            var students = await _context.Students.Find(x => x.Active).ToListAsync();
            students.ForEach(async student =>
            {
                var plan = await _context.StudyPlans.Find(x => x.Id == student.Plan.Id).FirstOrDefaultAsync();
                var existing = _context.Charges.Find(x => x.Student.Id == student.Id && x.MonthlyFee.Id == day.Id).FirstOrDefault();
                if (existing == null)
                {
                    var charge = new Charge()
                    {
                        Student = new Reference() { Id = student.Id, Name = student.FullName },
                        Description = day.Title,
                        StartDate = day.StartDate,
                        DueDate = day.DueDate,
                        MonthlyFee = new Reference() { Id = day.Id, Name = day.Title },
                        Amount = plan.MonthlyCost,
                        Balance = plan.MonthlyCost
                    };
                    await _context.Charges.InsertOneAsync(charge);
                }
            });
            return day;
        }

        public async Task CreateCreditNote(string id, UserInfo user)
        {
            var day = await _context.PaymentDays.Find(x => x.Id == id).ToListAsync();
            var charges = await _context.Charges.Find(x => x.MonthlyFee.Id == id && x.Balance > 0).ToListAsync();

            charges.ForEach(async charge =>
            {
                Payment payment = new Payment()
                {
                    Amount = charge.Amount,
                    Student = charge.Student,
                    CreditNote = true,
                    CreateUser = user,
                    PaymentDate = DateTime.Now,
                    Method = "Nota de crédito",
                };

                payment.Applications.Add(new PaymentApplication()
                {
                    Amount = charge.Amount,
                    Charge = charge
                });
                await _context.Payments.InsertOneAsync(payment);
            });

            var update = Builders<Charge>.Update.Set(x => x.Balance, 0);
            await _context.Charges.UpdateManyAsync(x => x.MonthlyFee.Id == id, update);
        }

        public async Task<IEnumerable<PaymentDay>> Get(UserInfo user)
        {
            return await _context.PaymentDays.Find(x => true).ToListAsync();
        }

        public async Task<PaymentDay> Get(string id, UserInfo user)
        {
            return await _context.PaymentDays.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task Remove(string id, UserInfo user)
        {
            await _context.PaymentDays.DeleteOneAsync(x => x.Id == id);
        }

        public async Task Update(string id, PaymentDay day, UserInfo user)
        {
            await _context.PaymentDays.ReplaceOneAsync(x => x.Id == id, day);
        }
    }

    public interface IPaymentDaysService : IContextService<PaymentDay, PaymentDay>
    {
        Task CreateCreditNote(string id, UserInfo user);
    }
}