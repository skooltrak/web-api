using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using skooltrak_api.Models;

namespace skooltrak_api.Services
{
    public class PaymentsService : IPaymentsService
    {
        private readonly IDbContext _context;

        public PaymentsService(IDbContext context)
        {
            _context = context;
        }

        public async Task<Payment> Create(Payment payment, UserInfo user)
        {
            payment.CreateUser = user;
            payment.Applications.ForEach(application =>
            {
                var update = Builders<Charge>.Update.Inc(x => x.Balance, application.Amount * -1).Set(x => x.PaymentDate, DateTime.Now);
                _context.Charges.UpdateOneAsync(x => x.Id == application.Charge.Id, update);
            });
            await _context.Payments.InsertOneAsync(payment);
            _ = SetStudentBalances(payment.Student.Id);
            return payment;
        }

        public async Task Remove(string id, UserInfo user)
        {
            var payment = await _context.Payments.Find(x => x.Id == id).FirstOrDefaultAsync();
            if (payment != null)
            {
                payment.Applications.ForEach(async application =>
                {
                    var update = Builders<Charge>.Update.Inc(x => x.Balance, application.Amount).Unset(x => x.PaymentDate);
                    await _context.Charges.UpdateManyAsync(x => x.Id == application.Charge.Id, update);
                });
            }
            _ = SetStudentBalances(payment.Student.Id);
            await _context.Payments.DeleteOneAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Payment>> Get(UserInfo user)
        {
            return await _context.Payments.Find(x => true).ToListAsync();
        }

        public async Task<Payment> Get(string id, UserInfo user)
        {
            return await _context.Payments.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<StudentBalance>> GetStudentBalances()
        {
            return await _context.Balances.Find(x => true).ToListAsync();
        }

        public double GetTotalCharges(string id)
        {
            return _context.Charges.AsQueryable()
                .Where(x => x.Balance > 0 && (x.DueDate < DateTime.Now || x.StartDate < DateTime.Now) && x.Student.Id == id)
                .GroupBy(x => x.Student)
                .Select(x => x.Sum(y => y.Balance))
                .FirstOrDefault();
        }
        public double GetCurrentCharges(string id)
        {
            return _context.Charges.AsQueryable()
                .Where(x => x.Balance > 0 && (x.DueDate > DateTime.Now && x.StartDate < DateTime.Now) && x.Student.Id == id)
                .GroupBy(x => x.Student)
                .Select(x => x.Sum(y => y.Balance))
                .FirstOrDefault();
        }

        public async Task Update(string id, Payment payment, UserInfo user)
        {
            await _context.Payments.ReplaceOneAsync(x => x.Id == id, payment);
        }

        public async Task SetStudentBalances()
        {
            var items = await _context.Students.Find(x => x.Active).Project<StudentBalance>(Builders<Student>.Projection.Expression(item => new StudentBalance()
            {
                Student = new Reference()
                {
                    Id = item.Id,
                    Name = item.FullName,
                },
                Group = new GroupReference()
                {
                    Id = item.Group.Id,
                    Name = item.Group.Name,
                    Level = item.Group.Level,
                },
                Plan = item.Plan,
                DueAmount = GetTotalCharges(item.Id),
                CurrentAmount = GetCurrentCharges(item.Id)
            })).SortBy(x => x.Surname)
                .ThenBy(x => x.SecondSurname)
                .ThenBy(x => x.FirstName)
                .ThenBy(x => x.MiddleName).ToListAsync();

            _ = _context.Balances.InsertManyAsync(items);
        }

        public async Task SetStudentBalances(string id)
        {
            var student = await _context.Students.Find(x => x.Id == id).Project<StudentBalance>(Builders<Student>.Projection.Expression(item => new StudentBalance()
            {
                Student = new Reference()
                {
                    Id = item.Id,
                    Name = item.FullName,
                },
                Group = new GroupReference()
                {
                    Id = item.Group.Id,
                    Name = item.Group.Name,
                    Level = item.Group.Level,
                },
                Plan = item.Plan,
                DueAmount = GetTotalCharges(item.Id),
                CurrentAmount = GetCurrentCharges(item.Id)
            })).SortBy(x => x.Surname)
                .ThenBy(x => x.SecondSurname)
                .ThenBy(x => x.FirstName)
                .ThenBy(x => x.MiddleName).FirstOrDefaultAsync();
            if (student == null)
            {
                return;
            }
            var update = Builders<StudentBalance>.Update
                .Set(x => x.CurrentAmount, student.CurrentAmount)
                .Set(x => x.DueAmount, student.DueAmount);
            await _context.Balances.UpdateOneAsync(x => x.Student.Id == id, update);
        }
    }
    public interface IPaymentsService : IContextService<Payment, Payment>
    {
        Task<IEnumerable<StudentBalance>> GetStudentBalances();
        Task SetStudentBalances();
        Task SetStudentBalances(string id);
    }
}