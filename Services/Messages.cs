using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using skooltrak_api.Models;
using skooltrak_api.Shared;

namespace skooltrak_api.Services
{
  public class MessagesService : IMessagesService
  {
    private readonly IDbContext _context;
    private readonly NotificationsService notifications;
    private readonly Mailer mailer;

    public MessagesService(IDbContext context)
    {
      _context = context;
      mailer = new Mailer();
      notifications = new NotificationsService();
    }

    public async Task<Message> Create(Message message, UserInfo user)
    {
      message.Sender = user;
      message.SendDate = DateTime.Now;
      await _context.Messages.InsertOneAsync(message);
      if (message.Status == (int)MessageEnum.Sent)
      {
        _ = SendToInbox(message);
      }

      return message;
    }

    private async Task SendToInbox(Message message)
    {
      var ids = message.Receivers.Select(x => x.Id).ToArray();
      message.Users = await _context.Users.Find(x => ids.Contains(x.Id)).Project<UserInfo>(Builders<User>.Projection.Expression(x => new UserInfo()
      {
        Id = x.Id,
        DisplayName = x.DisplayName,
        Email = x.Email,
        PhotoURL = x.PhotoURL,
        Role = x.Role
      })).ToListAsync();
      message.Receivers.ForEach(async receiver =>
      {
        var reference = new MessageReference()
        {
          Id = message.Id,
          Title = message.Title,
          Sender = message.Sender,
          Status = message.Status
        };
        var inbox = new MessageInbox()
        {
          Reference = reference,
          Receiver = receiver
        };

        await _context.MessageInboxes.InsertOneAsync(inbox);
        await notifications.SendMessage(inbox);
      });
      await mailer.SendEmail(message);
    }

    public async Task<IEnumerable<Message>> Get(UserInfo user)
    {
      return await _context.Messages.Find(x => true).SortByDescending(x => x.SendDate).ToListAsync();
    }

    public async Task<Message> Get(string id, UserInfo user)
    {
      return await _context.Messages.Find(x => x.Id == id).FirstOrDefaultAsync();
    }

    public void SetUserInfo()
    {
      var students = _context.Students.Find(x => true).ToList();
      students.ForEach(student =>
      {
        var update = Builders<Message>.Update.Set(x => x.Sender.Group, student.Group);
        var inbox = Builders<MessageInbox>.Update.Set(x => x.Message.Sender.Group, student.Group);
        _context.Messages.UpdateMany(x => x.Sender.Id == student.UserId, update);
        _context.MessageInboxes.UpdateMany(x => x.Message.Sender.Id == student.UserId, inbox);
      });

    }

    public async Task<IEnumerable<UserInfo>> GetContacts(UserInfo user)
    {
      var users = await _context.Users.Find(x => x.Role.Code == (int)RoleEnum.Administrator)
      .Project<UserInfo>(Builders<User>.Projection.Expression(x => new UserInfo()
      {
        Id = x.Id,
        DisplayName = x.DisplayName,
        Email = x.Email,
        PhotoURL = x.PhotoURL,
        Role = x.Role
      })).SortBy(x => x.DisplayName).ToListAsync();

      if (user.Role.Code == (int)RoleEnum.Administrator || user.Role.Code == (int)RoleEnum.Teacher)
      {
        users.AddRange(await _context.Users
        .Find(x => x.Role.Code == (int)RoleEnum.Teacher)
        .Project<UserInfo>(Builders<User>.Projection.Expression(x => new UserInfo()
        {
          Id = x.Id,
          DisplayName = x.DisplayName,
          Email = x.Email,
          PhotoURL = x.PhotoURL,
          Role = x.Role
        })).SortBy(x => x.DisplayName).ToListAsync());
      }
      else
      {
        var student = await _context.Students.Find(x => x.Id == user.People[0].Id).FirstOrDefaultAsync();
        var courses = await _context.Courses.Find(x => x.Plan.Id == student.Plan.Id).ToListAsync();
        var teachersIds = courses.SelectMany(x => x.Teachers.Select(y => y.Id)).ToArray();
        var teachers = await _context.Teachers.Find(x => teachersIds.Contains(x.Id)).ToListAsync();
        var UserIds = teachers.Select(y => y.UserId).ToArray();
        users.AddRange(await _context.Users
        .Find(x => UserIds.Contains(x.Id))
        .Project<UserInfo>(Builders<User>.Projection.Expression(x => new UserInfo()
        {
          Id = x.Id,
          DisplayName = x.DisplayName,
          Email = x.Email,
          PhotoURL = x.PhotoURL,
          Role = x.Role
        })).SortBy(x => x.DisplayName).ToListAsync());
      }

      if (user.Role.Code == (int)RoleEnum.Administrator)
      {
        users.AddRange(await _context.Users.Find(x => x.Role.Code == (int)RoleEnum.Student)
        .Project<UserInfo>(Builders<User>.Projection.Expression(x => new UserInfo()
        {
          Id = x.Id,
          DisplayName = x.DisplayName,
          Email = x.Email,
          PhotoURL = x.PhotoURL,
          Role = x.Role
        })).SortBy(x => x.DisplayName).ToListAsync());
      }
      else if (user.Role.Code == (int)RoleEnum.Teacher)
      {
        var teacher = await _context.Teachers.Find(x => x.Id == user.People[0].Id).FirstOrDefaultAsync();
        var courses = await _context.Courses.Find(x => x.Teachers.Any(y => y.Id == teacher.Id)).ToListAsync();
        var planIds = courses.Select(x => x.Plan.Id).ToArray();
        var students = await _context.Students.Find(x => planIds.Contains(x.Plan.Id) && x.Active).ToListAsync();
        var studentIds = students.Select(x => x.UserId).ToArray();
        users.AddRange(await _context.Users.Find(x => studentIds.Contains(x.Id)).Project<UserInfo>(Builders<User>.Projection.Expression(x => new UserInfo()
        {
          Id = x.Id,
          DisplayName = x.DisplayName,
          Email = x.Email,
          PhotoURL = x.PhotoURL,
          Role = x.Role
        })).SortBy(x => x.DisplayName).ToListAsync());
      }
      else if (user.Role.Code == (int)RoleEnum.Student)
      {
        var student = await _context.Students.Find(x => x.Id == user.People[0].Id).FirstOrDefaultAsync();
        var students = await _context.Students.Find(x => x.Group.Id == student.Group.Id && x.Active).ToListAsync();
        var studentIds = students.Select(x => x.UserId).ToArray();
        users.AddRange(await _context.Users.Find(x => studentIds.Contains(x.Id)).Project<UserInfo>(Builders<User>.Projection.Expression(x => new UserInfo()
        {
          Id = x.Id,
          DisplayName = x.DisplayName,
          Email = x.Email,
          PhotoURL = x.PhotoURL,
          Role = x.Role
        })).SortBy(x => x.DisplayName).ToListAsync());
      }
      users = users.OrderBy(x => x.DisplayName).ToList();
      return users;
    }

    public async Task<IEnumerable<Message>> GetDrafts(UserInfo user)
    {
      return await _context.Messages.Find(x => x.Sender.Id == user.Id && x.Status == (int)MessageEnum.Draft).SortByDescending(x => x.CreateDate).ToListAsync();
    }

    public async Task<MessageInbox> GetMessage(string id)
    {
      return await _context.MessageInboxes.Find(x => x.Id == id).FirstOrDefaultAsync();
    }

    public async Task<IEnumerable<MessageInbox>> GetMessages(UserInfo user, string lastId = "")
    {
      var index = Builders<MessageInbox>.IndexKeys;
      var indexModel = new CreateIndexModel<MessageInbox>(index.Descending(x => x.ArrivalDate));
      await _context.MessageInboxes.Indexes.CreateOneAsync(indexModel).ConfigureAwait(false);
      if (string.IsNullOrEmpty(lastId))
      {
        return await _context.MessageInboxes.Find(x => x.Receiver.Id == user.Id && !x.Trash)
            .SortByDescending(x => x.ArrivalDate).Limit(10).ToListAsync();
      }
      var lastObjectId = new ObjectId(lastId);
      var filter = Builders<MessageInbox>.Filter
                      .And(Builders<MessageInbox>.Filter
                      .Where(x => x.Receiver.Id == user.Id && !x.Trash), Builders<MessageInbox>.Filter.
                      Lt("_id", lastObjectId));
      return await _context.MessageInboxes.Find(filter)
              .SortByDescending(x => x.ArrivalDate).Limit(10).ToListAsync();

    }

    public async Task<IEnumerable<Receiver>> GetReceivers(UserInfo user)
    {
      var teacherRole = await _context.Roles.Find(x => x.Code == (int)RoleEnum.Teacher).FirstOrDefaultAsync();
      var studentRole = await _context.Roles.Find(x => x.Code == (int)RoleEnum.Student).FirstOrDefaultAsync();
      var receivers = new List<Receiver>();
      receivers.AddRange(await _context.Users.Find(x => x.Role.Code == (int)RoleEnum.Administrator)
              .Project<Receiver>(Builders<User>.Projection.Expression(x => new Receiver()
              {
                Role = x.Role,
                Description = x.Description,
                Name = x.DisplayName,
                Id = x.Id
              })).SortBy(x => x.DisplayName).ToListAsync()
          );
      if (user.Role.Code == (int)RoleEnum.Administrator || user.Role.Code == (int)RoleEnum.Teacher)
      {
        receivers.AddRange(
            await _context.Teachers.Find(x => true)
            .Project<Receiver>(Builders<Teacher>.Projection.Expression(x => new Receiver()
            {
              Role = teacherRole,
              Name = x.Name,
              Description = "",
              Id = x.UserId
            })).ToListAsync()
        );

        receivers.AddRange(
            await _context.Students.Find(x => x.UserId != null && x.Active)
            .Project<Receiver>(Builders<Student>.Projection.Expression(x => new Receiver()
            {
              Role = studentRole,
              Name = x.Name,
              Group = x.Group,
              Plan = x.Plan == null ? null : new Reference() { Id = x.Plan.Id, Name = x.Plan.Name },
              Id = x.UserId
            })).ToListAsync()
        );
      }
      else
      {
        var student = await _context.Students.Find(x => x.UserId == user.Id).FirstOrDefaultAsync();
        var courses = await _context.Courses.Find(x => x.Plan.Id == student.Plan.Id).ToListAsync();
        var teachersId = courses.SelectMany(x => x.Teachers.Select(y => y.Id)).ToArray();
        receivers.AddRange(
            await _context.Teachers.Find(x => teachersId.Contains(x.Id))
            .Project<Receiver>(Builders<Teacher>.Projection.Expression(x => new Receiver()
            {
              Role = teacherRole,
              Name = x.Name,
              Description = string.Join(", ", x.Subjects.Select(y => y.Name).ToArray()),
              Id = x.UserId
            })).ToListAsync()
        );

        receivers.AddRange(
            await _context.Students.Find(x => x.UserId != null && x.Active && x.Group.Id == student.Group.Id)
            .Project<Receiver>(Builders<Student>.Projection.Expression(x => new Receiver()
            {
              Role = studentRole,
              Name = x.Name,
              Group = x.Group,
              Plan = new Reference()
              {
                Id = x.Plan.Id,
                Name = x.Plan.Name
              },
              Id = x.UserId
            })).ToListAsync()
        );
      }
      return receivers;
    }

    public async Task<IEnumerable<Message>> GetSent(UserInfo user)
    {
      var index = Builders<Message>.IndexKeys;
      var indexModel = new CreateIndexModel<Message>(index.Descending(x => x.SendDate));
      await _context.Messages.Indexes.CreateOneAsync(indexModel).ConfigureAwait(false);
      return await _context.Messages.Find(x => x.Sender.Id == user.Id && x.Status == (int)MessageEnum.Sent).SortByDescending(x => x.SendDate).ToListAsync();
    }

    public async Task Remove(string id, UserInfo user)
    {
      var message = await _context.Messages.Find(x => x.Id == id).FirstOrDefaultAsync();
      message.Attached.ForEach(doc =>
      {
        _context.bucket.Delete(ObjectId.Parse(doc.Id));
      });
      await _context.MessageInboxes.DeleteManyAsync(x => x.Message.Id == id);
      await _context.Messages.DeleteOneAsync(x => x.Id == id);
    }

    public async Task SentToTrash(string id)
    {
      var update = Builders<MessageInbox>.Update.Set(x => x.Trash, true);
      await _context.MessageInboxes.UpdateOneAsync(x => x.Id == id, update);
    }
    public async Task RecoverFromTrash(string id)
    {
      var update = Builders<MessageInbox>.Update.Set(x => x.Trash, false);
      await _context.MessageInboxes.UpdateOneAsync(x => x.Id == id, update);
    }

    public async Task SetRead(UserInfo user, string id)
    {
      var message = await _context.MessageInboxes.Find(x => x.Id == id && x.Receiver.Id == user.Id).FirstOrDefaultAsync();
      var update = Builders<MessageInbox>.Update
          .Set(x => x.Read, true)
          .Set(x => x.ReadDate, DateTime.Now);

      await _context.MessageInboxes.UpdateOneAsync(x => x.Id == id, update);
    }

    public async Task<long> Unread(UserInfo user)
    {
      return await _context.MessageInboxes.Find(x => x.Receiver.Id == user.Id && x.Read == false).CountDocumentsAsync();
    }

    public async Task Update(string id, Message message, UserInfo user)
    {
      var update = Builders<Message>.Update
          .Set(x => x.Status, message.Status)
          .Set(x => x.Title, message.Title)
          .Set(x => x.Attached, message.Attached)
          .Set(x => x.Content, message.Content)
          .Set(x => x.Receivers, message.Receivers)
          .Set(x => x.SendDate, DateTime.Now);

      if (message.Status == (int)MessageEnum.Sent)
      {
        message.Receivers.ForEach(async receiver =>
        {
          var inbox = new MessageInbox()
          {
            Message = message,
            Receiver = receiver
          };
          await _context.MessageInboxes.InsertOneAsync(inbox);
        });
        await mailer.SendEmail(message);
      }
      await _context.Messages.UpdateOneAsync(x => x.Id == id, update);
    }

    public async Task<IEnumerable<MessageInbox>> GetTrash(UserInfo user)
    {
      return await _context.MessageInboxes.Find(x => x.Receiver.Id == user.Id && x.Trash).SortByDescending(x => x.ArrivalDate).ToListAsync();
    }

    public async Task RemoveInbox(string id)
    {
      await _context.MessageInboxes.DeleteOneAsync(x => x.Id == id);
    }

    public void MigrateToReferece()
    {
      var messages = _context.Messages.Find(x => true).SortByDescending(x => x.SendDate).ToList();
      messages.ForEach(item =>
      {
        var update = Builders<MessageInbox>.Update
                  .Set(x => x.Reference, new MessageReference()
                  {
                    Id = item.Id,
                    Title = item.Title,
                    Sender = item.Sender,
                    Status = item.Status,
                    ContentBlocks = item.ContentBlocks
                  })
                  .Unset(x => x.Message);
        _context.MessageInboxes.UpdateMany(x => x.Message.Id == item.Id || x.Reference.Id == item.Id, update);
      });
    }
  }

  public interface IMessagesService : IContextService<Message, Message>
  {
    Task<IEnumerable<MessageInbox>> GetMessages(UserInfo user, string lastId = "");
    Task<MessageInbox> GetMessage(string id);
    void MigrateToReferece();
    Task RemoveInbox(string id);
    void SetUserInfo();
    Task SentToTrash(string id);
    Task<IEnumerable<MessageInbox>> GetTrash(UserInfo user);
    Task RecoverFromTrash(string id);
    Task SetRead(UserInfo user, string id);
    Task<IEnumerable<Message>> GetSent(UserInfo user);
    Task<IEnumerable<Message>> GetDrafts(UserInfo user);
    Task<IEnumerable<UserInfo>> GetContacts(UserInfo user);
    Task<IEnumerable<Receiver>> GetReceivers(UserInfo user);
    Task<long> Unread(UserInfo user);
  }
}