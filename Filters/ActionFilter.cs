using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using MongoDB.Driver;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Filter
{
    public class SkipActionFilter : Attribute
    {
        public SkipActionFilter() { }
    }

    public class TeacherRequired : Attribute
    {
        public TeacherRequired() { }
    }

    public class AdminRequired : Attribute
    {
        public AdminRequired() { }
    }

    public interface IApiActionFilter
    {
        void OnActionExecuting(ActionExecutingContext context);
    }

    public class ApiActionFilter : ActionFilterAttribute, IApiActionFilter
    {
        private readonly IDbContext _context;

        public ApiActionFilter(IDbContext context)
        {
            _context = context;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var attrs = (context.ActionDescriptor as ControllerActionDescriptor).MethodInfo.GetCustomAttributes(inherit: true);
            foreach (var attr in attrs)
            {
                if (attr.GetType() == typeof(SkipActionFilter))
                {
                    return;
                }
            }

            var request = context.HttpContext.Request;
            var absoluteUri = string.Concat(
                request.Scheme,
                "://",
                request.Host.ToUriComponent(),
                request.PathBase.ToUriComponent(),
                request.Path.ToUriComponent(),
                request.QueryString.ToUriComponent());

            string userId = request.Headers["userId"];

            if (userId == null || userId.Length == 0)
            {
                context.Result = new ContentResult
                {
                    Content = "User ID not provider",
                    StatusCode = 401
                };
                return;
            }

            var user = _context.Users.Find(x => x.Id == userId).Project<UserInfo>(Builders<User>.Projection.Expression(x => new UserInfo()
            {
                Id = x.Id,
                DisplayName = x.DisplayName,
                Email = x.Email,
                Blocked = x.Blocked,
                MeetingBlocked = x.MeetingBlocked,
                PhotoURL = x.PhotoURL,
                Role = x.Role
            })).FirstOrDefault();

            if (user == null)
            {
                context.Result = new ContentResult
                {
                    Content = "User does not exists",
                    StatusCode = 401
                };
                return;
            }


            if (user.Role.Code == (int)RoleEnum.Student)
            {
                if (user.Blocked)
                {
                    context.Result = new ContentResult
                    {
                        Content = "Blocked user",
                        StatusCode = 401
                    };
                    return;
                }
                var person = _context.Students.Find(x => x.UserId == user.Id).Project<Reference>(Builders<Student>.Projection.Expression(x => new Reference()
                {
                    Id = x.Id,
                    Name = x.Name
                })).FirstOrDefault();
                user.People.Add(person);
            }

            if (user.Role.Code == (int)RoleEnum.Teacher)
            {
                user.People.Add(_context.Teachers.Find(x => x.UserId == user.Id).Project<Reference>(Builders<Teacher>.Projection.Expression(x => new Reference()
                {
                    Id = x.Id,
                    Name = x.Name
                })).FirstOrDefault());
            }

            foreach (var attr in attrs)
            {
                if (attr.GetType() == typeof(AdminRequired))
                {
                    if (user.Role.Code != (int)RoleEnum.Administrator)
                    {
                        context.Result = new ContentResult
                        {
                            Content = "Permissions denied. You need to be an administrator",
                            StatusCode = 401
                        };
                        return;
                    }
                }

                if (attr.GetType() == typeof(TeacherRequired))
                {
                    if (user.Role.Code != (int)RoleEnum.Teacher && user.Role.Code != (int)RoleEnum.Administrator)
                    {
                        context.Result = new ContentResult
                        {
                            Content = "Permissions denied. You need to be a teacher or an admninistrator",
                            StatusCode = 401
                        };
                        return;
                    }
                }
            }
            context.RouteData.Values.Add("user", user);
        }
    }
}