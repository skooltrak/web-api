using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

	[ApiController]
	[Route("api/[controller]")]
	public class QuizesController : ControllerBase
	{
		private readonly IQuizesService _context;
		

		public QuizesController(IQuizesService context)
		{
			_context = context;
			
		}

		[HttpGet]
		public async Task<IEnumerable<Quiz>> GetQuizes()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetQuiz")]
		public async Task<ActionResult<Quiz>> GetQuiz([FromRoute] string id)
		{
			var quiz = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (quiz == null)
			{
				return NotFound();
			}

			return Ok(quiz);
		}

		[HttpPost]
		public async Task<ActionResult<Quiz>> Create([FromBody] Quiz quiz)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(quiz, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetQuiz", new { id = quiz.Id }, quiz);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Update([FromBody] Quiz quiz, [FromRoute] string id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (quiz.Id != id)
			{
				return BadRequest();
			}

			if ((await _context.Get(id, (UserInfo)base.RouteData.Values["User"])) == null)
			{
				return NotFound();
			}

			await _context.Update(id, quiz, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			var role = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (role == null)
			{
				return NotFound();
			}

			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}