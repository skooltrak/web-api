using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

	[ApiController]
	[Route("api/[controller]")]
	public class CommentsController : ControllerBase
	{
		private readonly ICommentsService _context;

		public CommentsController(ICommentsService context)
		{
			_context = context;
		}

		[HttpGet]
		public async Task<IEnumerable<Comment>> GetComments()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}


		[HttpGet("{id:length(24)}", Name = "GetComment")]
		public async Task<ActionResult<Comment>> GetComment([FromRoute] string id)
		{
			var comment = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
			if (comment == null)
			{
				return NotFound();
			}
			return Ok(comment);
		}

		[HttpPost]
		public async Task<IActionResult> CreateComment([FromBody] Comment comment)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(comment, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetComment", new { Id = comment.Id }, comment);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> UpdateComment([FromRoute] string id, [FromBody] Comment comment)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Update(id, comment, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> DeleteComment([FromRoute] string id)
		{
			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}