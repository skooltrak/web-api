using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

	[ApiController]
	[Route("api/[controller]")]
	public class PaymentDaysController : ControllerBase
	{
		private readonly IPaymentDaysService _context;


		public PaymentDaysController(IPaymentDaysService context)
		{
			_context = context;

		}

		[HttpGet]
		public async Task<IEnumerable<PaymentDay>> Get()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetPaymentDay")]
		public async Task<ActionResult<PaymentDay>> Get([FromRoute] string id)
		{
			var day = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (day == null)
			{
				return NotFound();
			}

			return Ok(day);
		}

		[HttpPost]
		public async Task<ActionResult<PaymentDay>> Create([FromBody] PaymentDay day)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(day, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetPaymentDay", new { id = day.Id }, day);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Update([FromBody] PaymentDay dayIn, [FromRoute] string id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (dayIn.Id != id)
			{
				return BadRequest();
			}

			var PaymentDay = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (PaymentDay == null)
			{
				return NotFound();
			}

			await _context.Update(id, dayIn, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}

		[HttpPost("CreditNote")]
		public async Task<IActionResult> CreditNote([FromBody] PaymentDay day)
		{
			await _context.CreateCreditNote(day.Id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			var PaymentDay = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (PaymentDay == null)
			{
				return NotFound();
			}

			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}