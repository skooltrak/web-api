using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Filter;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers {

	[ApiController]
	[Route("api/[controller]")]
	public class ClassroomsController : ControllerBase {
		private readonly IClassroomsService _context;

		public ClassroomsController(IClassroomsService context) {
			_context = context;
		}

		[HttpGet]
		public async Task<IEnumerable<Classroom>> GetClassrooms()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("all")]
		[TeacherRequired]
		public async Task<IEnumerable<Classroom>> GetAllClassrooms()
		{
			return await _context.GetClassrooms();
		}

		[HttpGet("{id:length(24)}", Name = "GetClassroom")]
		public async Task<ActionResult<Classroom>> GetClassroom([FromRoute] string id)
		{
			var classroom = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
			if (classroom == null)
			{
				return NotFound();
			}
			return Ok(classroom);
		}

		[HttpPost]
		public async Task<IActionResult> CreateClassroom([FromBody] Classroom classroom)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(classroom, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetClassroom", new { Id = classroom.Id }, classroom);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> UpdateClassroom([FromRoute] string id, [FromBody] Classroom classroom)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Update(id, classroom, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> DeleteClassroom([FromRoute] string id)
		{
			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}