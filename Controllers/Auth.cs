using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Filter;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class AuthController : ControllerBase
	{
		private readonly IAuthenticationService _context;

		public AuthController(IAuthenticationService context)
		{
			_context = context;
		}

		[SkipActionFilter]
		[HttpPost("Login")]
		public async Task<ActionResult<User>> Login([FromBody] Login login)
		{
			var user = await _context.Login(login);
			if (user == null)
			{
				return NotFound();
			}

			if (user.Role.Code == (int)RoleEnum.Student)
			{
				if (!(await _context.ValidateBlock(user)))
				{
					return Unauthorized();
				}
			}

			return Ok(user);
		}

		[SkipActionFilter]
		[HttpGet("ResetPassword/{email}")]
		public async Task<IActionResult> ResetPassword([FromRoute] string email)
		{
			await _context.ResetPassword(email);
			return NoContent();
		}

		[HttpPost("ChangePassword")]
		public async Task<IActionResult> ChangePassword([FromBody] Password password)
		{
			var user = (UserInfo)base.RouteData.Values["User"];
			await _context.ChangePassword(user.Id, password.NewPassword);
			return NoContent();
		}

		[HttpPost("UpdateProfile")]
		public async Task<IActionResult> UpdateProfile([FromBody] User user)
		{
			await _context.UpdateProfile(user.Id, user);
			return NoContent();
		}

		public class Password
		{
			public string NewPassword { get; set; }
		}

	}
}