using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class PreScholarController : ControllerBase
    {
        private readonly IPreScholarService _context;

        public PreScholarController(IPreScholarService context)
        {
            _context = context;
        }

        [HttpGet("{id:length(24)}")]
        public async Task<IActionResult> GetEvaluations([FromRoute] string id)
        {
            var comparer = new evaluationsComparer();
            var evaluations = await _context.GetEvaluations(id);
            var grouped = evaluations
                               .GroupBy(x => x.Area, comparer)
                               .Select(g => new
                               {
                                   Area = g.Key,
                                   Evaluations = g.Select(item => new { item.Item, item.Periods })
                               }).ToList();
            return Ok(grouped);
        }

        [HttpPost("SetEvaluation")]
        public async Task<IActionResult> SetEvaluation([FromBody] EvaluationValue item)
        {
            try
            {
                await _context.SetValue(item);
            }
            catch (System.Exception e)
            {
                return BadRequest(e);
            }

            return NoContent();
        }

        [HttpGet("InitValues")]
        public IActionResult InitValues()
        {
            try
            {
                _context.InitValues();
            }
            catch (System.Exception e)
            {
                return BadRequest(e);
            }

            return NoContent();
        }
    }

    public class evaluationsComparer : IEqualityComparer<ReferenceArea>
    {
        public bool Equals(ReferenceArea x, ReferenceArea y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(ReferenceArea x)
        {
            return x.Id.GetHashCode();
        }
    }
}