using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CleaningController : ControllerBase
    {
        private readonly ICleaningService _context;

        public CleaningController(ICleaningService context)
        {
            _context = context;
        }

        [HttpGet("items")]
        public async Task<IEnumerable<CleaningItem>> GetItems()
        {
            return await _context.GetItems();
        }


        [HttpPost]
        public async Task<IActionResult> RunCleaning([FromBody] List<CleaningItem> items)
        {
            await _context.ExecuteClean(items);
            return Ok();
        }
    }
}