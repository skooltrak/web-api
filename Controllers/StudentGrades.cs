using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class StudentGradesController : ControllerBase
	{
		private readonly IStudentGradesService _context;
		

		public StudentGradesController(IStudentGradesService context)
		{
			_context = context;
			
		}

		[HttpGet]
		public async Task<IEnumerable<StudentGrade>> Get()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetStudentGrade")]
		public async Task<ActionResult<StudentGrade>> Get([FromRoute] string id)
		{
			var grade = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (grade == null)
			{
				return NotFound();
			}

			return Ok(grade);
		}

		[HttpPost]
		public async Task<ActionResult<StudentGrade>> Create([FromBody] StudentGrade grade)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(grade, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetStudentGrade", new { id = grade.Id }, grade);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Update([FromBody] StudentGrade gradeIn, [FromRoute] string id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (gradeIn.Id != id)
			{
				return BadRequest();
			}

			var grade = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (grade == null)
			{
				return NotFound();
			}

			await _context.Update(id, gradeIn, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			var grade = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (grade == null)
			{
				return NotFound();
			}

			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}