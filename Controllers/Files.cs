using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using skooltrak_api.Filter;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FilesController : ControllerBase
    {
        private readonly IFilesService _context;
        private readonly IDbContext _global;

        public FilesController(IFilesService context, IDbContext global)
        {
            _context = context;
            _global = global;
        }

        [SkipActionFilter]
        [HttpPost]
        [RequestSizeLimit(100000000)]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            if (file == null)
            {
                file = HttpContext.Request.Form.Files[0];
            }
            var result = await _context.Upload(file);
            return Ok(result);
        }


        [SkipActionFilter]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFile(string id)
        {

            Stream sourceStream = await _global.bucket.OpenDownloadStreamAsync(ObjectId.Parse(id));
            BufferedStream bs = new BufferedStream(sourceStream);
            var file = await _context.GetFile(id);
            FileResult res = File(bs, file.Info.Metadata["type"].ToString(), file.Info.Filename, true);
            return res;
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> DeleFile(string id)
        {
            await _context.DeleteFile(id);
            return Ok();
        }
    }
}