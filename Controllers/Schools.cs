using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Filter;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class SchoolsController : ControllerBase
  {
    private readonly ISchoolsService _context;
    public SchoolsController(ISchoolsService context)
    {
      _context = context;

    }

    [HttpGet]
    public async Task<IEnumerable<School>> Get()
    {
      return await _context.Get((UserInfo)base.RouteData.Values["User"]);
    }

    [HttpGet("{id:length(24)}", Name = "GetSchool")]
    public async Task<ActionResult<School>> Get([FromRoute] string id)
    {
      var school = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

      if (school == null)
      {
        return NotFound();
      }

      return Ok(school);
    }

    [SkipActionFilter]
    [HttpGet("default")]
    public async Task<ActionResult<School>> GetDefault()
    {
      var school = await _context.GetDefaultSchool();
      if (school == null)
      {
        return NotFound();
      }

      return Ok(school);
    }

    [HttpPost("SetSchool")]
    public async Task<IActionResult> SetSchool([FromBody] string id)
    {
      await _context.SetStudents(id);
      return NoContent();
    }

    [HttpGet("FixPlan")]
    public async Task<IActionResult> FixPlan()
    {
      await _context.FixPlan();
      return NoContent();
    }

    [HttpGet("{id:length(24)}/CloseYear")]
    public IActionResult CloseSchool([FromRoute] string id)
    {
      _context.CloseYear(id);
      return NoContent();
    }

    [HttpGet("ClearYear")]
    public async Task<IActionResult> ClearSchool()
    {
      await _context.ClearYear();
      return NoContent();
    }

    [HttpPost]
    public async Task<ActionResult<School>> Create([FromBody] School school)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      await _context.Create(school, (UserInfo)base.RouteData.Values["User"]);
      return CreatedAtRoute("GetSchool", new { id = school.Id }, school);
    }

    [HttpPut("{id:length(24)}")]
    public async Task<IActionResult> Update([FromBody] School schoolIn, [FromRoute] string id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (schoolIn.Id != id)
      {
        return BadRequest();
      }

      var school = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

      if (school == null)
      {
        return NotFound();
      }

      await _context.Update(id, schoolIn, (UserInfo)base.RouteData.Values["User"]);

      return NoContent();
    }

    [HttpGet("{id:length(24)}/SaveCredits")]
    public IActionResult SaveCredits([FromRoute] string id)
    {
      _context.SaveCredits(2021, id);
      return NoContent();
    }

    [HttpGet("{id:length(24)}/SaveCredits/{studentId:length(24)}")]
    public IActionResult SaveStudentCredits([FromRoute] string id, [FromRoute] string studentId)
    {
      _context.SaveStudentCredits(2021, id, studentId);
      return NoContent();
    }

    [HttpDelete("{id:length(24)}")]
    public async Task<IActionResult> Delete([FromRoute] string id)
    {
      var school = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

      if (school == null)
      {
        return NotFound();
      }

      await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
      return NoContent();
    }
  }
}