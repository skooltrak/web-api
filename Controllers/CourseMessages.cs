using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class CoursesMessagesController : ControllerBase
	{
		private readonly ICourseMessagesService _context;
		

		public CoursesMessagesController(ICourseMessagesService context)
		{
			_context = context;
			
		}

		[HttpGet]
		public async Task<IEnumerable<CourseMessage>> GetMessages()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetCourseMessage")]
		public async Task<ActionResult<CourseMessage>> GetCourseMessage([FromRoute] string id)
		{
			var message = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
			if (message == null)
			{
				return NotFound();
			}
			return Ok(message);
		}

		[HttpPost]
		public async Task<IActionResult> CreateCourseMessage([FromBody] CourseMessage message)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(message, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetCourseMessage", new { Id = message.Id }, message);
		}


		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> DeleteCourseMessage([FromRoute] string id)
		{
			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}