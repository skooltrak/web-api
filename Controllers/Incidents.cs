using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

	[ApiController]
	[Route("api/[controller]")]
	public class IncidentsController : ControllerBase
	{
		private readonly IIncidentsService _context;


		public IncidentsController(IIncidentsService context)
		{
			_context = context;

		}

		[HttpGet]
		public async Task<IEnumerable<Incident>> GetIncidents()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetIncident")]
		public async Task<ActionResult<Incident>> GetIncident([FromRoute] string id)
		{
			var incident = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (incident == null)
			{
				return NotFound();
			}

			return Ok(incident);
		}

		[HttpPost("{id:length(24)}/Update")]
		public async Task<IActionResult> AddUpdate([FromRoute] string id, [FromBody] IncidentUpdate update)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			try
			{
				await _context.AddUpdates(id, update, (UserInfo)base.RouteData.Values["User"]);
			}
			catch (System.Exception)
			{
				return Unauthorized();
			}

			return NoContent();
		}

		[HttpPost]
		public async Task<ActionResult<Incident>> Create([FromBody] Incident incident)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(incident, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetIncident", new { id = incident.Id }, incident);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Update([FromBody] Incident incident, [FromRoute] string id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (incident.Id != id)
			{
				return BadRequest();
			}

			if ((await _context.Get(id, (UserInfo)base.RouteData.Values["User"])) == null)
			{
				return NotFound();
			}

			await _context.Update(id, incident, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			var incident = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (incident == null)
			{
				return NotFound();
			}

			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}