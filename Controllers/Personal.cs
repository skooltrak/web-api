using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class PersonalController : ControllerBase
	{
		private readonly IPersonalService _context;

		public PersonalController(IPersonalService context)
		{
			_context = context;
		}

		[HttpGet("Documents")]
		public async Task<IEnumerable<Document>> GetDocument()
		{
			return await _context.GetDocuments((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("Contents")]
		public async Task<IEnumerable<CourseContent>> GetContents()
		{
			return await _context.GetContents((UserInfo)base.RouteData.Values["User"]);
		}

	}
}