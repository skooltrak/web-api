using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class ContentController : ControllerBase
	{
		private readonly ICourseContentsService _context;


		public ContentController(ICourseContentsService context)
		{
			_context = context;

		}

		[HttpGet]
		public async Task<IEnumerable<CourseContent>> Get()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetCourseContent")]
		public async Task<ActionResult<CourseContent>> Get([FromRoute] string id)
		{
			var content = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (content == null)
			{
				return NotFound();
			}

			return Ok(content);
		}

		[HttpPost]
		public async Task<ActionResult<CourseContent>> Create([FromBody] CourseContent content)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(content, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetCourseContent", new { id = content.Id }, content);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Update([FromBody] CourseContent contentIn, [FromRoute] string id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (contentIn.Id != id)
			{
				return BadRequest();
			}

			var content = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (content == null)
			{
				return NotFound();
			}

			await _context.Update(id, contentIn, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			var content = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (content == null)
			{
				return NotFound();
			}

			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}
