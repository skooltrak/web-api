using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Filter;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class RolesController : ControllerBase
	{
		private readonly IRolesService _context;
		

		public RolesController(IRolesService context)
		{
			_context = context;
			
		}

		[HttpGet]
		[SkipActionFilter]
		public async Task<IEnumerable<Role>> Get()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetRole")]
		public async Task<ActionResult<Role>> Get([FromRoute] string id)
		{
			var role = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (role == null)
			{
				return NotFound();
			}

			return Ok(role);
		}

		[HttpPost]
		public async Task<ActionResult<Role>> Create([FromBody] Role role)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(role, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetRole", new { id = role.Id }, role);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Update([FromRoute] string id, [FromBody] Role roleIn)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (roleIn.Id != id)
			{
				return BadRequest();
			}

			var role = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (role == null)
			{
				return NotFound();
			}

			await _context.Update(id, roleIn, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			var role = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (role == null)
			{
				return NotFound();
			}

			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}

	}
}