using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class PeriodsController : ControllerBase
    {
        private readonly IPeriodsService _context;


        public PeriodsController(IPeriodsService context)
        {
            _context = context;

        }

        [HttpGet]
        public async Task<IEnumerable<Period>> GetPeriods()
        {
            return await _context.Get((UserInfo)base.RouteData.Values["User"]);
        }

        /// <summary>
        /// Get Active Period
        /// </summary>
        [HttpGet("active")]
        public async Task<Period> GetActive()
        {
            return await _context.GetActive();
        }

        [HttpGet("{id:length(24)}", Name = "GetPeriod")]
        public async Task<ActionResult<Period>> GetPeriod([FromRoute] string id)
        {
            var period = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
            if (period == null)
            {
                return NotFound();
            }
            return Ok(period);
        }

        [HttpGet("{id:length(24)}/SetRanking")]
        public async Task<IActionResult> GenerateRanking([FromRoute] string id)
        {
            await _context.GenerateRanking(id);


            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> CreatePeriod([FromBody] Period period)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _context.Create(period, (UserInfo)base.RouteData.Values["User"]);
            return CreatedAtRoute("GetPeriod", new { Id = period.Id }, period);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> UpdatePeriod([FromRoute] string id, [FromBody] Period period)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _context.Update(id, period, (UserInfo)base.RouteData.Values["User"]);
            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> DeletePeriod([FromRoute] string id)
        {
            await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
            return NoContent();
        }
    }
}
