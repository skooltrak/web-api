using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Filter;

namespace skooltrak_api.Controllers
{
	[ApiController]
	public class ErrorController : ControllerBase
	{
		[Route("/error")]
		[SkipActionFilter]
		[ApiExplorerSettings(IgnoreApi = true)]
		public IActionResult Error()
		{
			var context = HttpContext.Features.Get<IExceptionHandlerFeature>();

			return Problem(
				detail: context.Error.StackTrace,
				title: context.Error.Message);

		}
	}
}