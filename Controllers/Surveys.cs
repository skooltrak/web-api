using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class SurveysController : ControllerBase
	{
		private readonly ISurveysService _context;

		public SurveysController(ISurveysService context)
		{
			_context = context;
		}

		[HttpGet]
		public async Task<IEnumerable<Survey>> Get()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetSurvey")]
		public async Task<ActionResult<Survey>> Get([FromRoute] string id)
		{
			var survey = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (survey == null)
			{
				return NotFound();
			}

			return Ok(survey);
		}

		[HttpGet("{id:length(24)}/Recharge")]
		public async Task<IActionResult> Recharge([FromRoute] string id)
		{
			await _context.RechargeSurvey(id);
			return NoContent();
		}

		[HttpGet("{id:length(24)}/Answers")]
		public async Task<IEnumerable<UserSurvey>> GetAnswers([FromRoute] string id)
		{
			return await _context.GetAnswers(id);
		}

		[HttpPost("Answer")]
		public async Task<IActionResult> AnswerSurvey([FromBody] UserSurvey survey)
		{
			await _context.AnswerSurvey(survey, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}

		[HttpPost]
		public async Task<ActionResult<Survey>> Create([FromBody] Survey Survey)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(Survey, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetSurvey", new { id = Survey.Id }, Survey);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Update([FromBody] Survey SurveyIn, [FromRoute] string id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (SurveyIn.Id != id)
			{
				return BadRequest();
			}

			var Survey = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (Survey == null)
			{
				return NotFound();
			}

			await _context.Update(id, SurveyIn, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			var Survey = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (Survey == null)
			{
				return NotFound();
			}

			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}