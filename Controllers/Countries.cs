using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

	[ApiController]
	[Route("api/[controller]")]
	public class CountriesController : ControllerBase
	{
		private readonly ICountriesService _context;
		

		public CountriesController(ICountriesService context)
		{
			_context = context;
			
		}

		[HttpGet]
		public async Task<IEnumerable<Country>> GetCountries()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetCountry")]
		public async Task<ActionResult<Country>> GetCountry([FromRoute] string id)
		{
			var country = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
			if (country == null)
			{
				return NotFound();
			}
			return Ok(country);
		}

		[HttpPost]
		public async Task<IActionResult> CreateCountry([FromBody] Country country)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(country, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetCountry", new { Id = country.Id }, country);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> UpdateCountry([FromRoute] string id, [FromBody] Country country)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Update(id, country, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> DeleteCountry([FromRoute] string id)
		{
			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}
