using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ChargesController : ControllerBase
    {
        private readonly IChargesService _context;

        public ChargesController(IChargesService context)
        {
            _context = context;

        }

        [HttpGet]
        public async Task<IEnumerable<Charge>> GetCharges()
        {
            return await _context.Get((UserInfo)base.RouteData.Values["User"]);
        }

        [HttpGet("Balance")]
        public IActionResult GetBalance()
        {
            return Ok(_context.GetBalances());
        }

        [HttpGet("Due")]
        public IActionResult GetDue()
        {
            return Ok(_context.GetDue());
        }

        [HttpGet("TotalDue")]
        public IActionResult GetTotalDue()
        {
            return Ok(_context.GetDueCharges());
        }

        [HttpGet("TotalCurrent")]
        public IActionResult GetTotalCurrent()
        {
            return Ok(_context.GetCurrentCharges());
        }

        [HttpGet("Students")]
        public async Task<IEnumerable<StudentBalance>> GetStudentBalances()
        {
            return await _context.GetStudentBalances();
        }


        [HttpGet("{id:length(24)}", Name = "GetCharge")]
        public async Task<ActionResult<Charge>> GetCharge([FromRoute] string id)
        {
            var charge = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
            if (charge == null)
            {
                return NotFound();
            }
            return Ok(charge);
        }

        [HttpPost]
        public async Task<IActionResult> CreateCharge([FromBody] Charge charge)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _context.Create(charge, (UserInfo)base.RouteData.Values["User"]);
            return CreatedAtRoute("GetCharge", new { Id = charge.Id }, charge);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> UpdateCharge([FromRoute] string id, [FromBody] Charge charge)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _context.Update(id, charge, (UserInfo)base.RouteData.Values["User"]);
            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> DeleteCharge([FromRoute] string id)
        {
            await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
            return NoContent();
        }
    }
}