using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class QuizAssignationsController : ControllerBase
	{
		private readonly IQuizAssignationsService _context;

		public QuizAssignationsController(IQuizAssignationsService context)
		{
			_context = context;
		}

		[HttpGet]
		public async Task<IEnumerable<QuizAssignation>> Get()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetQuizAssignation")]
		public async Task<ActionResult<QuizAssignation>> Get([FromRoute] string id)
		{
			var QuizAssignation = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (QuizAssignation == null)
			{
				return NotFound();
			}

			return Ok(QuizAssignation);
		}

		[HttpGet("{id:length(24)}/Results")]
		public async Task<IEnumerable<QuizResult>> GetResults([FromRoute] string id)
		{
			return await _context.GetResults(id);
		}

		[HttpPost]
		public async Task<ActionResult<QuizAssignation>> Create([FromBody] QuizAssignation QuizAssignation)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(QuizAssignation, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetQuizAssignation", new { id = QuizAssignation.Id }, QuizAssignation);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Update([FromBody] QuizAssignation QuizAssignationIn, [FromRoute] string id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (QuizAssignationIn.Id != id)
			{
				return BadRequest();
			}

			var QuizAssignation = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (QuizAssignation == null)
			{
				return NotFound();
			}

			await _context.Update(id, QuizAssignationIn, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			var QuizAssignation = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (QuizAssignation == null)
			{
				return NotFound();
			}

			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}