using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

	[ApiController]
	[Route("api/[controller]")]
	public class ExamsController : ControllerBase
	{
		private readonly IExamsService _context;


		public ExamsController(IExamsService context)
		{
			_context = context;

		}

		[HttpGet]
		public async Task<IEnumerable<ExamReference>> GetExams()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetExam")]
		public async Task<ActionResult<Exam>> GetExam([FromRoute] string id)
		{
			var quiz = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (quiz == null)
			{
				return NotFound();
			}

			return Ok(quiz);
		}

		[HttpPost]
		public async Task<ActionResult<Exam>> Create([FromBody] Exam quiz)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(quiz, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetExam", new { id = quiz.Id }, quiz);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Update([FromBody] Exam quiz, [FromRoute] string id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (quiz.Id != id)
			{
				return BadRequest();
			}

			if ((await _context.Get(id, (UserInfo)base.RouteData.Values["User"])) == null)
			{
				return NotFound();
			}

			await _context.Update(id, quiz, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}

		[HttpGet("{id:length(24)}/Assignations")]
		public async Task<IEnumerable<ExamAssignation>> GetAssignations([FromRoute] string id)
		{
			return await _context.GetAssignations(id);
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			var exam = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (exam == null)
			{
				return NotFound();
			}

			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}