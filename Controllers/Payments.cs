using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class PaymentsController : ControllerBase
	{
		private readonly IPaymentsService _context;
		

		public PaymentsController(IPaymentsService context)
		{
			_context = context;
			
		}

		[HttpGet]
		public async Task<IEnumerable<Payment>> GetPayments()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetPayment")]
		public async Task<ActionResult<Payment>> GetPayment([FromRoute] string id)
		{
			var payment = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
			if (payment == null)
			{
				return NotFound();
			}
			return Ok(payment);
		}

		[HttpGet("Students")]
        public async Task<IEnumerable<StudentBalance>> GetStudentBalances()
        {
            return await _context.GetStudentBalances();
        }

		[HttpGet("SetStudents")]
        public async Task<IActionResult> SetStudentBalances()
        {
            await _context.SetStudentBalances();
			return Ok();

        }

		[HttpPost]
		public async Task<IActionResult> CreatePayment([FromBody] Payment payment)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(payment, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetPayment", new { Id = payment.Id }, payment);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> UpdatePayment([FromRoute] string id, [FromBody] Payment payment)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Update(id, payment, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> DeletePayment([FromRoute] string id)
		{
			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}