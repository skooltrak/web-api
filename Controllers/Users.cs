using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Filter;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _context;
        public UsersController(IUsersService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<UserDetails>> Get()
        {
            return await _context.GetDetails();
        }

        [SkipActionFilter]
        [HttpGet("resetpassword/{email}")]
        public async Task<IActionResult> ResetPassword([FromRoute] string email)
        {
            await _context.ResetPassword(email);
            return NoContent();
        }

        [HttpGet("{id:length(24)}", Name = "GetUser")]
        public async Task<ActionResult<User>> Get([FromRoute] string id)
        {
            var user = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [SkipActionFilter]
        [HttpPost]
        public async Task<ActionResult<User>> Create([FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!(await _context.ValidateUser(user)))
            {
                return new ContentResult
                {
                    Content = "Username or email already exists",
                    StatusCode = 409
                };
            };

            await _context.Create(user, (UserInfo)base.RouteData.Values["User"]);
            return CreatedAtRoute("GetUser", new { id = user.Id }, user);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> Update([FromRoute] string id, [FromBody] User userIn)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userIn.Id != id)
            {
                return BadRequest();
            }

            var user = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

            if (user == null)
            {
                return NotFound();
            }

            await _context.Update(id, userIn, (UserInfo)base.RouteData.Values["User"]);

            return NoContent();
        }

        [HttpGet("Surveys")]
        public async Task<IEnumerable<Survey>> GetSurveys()
        {
            return await _context.GetSurveys((UserInfo)base.RouteData.Values["User"]);
        }

        [HttpPost("{id:length(24)}/ChangeAvatar")]
        public async Task<IActionResult> UpdateAvatar([FromRoute] string id, [FromBody] Avatar avatar)
        {
            await _context.ChangeAvatar(id, avatar.PhotoURL);
            return NoContent();
        }

        [HttpGet("{id:length(24)}/Messages")]
        public async Task<IEnumerable<Message>> GetMessages([FromRoute] string id)
        {
            return await _context.GetMessages(id);
        }

        [HttpPut("{id:length(24)}/UpdateInfo")]
        public async Task<IActionResult> UpdateInfo([FromRoute] string id, [FromBody] User userIn)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

            if (user == null)
            {
                return NotFound();
            }

            await _context.UpdateProfile(userIn, id);

            return NoContent();
        }


        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> Delete([FromRoute] string id)
        {
            var user = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

            if (user == null)
            {
                return NotFound();
            }

            await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
            return NoContent();
        }

        public class Avatar
        {
            public string PhotoURL { get; set; }
        }

    }
}