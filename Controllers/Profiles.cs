using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProfilesController : ControllerBase
    {
        private readonly IProfilesService _context;

        public ProfilesController(IProfilesService context)
        {
            _context = context;
        }

        [HttpGet("{id:length(24)}")]
        public async Task<IActionResult> GetPerson([FromRoute] string id)
        {
            var value = await _context.GetPerson(id);
            return Ok(value);
        }
    }
}