using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MessagesController : ControllerBase
    {
        private readonly IMessagesService _context;

        public MessagesController(IMessagesService context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<ActionResult<Message>> CreateMessage([FromBody] Message message)
        {
            await _context.Create(message, (UserInfo)base.RouteData.Values["User"]);
            return Ok(message);
        }

        [HttpGet]
        public async Task<IEnumerable<MessageInbox>> GetMessages([FromQuery] int pageSize, [FromQuery] string lastId)
        {
            return await _context.GetMessages((UserInfo)base.RouteData.Values["User"], lastId);
        }

        [HttpGet("{id:length(24)}")]
        public async Task<ActionResult<Message>> GetMessage([FromRoute] string id)
        {
            var message = await _context.GetMessage(id);
            if (message == null)
            {
                return NotFound();
            }

            return Ok(message);
        }

        [HttpGet("SetReference")]
        public IActionResult SetReference()
        {
            _context.MigrateToReferece();
            return NoContent();
        }

        /* [HttpGet("SetUserInfo")]
		public IActionResult SetUserInfo()
		{
			_context.SetUserInfo();
			return NoContent();
		} */

        [HttpGet("{id:length(24)}/Details")]
        public async Task<ActionResult<Message>> GetMessageDetails([FromRoute] string id)
        {
            var message = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
            if (message == null)
            {
                return NotFound();
            }

            return Ok(message);
        }

        [HttpGet("Sent")]
        public async Task<IEnumerable<Message>> GetSent()
        {
            return await _context.GetSent((UserInfo)base.RouteData.Values["User"]);
        }

        [HttpGet("Contacts")]
        public async Task<IEnumerable<UserInfo>> GetContacts()
        {
            return await _context.GetContacts((UserInfo)base.RouteData.Values["User"]);
        }

        [HttpGet("Receivers")]
        public async Task<IEnumerable<Receiver>> GetReceivers()
        {
            return await _context.GetReceivers((UserInfo)base.RouteData.Values["User"]);
        }

        [HttpPost("Trash")]
        public async Task<IActionResult> SentToTrash([FromBody] IdObject messageObject)
        {
            var message = await _context.GetMessage(messageObject.Id);

            if (message == null)
            {
                return NotFound();
            }

            await _context.SentToTrash(message.Id);
            return Ok();
        }

        [HttpPost("Recover")]
        public async Task<IActionResult> Recover([FromBody] IdObject messageObject)
        {
            var message = await _context.GetMessage(messageObject.Id);

            if (message == null)
            {
                return NotFound();
            }

            await _context.RecoverFromTrash(message.Id);
            return Ok();
        }

        [HttpGet("Drafts")]
        public async Task<IEnumerable<Message>> GetDrafts()
        {
            return await _context.GetDrafts((UserInfo)base.RouteData.Values["User"]);
        }

        [HttpGet("Trash")]
        public async Task<IEnumerable<MessageInbox>> GetTrash()
        {
            return await _context.GetTrash((UserInfo)base.RouteData.Values["User"]);
        }

        [HttpPost("{id:length(24)}/Read")]
        public async Task<IActionResult> SetRead([FromRoute] string id)
        {
            await _context.SetRead((UserInfo)base.RouteData.Values["User"], id);
            return NoContent();
        }

        [HttpGet("Counter")]
        public async Task<long> GetCounter()
        {
            return await _context.Unread((UserInfo)base.RouteData.Values["User"]);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> RemoveMessage([FromRoute] string id)
        {
            await _context.RemoveInbox(id);
            return NoContent();
        }

        [HttpDelete("{id:length(24)}/Sent")]
        public async Task<IActionResult> RemoveSentMessage([FromRoute] string id)
        {
            await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
            return NoContent();
        }

        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> EditMessage([FromBody] Message message, [FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _context.Update(id, message, (UserInfo)base.RouteData.Values["User"]);
            return NoContent();
        }
    }
}