using System.Collections.Generic;
using System.Threading.Tasks;
using skooltrak_api.Models;
using skooltrak_api.Services;
using Microsoft.AspNetCore.Mvc;

namespace skooltrak_api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AssignmentsController : ControllerBase
	{
		private readonly IAssignmentsService _context;


		public AssignmentsController(IAssignmentsService context)
		{
			_context = context;

		}

		[HttpGet]
		public async Task<IEnumerable<Assignment>> Get()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetAssignment")]
		public async Task<ActionResult<Assignment>> Get([FromRoute] string id)
		{
			var assignment = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (assignment == null)
			{
				return NotFound();
			}

			return Ok(assignment);
		}

		[HttpPost]
		public async Task<ActionResult<Assignment>> Post([FromBody] Assignment assignment)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			try
			{
				await _context.Create(assignment, (UserInfo)base.RouteData.Values["User"]);
			}
			catch (System.Exception)
			{

				return Unauthorized();
			}
			
			return CreatedAtRoute("GetAssignment", new { id = assignment.Id }, assignment);
		}

		[HttpGet("{id:length(24)}/Comments")]
		public async Task<IEnumerable<Comment>> GetComments([FromRoute] string id)
		{
			return await _context.GetComments(id);
		}

		[HttpGet("{id:length(24)}/Forum")]
		public async Task<Forum> Forum([FromRoute] string id)
		{
			return await _context.GetForum(id);
		}

		[HttpGet("{id:length(24)}/Documents")]
		public async Task<IEnumerable<Document>> GetDocuments([FromRoute] string id)
		{
			return await _context.GetDocuments(id, (UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}/Videos")]
		public async Task<IEnumerable<Video>> GetVideos([FromRoute] string id)
		{
			return await _context.GetVideos(id);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Put([FromRoute] string id, [FromBody] Assignment assignmentIn)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (assignmentIn.Id != id)
			{
				return BadRequest();
			}

			var assignment = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (assignment == null)
			{
				return NotFound();
			}

			await _context.Update(id, assignmentIn, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			var assignment = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (assignment == null)
			{
				return NotFound();
			}

			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}
	}
}