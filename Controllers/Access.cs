using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class AccessController : ControllerBase
	{
		private readonly IAccessService _context;

		public AccessController(IAccessService context)
		{
			_context = context;
		}

		[HttpGet]
		public IEnumerable<AdminAccess> GetAccess()
		{
			return _context.GetAccess();
		}
	}
}