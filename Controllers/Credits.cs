using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CreditsController : ControllerBase
    {
        private readonly ICreditsService _context;

        public CreditsController(ICreditsService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetCredits([FromQuery] string documentId, [FromQuery] string type)
        {
            var credits = await _context.GetCredits(documentId, type);
            var grouped = credits.GroupBy(x => x.Subject)
            .Select(x => new
            {
                Subject = x.Key,
                Grades = x.Select(item => new { item.Year, item.Level, item.Period, item.Grade })
            });
            return Ok(grouped);

        }

        [HttpGet("Summary")]
        public async Task<IActionResult> GetCreditsByYear([FromQuery] string documentId,  [FromQuery] string type)
        {
            var credits = await _context.GetCredits(documentId, type);
            var grouped = credits.GroupBy(x => new { x.Year, x.Level }).Select(x => new
            {
                Year = x.Key,
                Grades = x.Average(x => x.Grade)
            });
            return Ok(grouped);

        }
    }
}