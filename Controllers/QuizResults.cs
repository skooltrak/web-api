using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class QuizResultsController : ControllerBase
	{
		private readonly IQuizResultsService _context;

		public QuizResultsController(IQuizResultsService context)
		{
			_context = context;
		}

		[HttpGet("{id:length(24)}")]
		public async Task<ActionResult<QuizResult>> GetResult([FromRoute] string id)
		{
			var result = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
			if (result == null)
			{
				return NotFound();
			}

			return Ok(result);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Update([FromBody] QuizResult result, [FromRoute] string id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if ((await _context.Get(id, (UserInfo)base.RouteData.Values["User"])) == null)
			{
				return NotFound();
			}

			await _context.Update(id, result, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}
	}
}