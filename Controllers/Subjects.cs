using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class SubjectsController : ControllerBase
	{
		private readonly ISubjectsService _context;
		

		public SubjectsController(ISubjectsService context)
		{
			_context = context;
			
		}

		[HttpGet]
		public async Task<IEnumerable<Subject>> Get()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetSubject")]
		public async Task<ActionResult<Subject>> Get([FromRoute] string id)
		{
			var subject = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (subject == null)
			{
				return NotFound();
			}

			return Ok(subject);
		}

		[HttpPost]
		public async Task<ActionResult<Subject>> Create([FromBody] Subject subject)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(subject, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetSubject", new { id = subject.Id }, subject);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Update([FromBody] Subject subjectIn, [FromRoute] string id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (subjectIn.Id != id)
			{
				return BadRequest();
			}

			var subject = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (subject == null)
			{
				return NotFound();
			}

			await _context.Update(id, subjectIn, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			var subject = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (subject == null)
			{
				return NotFound();
			}

			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}