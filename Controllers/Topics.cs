using System.Collections.Generic;
using System.Threading.Tasks;
using skooltrak_api.Models;
using skooltrak_api.Services;
using Microsoft.AspNetCore.Mvc;

namespace skooltrak_api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class TopicsController : ControllerBase
	{
		private readonly ITopicsService _context;
		

		public TopicsController(ITopicsService context)
		{
			_context = context;
			
		}

		[HttpGet]
		public async Task<IEnumerable<Topic>> Get()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetTopic")]
		public async Task<ActionResult<Topic>> Get([FromRoute] string id)
		{
			var topic = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (topic == null)
			{
				return NotFound();
			}

			return Ok(topic);
		}

		[HttpPost]
		public async Task<ActionResult<Topic>> Post([FromBody] Topic topic)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(topic, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetTopic", new { id = topic.Id }, topic);

		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Put([FromRoute] string id, [FromBody] Topic topicIn)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (topicIn.Id != id)
			{
				return BadRequest();
			}

			var topic = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (topic == null)
			{
				return NotFound();
			}

			await _context.Update(id, topicIn, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			var topic = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (topic == null)
			{
				return NotFound();
			}

			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}
	}
}