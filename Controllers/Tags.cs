using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

	[ApiController]
	[Route("api/[controller]")]
	public class TagsController : ControllerBase
	{
		private readonly ITopicTagsService _context;
		

		public TagsController(ITopicTagsService context)
		{
			_context = context;
			
		}

		[HttpGet]
		public async Task<IEnumerable<TopicTag>> Get()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetTag")]
		public async Task<IActionResult> Get([FromRoute] string id)
		{
			var tag = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
			if (tag == null)
			{
				return NotFound();
			}

			return Ok(tag);
		}

		[HttpPost]
		public async Task<IActionResult> Create([FromBody] TopicTag tag)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(tag, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetTag", new { id = tag.Id }, tag);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Edit([FromRoute] string id, [FromBody] TopicTag tag)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Update(id, tag, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}

		[HttpPost("search")]
		public async Task<IEnumerable<TopicTag>> SearchTags([FromBody] SearchText search)
		{
			return await _context.SearchByName(search.Text);
		}

		public class SearchText
		{
			public string Text { get; set; }
		}

	}
}