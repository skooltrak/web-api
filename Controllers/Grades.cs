using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class GradesController : ControllerBase
    {
        private readonly IGradesService _context;


        public GradesController(IGradesService context)
        {
            _context = context;

        }

        [HttpGet]
        public async Task<IEnumerable<Grade>> Get()
        {
            return await _context.Get((UserInfo)base.RouteData.Values["User"]);
        }

        [HttpGet("{id:length(24)}", Name = "GetGrade")]
        public async Task<ActionResult<Grade>> Get([FromRoute] string id)
        {
            var grade = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

            if (grade == null)
            {
                return NotFound();
            }

            return Ok(grade);
        }

        [HttpGet("SetGroup")]
        public async Task<IActionResult> SetGroups()
        {
            try
            {
                await _context.SetGroup();
            }
            catch (System.Exception e)
            {
                return BadRequest(e);
            }

            return Ok();
        }

        [HttpGet("MoveGroup")]
        public async Task<IActionResult> MoveGroup()
        {
            await _context.MoveToGroup();
            return NoContent();
        }

        [HttpGet("{id:length(24)}/SetGrades")]
        public async Task<IActionResult> SetPreviousGrades([FromRoute] string id)
        {
            await _context.SetPreviousGrades(id);
            return Ok();
        }

        [HttpGet("{id:length(24)}/Publish")]
        public async Task<IActionResult> PublishGrades([FromRoute] string id)
        {
            await _context.PublishGrades(id, (UserInfo)base.RouteData.Values["User"]);
            return Ok();
        }

        [HttpPost]
        public async Task<ActionResult<Grade>> Create([FromBody] Grade grade)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _context.Create(grade, (UserInfo)base.RouteData.Values["User"]);
            return CreatedAtRoute("GetGrade", new { id = grade.Id }, grade);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> Update([FromBody] Grade gradeIn, [FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (gradeIn.Id != id)
            {
                return BadRequest();
            }

            var grade = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

            if (grade == null)
            {
                return NotFound();
            }

            await _context.Update(id, gradeIn, (UserInfo)base.RouteData.Values["User"]);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> Delete([FromRoute] string id)
        {
            var grade = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

            if (grade == null)
            {
                return NotFound();
            }

            await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
            return NoContent();
        }
    }
}