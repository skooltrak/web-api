using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class SkillsController : ControllerBase
    {
        private readonly ISkillsService _context;
        public SkillsController(ISkillsService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<Skill>> Get()
        {
            return await _context.Get((UserInfo)base.RouteData.Values["User"]);
        }

        [HttpGet("InitSkills")]
        public IActionResult Init()
        {
            _context.InitSkills();
            return NoContent();
        }

        [HttpGet("{id:length(24)}", Name = "GetSkill")]
        public async Task<ActionResult<Skill>> Get([FromRoute] string id)
        {
            var skill = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

            if (skill == null)
            {
                return NotFound();
            }

            return Ok(skill);
        }

        [HttpPost("SetSkill")]
        public async Task<IActionResult> SetSkill([FromBody] SkillValue item)
        {
            try
            {
                await _context.SetValue(item);
            }
            catch (System.Exception e)
            {
                return BadRequest(e);
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Skill>> Create([FromBody] Skill skill)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _context.Create(skill, (UserInfo)base.RouteData.Values["User"]);
            return CreatedAtRoute("GetSkill", new { id = skill.Id }, skill);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> Update([FromRoute] string id, [FromBody] Skill skillIn)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (skillIn.Id != id)
            {
                return BadRequest();
            }

            var skill = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
            if (skill == null)
            {
                return NotFound();
            }

            await _context.Update(id, skill, (UserInfo)base.RouteData.Values["User"]);
            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> Delete([FromRoute] string id)
        {
            var skill = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
            if (skill == null)
            {
                return NotFound();
            }
            await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
            return NoContent();
        }
    }
}