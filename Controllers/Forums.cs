using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using skooltrak_api.Models;
using skooltrak_api.Services;
using skooltrak_api.Shared;

namespace skooltrak_api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class ForumsController : ControllerBase
	{
		private readonly IForumsService _context;
		private readonly IHubContext<ForumHub> _hub;
		
		public ForumsController(IForumsService context, IHubContext<ForumHub> hub)
		{
			_context = context;
			_hub = hub;
		}

		[HttpGet]
		public async Task<IEnumerable<Forum>> GetCountries()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetForum")]
		public async Task<ActionResult<Forum>> GetForum([FromRoute] string id)
		{
			var forum = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
			if (forum == null)
			{
				return NotFound();
			}
			return Ok(forum);
		}

		[HttpGet("{id:length(24)}/Posts")]
		public async Task<IEnumerable<ForumPost>> GetPosts([FromRoute] string id)
		{
			return await _context.GetPosts(id);
		}

		[HttpGet("{id:length(24)}/Documents")]
		public async Task<IEnumerable<Document>> GetDocuments([FromRoute] string id)
		{
			return await _context.GetDocuments(id);
		}

		[HttpPost("{id:length(24)}")]
		public async Task<ActionResult<ForumPost>> AddPost([FromRoute] string id, [FromBody] ForumPost post)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.AddPost(id, post, (UserInfo)base.RouteData.Values["User"]);
			await _hub.Clients.All.SendAsync(id, post);
			return Ok(post);
		}

		[HttpDelete("{forumId:length(24)}/Delete/{id:length(24)}")]
		public async Task<IActionResult> DeletePost([FromRoute] string forumId, [FromRoute] string id)
		{
			await _context.RemovePost(forumId, id);
			return NoContent();
		}

		[HttpPost]
		public async Task<IActionResult> CreateForum([FromBody] Forum forum)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(forum, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetForum", new { Id = forum.Id }, forum);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> UpdateForum([FromRoute] string id, [FromBody] Forum forum)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Update(id, forum, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> DeleteForum([FromRoute] string id)
		{
			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}