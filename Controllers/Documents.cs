using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DocumentsController : ControllerBase
    {
        private readonly IDocumentsService _context;
        private readonly IFilesService _files;

        public DocumentsController(IDocumentsService context, IFilesService files)
        {
            _context = context;
            _files = files;
        }

        [HttpGet]
        public async Task<IEnumerable<Document>> GetDocuments()
        {
            return await _context.Get((UserInfo)base.RouteData.Values["User"]);
        }

        [HttpGet("{id:length(24)}", Name = "GetDocument")]
        public async Task<ActionResult<Document>> Get([FromRoute] string id)
        {
            var document = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

            if (document == null)
            {
                return NotFound();
            }

            return Ok(document);
        }

        [HttpPost]
        public async Task<ActionResult<Document>> Create([FromBody] Document document)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _context.Create(document, (UserInfo)base.RouteData.Values["User"]);
            return CreatedAtRoute("GetDocument", new { id = document.Id }, document);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> Delete([FromRoute] string id)
        {
            var user = (UserInfo)base.RouteData.Values["User"];
            var document = await _context.Get(id, user);

            if (document == null)
            {
                return NotFound();
            }
            if (user.Id != document.CreateUser.Id && user.Role.Code != (int)RoleEnum.Administrator)
            {
                return Unauthorized();
            }
            else
            {
                try
                {
                    await _files.DeleteFile(document.File.Id);
                }
                catch (System.Exception)
                {

                }
                finally
                {
                    await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
                }
                return NoContent();
            }
        }
    }
}