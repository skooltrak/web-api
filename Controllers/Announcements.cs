using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AnnouncementsController : ControllerBase
	{
		private readonly IAnnouncementsService _context;
		

		public AnnouncementsController(IAnnouncementsService context)
		{
			_context = context;
			
		}

		[HttpGet]
		public async Task<IEnumerable<Announcement>> Get()
		{
			return await _context.GetActive();
		}

		[HttpGet("All")]
		public async Task<IEnumerable<Announcement>> GetAll()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetAnnouncement")]
		public async Task<ActionResult<Announcement>> Get([FromRoute] string id)
		{
			var announcement = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (announcement == null)
			{
				return NotFound();
			}

			return Ok(announcement);
		}

		[HttpPost]
		public async Task<ActionResult<Announcement>> Create([FromBody] Announcement announcement)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(announcement, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetAnnouncement", new { id = announcement.Id }, announcement);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Update([FromBody] Announcement announcementIn, [FromRoute] string id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (announcementIn.Id != id)
			{
				return BadRequest();
			}

			var announcement = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (announcement == null)
			{
				return NotFound();
			}

			await _context.Update(id, announcementIn, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			var announcement = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (announcement == null)
			{
				return NotFound();
			}

			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}


	}
}