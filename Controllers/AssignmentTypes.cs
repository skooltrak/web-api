using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class AssignmentTypesController : ControllerBase
	{
		private readonly IAssignmentTypesService _context;
		
		public AssignmentTypesController(IAssignmentTypesService context)
		{
			_context = context;
			
		}

		[HttpGet]
		public async Task<IEnumerable<AssignmentType>> Get()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetType")]
		public async Task<IActionResult> Get([FromRoute] string id)
		{
			var type = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
			if (type == null)
			{
				return NotFound();
			}

			return Ok(type);
		}

		[HttpPost]
		public async Task<IActionResult> Create([FromBody] AssignmentType type)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(type, (UserInfo)base.RouteData.Values["User"]);

			return CreatedAtRoute("GetType", new { id = type.Id }, type);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Update([FromRoute] string id, [FromBody] AssignmentType type)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Update(id, type, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}