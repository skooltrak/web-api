using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

	[ApiController]
	[Route("api/[controller]")]
	public class VideosController : ControllerBase
	{
		private readonly IVideosService _context;

		public VideosController(IVideosService context)
		{
			_context = context;
		}


		[HttpGet]
		public async Task<IEnumerable<Video>> GetVideos()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetVideo")]
		public async Task<ActionResult<Video>> GetVideo([FromRoute] string id)
		{
			var video = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
			if (video == null)
			{
				return NotFound();
			}
			return Ok(video);
		}


		[HttpPut("{id:length(24)}/Publish")]
		public async Task<IActionResult> TogglePublish([FromRoute] string id)
		{
			await _context.TogglePublished(id);
			return NoContent();
		}

		[HttpPost]
		public async Task<IActionResult> CreateVideo([FromBody] Video Video)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(Video, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetVideo", new { Id = Video.Id }, Video);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> UpdateVideo([FromRoute] string id, [FromBody] Video video)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			await _context.Update(id, video, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}


		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> DeleteVideo([FromRoute] string id)
		{
			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}