using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class StudyPlansController : ControllerBase
    {
        private readonly IStudyPlansService _context;


        public StudyPlansController(IStudyPlansService context)
        {
            _context = context;

        }

        [HttpGet]
        public async Task<IEnumerable<StudyPlan>> Get()
        {
            return await _context.Get((UserInfo)base.RouteData.Values["User"]);
        }

        [HttpGet("{id:length(24)}", Name = "GetStudyPlan")]
        public async Task<ActionResult<StudyPlan>> Get([FromRoute] string id)
        {
            var plan = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

            if (plan == null)
            {
                return NotFound();
            }

            return Ok(plan);
        }

        [HttpGet("{id:length(24)}/Evaluations")]
        public async Task<IEnumerable<EvaluationArea>> GetEvaluationAreas([FromRoute] string id)
        {
            return await _context.GetEvaluations(id);
        }

        [HttpPost("{id:length(24)}/Evaluations")]
        public async Task<ActionResult<EvaluationArea>> CreateEvaluationArea([FromRoute] string id, [FromBody] EvaluationArea area)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await _context.AddArea(id, area);
            return Ok(area);
        }

        [HttpPut("{id:length(24)}/Evaluations")]
        public async Task<IActionResult> UpdateEvaluationArea([FromRoute] string id, [FromBody] EvaluationArea area)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await _context.UpdateArea(id, area);
            return Ok();
        }

        [HttpDelete("{id:length(24)}/Evaluations")]
        public async Task<IActionResult> DeleteEvaluationArea([FromRoute] string id)
        {
            await _context.DeleteArea(id);
            return Ok();
        }

        [HttpPost]
        public async Task<ActionResult<StudyPlan>> Create([FromBody] StudyPlan plan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _context.Create(plan, (UserInfo)base.RouteData.Values["User"]);
            return CreatedAtRoute("GetStudyPlan", new { id = plan.Id }, plan);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> Update([FromBody] StudyPlan planIn, [FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (planIn.Id != id)
            {
                return BadRequest();
            }

            var plan = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

            if (plan == null)
            {
                return NotFound();
            }

            await _context.Update(id, planIn, (UserInfo)base.RouteData.Values["User"]);

            return NoContent();
        }

        [HttpGet("{id:length(24)}/courses")]
        public async Task<IEnumerable<Course>> GetCourses([FromRoute] string id)
        {
            return await _context.GetCourses(id);
        }

        [HttpGet("{id:length(24)}/groups")]
        public async Task<IEnumerable<ClassGroup>> GetGroups([FromRoute] string id)
        {
            return await _context.GetGroups(id);
        }

        [HttpPost("copy")]
        public async Task<IActionResult> CopyCourses([FromBody] string[] ids)
        {
            if (!await ExistPlan(ids[0]) || !await ExistPlan(ids[1]))
            {
                return NotFound();
            }

            await _context.CopyCourses(ids[0], ids[1]);
            return Ok();
        }

        [HttpPost("copy_charges")]
        public async Task<IActionResult> CopyCharges([FromBody] string[] ids)
        {
            if (!await ExistPlan(ids[0]) || !await ExistPlan(ids[1]))
            {
                return NotFound();
            }

            await _context.CopyCharges(ids[0], ids[1]);
            return Ok();
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> Delete([FromRoute] string id)
        {
            var plan = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

            if (plan == null)
            {
                return NotFound();
            }

            await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
            return NoContent();
        }


        private async Task<bool> ExistPlan(string id)
        {
            var item = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
            return item != null;
        }
    }
}