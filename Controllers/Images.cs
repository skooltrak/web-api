using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Filter;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

	[ApiController]
	[Route("api/[controller]")]
	public class ImagesController : ControllerBase
	{
		private readonly IFilesService _context;
		private readonly IHttpContextAccessor _accessor;

		public ImagesController(IFilesService context, IHttpContextAccessor accessor)
		{
			_context = context;
			_accessor = accessor;
		}

		[SkipActionFilter]
		[HttpPost]
		[RequestSizeLimit(100000000)]
		public async Task<IActionResult> Upload()
		{
			var file = HttpContext.Request.Form.Files[0];
			var request = _accessor.HttpContext.Request;
			var result = await _context.Upload(file);
			var value = request.Scheme + "://" + request.Host.Value + "/api/Images/" + result.Id;
			var response = new { path = value };
			return Ok(response);
		}

		[SkipActionFilter]
		[HttpGet("{id}")]
		public async Task<IActionResult> GetImage(string id)
		{
			var result = await _context.GetFile(id);
			if (result.Info == null)
			{
				return NotFound();
			}
			return File(result.File, result.Info.Metadata["type"].ToString(), result.Info.Filename);
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> DeleteImage(string id)
		{
			await _context.DeleteFile(id);
			return Ok();
		}

	}
}