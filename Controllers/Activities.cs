using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

	[ApiController]
	[Route("api/[controller]")]
	public class ActivitiesController : ControllerBase
	{
		private readonly IActivitiesService _context;

		public ActivitiesController(IActivitiesService context)
		{
			_context = context;
		}


		[HttpGet("load")]
		public async Task<IActionResult> LoadActivities()
		{
			await _context.ReloadActivity();
			return NoContent();
		}
	}
}