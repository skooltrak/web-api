using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class TeachersController : ControllerBase
	{
		private readonly ITeachersService _context;


		public TeachersController(ITeachersService context)
		{
			_context = context;

		}

		[HttpGet]
		public async Task<IEnumerable<Teacher>> Get()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetTeacher")]
		public async Task<ActionResult<Teacher>> Get([FromRoute] string id)
		{
			var teacher = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (teacher == null)
			{
				return NotFound();
			}

			return Ok(teacher);
		}

		[HttpGet("{id:length(24)}/Courses")]
		public async Task<IEnumerable<Course>> GetCourses([FromRoute] string id)
		{
			return await _context.GetCourses(id);
		}

		[HttpGet("{id:length(24)}/Quizes")]
		public async Task<IEnumerable<Quiz>> GetQuizes([FromRoute] string id)
		{
			return await _context.GetQuizes(id);
		}

		[HttpGet("{id:length(24)}/Exams")]
		public async Task<IEnumerable<ExamReference>> GetExams([FromRoute] string id)
		{
			return await _context.GetExams(id);
		}

		[HttpGet("{id:length(24)}/ExamAssignations")]
		public async Task<IEnumerable<ExamAssignation>> GetExamAssignations([FromRoute] string id)
		{
			return await _context.GetExamAssignations(id);
		}

		[HttpGet("{id:length(24)}/Videos")]
		public async Task<IEnumerable<Video>> GetVideos([FromRoute] string id)
		{
			return await _context.GetVideos(id);
		}

		[HttpGet("{id:length(24)}/Documents")]
		public async Task<IEnumerable<Document>> GetDocuments([FromRoute] string id)
		{
			return await _context.GetDocuments(id);
		}

		[HttpGet("{id:length(24)}/QuizAssignations")]
		public async Task<IEnumerable<QuizAssignation>> GetQuizAssignations([FromRoute] string id)
		{
			return await _context.GetQuizAssignations(id);
		}

		[HttpGet("{id:length(24)}/Assignments")]
		public async Task<IEnumerable<Assignment>> GetAssignments([FromRoute] string id)
		{
			return await _context.GetAssignments(id);
		}

		[HttpGet("{id:length(24)}/Activity")]
		public async Task<IEnumerable<Activity>> GetActivities([FromRoute] string id)
		{
			return await _context.GetLastActivities(id);
		}

		[HttpGet("{id:length(24)}/AllActivity")]
		public async Task<IEnumerable<Activity>> GetAllActivities([FromRoute] string id)
		{
			return await _context.GetActivities(id);
		}

		[HttpGet("{id:length(24)}/Schedule")]
		public async Task<IEnumerable<TeacherDayClass>> GetClassDays([FromRoute] string id)
		{
			return await _context.GetTeacherDays(id);
		}

		[HttpGet("{id:length(24)}/Groups")]
		public async Task<IEnumerable<ClassGroup>> GetGroups([FromRoute] string id)
		{
			return await _context.GetGroups(id);
		}

		[HttpGet("{id:length(24)}/Forums")]
		public async Task<IEnumerable<Forum>> GetForums([FromRoute] string id)
		{
			return await _context.GetForums(id);
		}

		[HttpPost]
		public async Task<ActionResult<Teacher>> Post([FromBody] Teacher teacher)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(teacher, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetTeacher", new { id = teacher.Id }, teacher);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Put([FromRoute] string id, [FromBody] Teacher teacherIn)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (teacherIn.Id != id)
			{
				return BadRequest();
			}

			var teacher = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (teacher == null)
			{
				return NotFound();
			}

			await _context.Update(id, teacherIn, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			var teacher = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (teacher == null)
			{
				return NotFound();
			}

			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}