using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Filter;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

  [ApiController]
  [Route("api/[controller]")]
  public class ClassGroupsController : ControllerBase
  {
    private readonly IClassGroupsService _context;

    public ClassGroupsController(IClassGroupsService context)
    {
      _context = context;
    }

    [HttpGet]
    public async Task<IEnumerable<ClassGroup>> Get()
    {
      return await _context.Get((UserInfo)base.RouteData.Values["User"]);
    }

    [HttpGet("{id:length(24)}", Name = "GetClassGroup")]
    public async Task<ActionResult<ClassGroup>> Get([FromRoute] string id)
    {
      var group = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

      if (group == null)
      {
        return NotFound();
      }

      return Ok(group);
    }

    [HttpPost]
    public async Task<ActionResult<ClassGroup>> Create([FromBody] ClassGroup group)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      await _context.Create(group, (UserInfo)base.RouteData.Values["User"]);
      return CreatedAtRoute("GetClassGroup", new { id = group.Id }, group);
    }

    [HttpGet("{id:length(24)}/students")]
    public async Task<IEnumerable<Student>> GetStudents([FromRoute] string id)
    {
      return await _context.GetStudents(id);
    }

    [HttpGet("{id:length(24)}/courses")]
    public async Task<IEnumerable<Course>> GetCourses([FromRoute] string id)
    {
      return await _context.GetCourses(id);
    }

    [HttpGet("{id:length(24)}/Rankings")]
    public async Task<IEnumerable<Ranking>> GetRankings([FromRoute] string id, [FromQuery] string periodId)
    {
      return await _context.GetRankings(id, periodId);
    }

    [AdminRequired]
    [HttpGet("{id:length(24)}/Users")]
    public async Task<IEnumerable<User>> GetUsers([FromRoute] string id)
    {
      return await _context.GetUsers(id);
    }

    [HttpGet("{id:length(24)}/attendance")]
    public async Task<IEnumerable<Attendance>> GetAttendances([FromRoute] string id)
    {
      return await _context.GetAttendances(id);
    }

    [HttpGet("setClasses")]
    public async Task<IActionResult> SetTeacherClass()
    {
      await _context.SetTeacherClasses();
      return Ok();
    }

    [HttpGet("{id:length(24)}/Rooms")]
    public async Task<IEnumerable<Classroom>> GetClassrooms([FromRoute] string id)
    {
      return await _context.GetClassrooms(id);
    }

    [HttpPut("{id:length(24)}")]
    public async Task<IActionResult> Update([FromBody] ClassGroup groupIn, [FromRoute] string id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (groupIn.Id != id)
      {
        return BadRequest();
      }

      var group = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

      if (group == null)
      {
        return NotFound();
      }

      await _context.Update(id, groupIn, (UserInfo)base.RouteData.Values["User"]);

      return NoContent();
    }

    [HttpDelete("{id:length(24)}")]
    public async Task<IActionResult> Delete([FromRoute] string id)
    {
      var group = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

      if (group == null)
      {
        return NotFound();
      }

      await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
      return NoContent();
    }
  }
}