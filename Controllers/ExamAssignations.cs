using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

	[ApiController]
	[Route("api/[controller]")]
	public class ExamAssignationsController : ControllerBase
	{
		private readonly IExamsAssignationsService _context;


		public ExamAssignationsController(IExamsAssignationsService context)
		{
			_context = context;

		}

		[HttpGet]
		public async Task<IEnumerable<ExamAssignation>> GetExamAssignations()
		{
			return await _context.Get((UserInfo)base.RouteData.Values["User"]);
		}

		[HttpGet("{id:length(24)}", Name = "GetExamAssignation")]
		public async Task<ActionResult<Exam>> GetExam([FromRoute] string id)
		{
			var quiz = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (quiz == null)
			{
				return NotFound();
			}

			return Ok(quiz);
		}

		[HttpPost]
		public async Task<ActionResult<Exam>> Create([FromBody] ExamAssignation exam)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			await _context.Create(exam, (UserInfo)base.RouteData.Values["User"]);
			return CreatedAtRoute("GetExamAssignation", new { id = exam.Id }, exam);
		}

		[HttpGet("{id:length(24)}/Results")]
		public async Task<IEnumerable<ExamResult>> GetResults([FromRoute] string id)
		{
			return await _context.GetResults(id);
		}

		[HttpPut("{id:length(24)}")]
		public async Task<IActionResult> Update([FromBody] ExamAssignation exam, [FromRoute] string id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (exam.Id != id)
			{
				return BadRequest();
			}

			if ((await _context.Get(id, (UserInfo)base.RouteData.Values["User"])) == null)
			{
				return NotFound();
			}

			await _context.Update(id, exam, (UserInfo)base.RouteData.Values["User"]);

			return NoContent();
		}

		[HttpDelete("{id:length(24)}")]
		public async Task<IActionResult> Delete([FromRoute] string id)
		{
			var exam = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

			if (exam == null)
			{
				return NotFound();
			}

			await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
			return NoContent();
		}
	}
}