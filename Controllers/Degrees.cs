using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class DegreesController : ControllerBase
    {
        private readonly IDegreesService _context;
        

        public DegreesController(IDegreesService context)
        {
            _context = context;
            
        }

        [HttpGet]
        public async Task<IEnumerable<Degree>> GetDegrees()
        {
            return await _context.Get((UserInfo)base.RouteData.Values["User"]);
        }

        [HttpGet("{id:length(24)}", Name = "GetDegree")]
        public async Task<ActionResult<Degree>> GetDegree([FromRoute] string id)
        {
            var degree = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);
            if (degree == null)
            {
                return NotFound();
            }
            return Ok(degree);
        }

        [HttpPost]
        public async Task<IActionResult> CreateDegree([FromBody] Degree degree)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _context.Create(degree, (UserInfo)base.RouteData.Values["User"]);
            return CreatedAtRoute("GetDegree", new { Id = degree.Id }, degree);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> UpdateDegree([FromRoute] string id, [FromBody] Degree degree)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _context.Update(id, degree, (UserInfo)base.RouteData.Values["User"]);
            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> DeleteDegree([FromRoute] string id)
        {
            await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
            return NoContent();
        }
    }
}