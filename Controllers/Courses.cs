using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Filter;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{

  [ApiController]
  [Route("api/[controller]")]
  public class CoursesController : ControllerBase
  {
    private readonly ICoursesService _context;

    public CoursesController(ICoursesService context)
    {
      _context = context;

    }

    [HttpGet]
    public async Task<IEnumerable<Course>> Get()
    {
      return await _context.Get((UserInfo)base.RouteData.Values["User"]);
    }

    [HttpGet("{id:length(24)}", Name = "GetCourse")]
    public async Task<ActionResult<Course>> Get([FromRoute] string id)
    {
      var course = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

      if (course == null)
      {
        return NotFound();
      }

      return Ok(course);
    }

    [HttpPost]
    public async Task<ActionResult<Course>> Create([FromBody] Course course)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      await _context.Create(course, (UserInfo)base.RouteData.Values["User"]);
      return CreatedAtRoute("GetCourse", new { id = course.Id }, course);
    }

    [HttpPut("{id:length(24)}")]
    public async Task<IActionResult> Update([FromBody] Course courseIn, [FromRoute] string id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (courseIn.Id != id)
      {
        return BadRequest();
      }

      var course = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

      if (course == null)
      {
        return NotFound();
      }

      await _context.Update(id, courseIn, (UserInfo)base.RouteData.Values["User"]);

      return NoContent();
    }

    [HttpGet("{id:length(24)}/Topics")]
    public async Task<IEnumerable<Topic>> GetTopics([FromRoute] string id)
    {
      return await _context.GetTopics(id);
    }

    [HttpGet("{id:length(24)}/Documents")]
    public async Task<IEnumerable<Document>> GetDocuments([FromRoute] string id)
    {
      return await _context.GetDocuments(id, (UserInfo)base.RouteData.Values["User"]);
    }

    [HttpGet("{id:length(24)}/Groups")]
    public async Task<IEnumerable<ClassGroup>> GetGroups([FromRoute] string id)
    {
      return await _context.GetGroups(id);
    }

    [HttpGet("{id:length(24)}/Students")]
    public async Task<IEnumerable<Student>> GetStudents([FromRoute] string id)
    {
      return await _context.GetStudents(id);
    }

    [HttpGet("{id:length(24)}/Videos")]
    public async Task<IEnumerable<Video>> GetVideos([FromRoute] string id)
    {
      return await _context.GetVideos(id, (UserInfo)base.RouteData.Values["User"]);
    }

    [HttpGet("{id:length(24)}/Attendance")]
    public async Task<IEnumerable<Attendance>> GetAttendances([FromRoute] string id, [FromQuery] int year)
    {
      return await _context.GetAttendances(id);
    }

    [HttpGet("{id:length(24)}/Forums")]
    public async Task<IEnumerable<Forum>> GetForums([FromRoute] string id)
    {
      return await _context.GetForums(id);
    }

    [HttpGet("{id:length(24)}/Assignments")]
    public async Task<IEnumerable<Assignment>> GetCourses([FromRoute] string id)
    {
      return await _context.GetAssignments(id);
    }

    [HttpGet("{id:length(24)}/Contents")]
    public async Task<IEnumerable<CourseContent>> GetContents([FromRoute] string id)
    {
      return await _context.GetContent(id);
    }

    [HttpPost("InitPeriod")]
    [AdminRequired]
    public async Task<IActionResult> InitPeriod([FromBody] Period period)
    {
      await _context.SetPeriod(period);
      return NoContent();
    }

    [HttpPost("{id:length(24)}/Assignments")]
    public async Task<IEnumerable<Assignment>> GetAssignments([FromRoute] string id, [FromBody] Interval range)
    {
      return await _context.GetAssignments(id, range.StartDate, range.EndDate);
    }


    [HttpGet("{id:length(24)}/Grades")]
    public async Task<IEnumerable<Grade>> GetGrades([FromRoute] string id)
    {
      return await _context.GetGrades(id);
    }

    [HttpGet("{id:length(24)}/Grades/{periodId:length(24)}")]
    public async Task<IEnumerable<Grade>> GetClosedGrades([FromRoute] string id, [FromRoute] string periodId)
    {
      return await _context.GetClosedGrades(id, periodId);
    }

    [HttpGet("{id:length(24)}/Score/{studentId:length(24)}")]
    public double GetStudentScore([FromRoute] string id, [FromRoute] string studentId)
    {
      return _context.GetScore(id, studentId);
    }

    [HttpGet("{id:length(24)}/PeriodScore/{studentId:length(24)}")]
    public double GetPeriodScore([FromRoute] string id, [FromRoute] string studentId, [FromQuery] string periodId)
    {
      return _context.GetPeriodScore(id, studentId, periodId);
    }

    [HttpGet("SetGradePeriod")]
    public async Task<IActionResult> SetGradesPeriod()
    {
      await _context.SetGradePeriod();
      return NoContent();
    }

    [HttpPost("ClosePeriod")]
    [TeacherRequired]
    public async Task<Course> ClosePeriod([FromBody] Course course)
    {
      return await _context.ClosePeriod(course);
    }

    [HttpGet("{id:length(24)}/StudentsGrades")]
    public async Task<IEnumerable<StudentGrade>> GetStudentGrades([FromRoute] string id, [FromQuery] string periodId)
    {
      return await _context.GetStudentGrades(id, periodId);
    }

    [HttpGet("{id:length(24)}/Messages")]
    public async Task<IEnumerable<CourseMessage>> GetMessages([FromRoute] string id)
    {
      return await _context.GetMessages(id);
    }

    [HttpGet("SetPlan")]
    public async Task<IActionResult> SetPlan()
    {
      await _context.SetPlan();
      return Ok();
    }

    [HttpPut("{id:length(24)}/ChangeColor")]
    public async Task<IActionResult> ChangeColor([FromRoute] string id, [FromBody] Reference color)
    {
      await _context.ChangeColor(id, color.Id);
      return Ok();
    }

    [HttpPut("{id:length(24)}/OpenPeriod")]
    public async Task<IActionResult> OpenPeriod([FromRoute] string id, [FromBody] Period period)
    {
      await _context.OpenPeriod(id, period);
      return Ok();
    }

    [HttpPut("{id:length(24)}/ChangeIcon")]
    public async Task<IActionResult> ChangeIcon([FromRoute] string id, [FromBody] Reference icon)
    {
      await _context.ChangeIcon(id, icon.Id);
      return Ok();
    }

    [HttpDelete("{id:length(24)}")]
    public async Task<IActionResult> Delete([FromRoute] string id)
    {
      var course = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

      if (course == null)
      {
        return NotFound();
      }

      await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
      return NoContent();
    }
  }
}