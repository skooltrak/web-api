using System.Net.Http;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Filter;
using skooltrak_api.Models;
using skooltrak_api.Services;
using MongoDB.Driver;
using System.Net;
using System.Net.Http.Headers;
using MongoDB.Bson;
using System.IO;
using System.Linq;

namespace skooltrak_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class EditorMediaController : ControllerBase
    {
        private readonly IFilesService _context;
        private readonly IHttpContextAccessor _accessor;
        private readonly IDbContext _global;

        public EditorMediaController(IFilesService context, IHttpContextAccessor accessor, IDbContext global)
        {
            _context = context;
            _accessor = accessor;
            _global = global;
        }

        [SkipActionFilter]
        [HttpPost("Images")]
        [RequestSizeLimit(100000000)]
        public async Task<IActionResult> UploadImage()
        {
            var file = HttpContext.Request.Form.Files[0];
            var result = await _context.Upload(file);
            var value = "http://" + _accessor.HttpContext.Request.Host.Value + "/api/EditorMedia/Images/" + result.Id;
            var response = new { success = 1, file = new { url = value } };
            return Ok(response);
        }

        [SkipActionFilter]
        [HttpGet("Images/{id}")]
        public async Task<IActionResult> GetImage(string id)
        {
            var result = await _context.GetFile(id);
            if (result.Info == null)
            {
                return NotFound();
            }
            return File(result.File, result.Info.Metadata["type"].ToString(), result.Info.Filename);
        }

        [SkipActionFilter]
        [HttpPost("Files")]
        [RequestSizeLimit(100000000)]
        public async Task<IActionResult> UploadFile()
        {
            var file = HttpContext.Request.Form.Files[0];
            var result = await _context.Upload(file);
            var value = "https://" + _accessor.HttpContext.Request.Host.Value + "/api/EditorMediaController/Files/" + result.Id;
            var response = new { success = 1, file = new { url = value } };
            return Ok(response);
        }

        [SkipActionFilter]
        [HttpGet("Files/{id}")]
        public async Task<HttpResponseMessage> GetFile(string id)
        {
            HttpResponseMessage result = null;

            if (Request.GetTypedHeaders().Range == null ||
            Request.GetTypedHeaders().Range.Ranges.Count == 0 ||
            Request.GetTypedHeaders().Range.Ranges.FirstOrDefault().From.Value == 0)
            {
                Stream sourceStream = await _global.bucket.OpenDownloadStreamAsync(ObjectId.Parse(id));
                BufferedStream bs = new BufferedStream(sourceStream);
                var file = await _context.GetFile(id);
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StreamContent(bs);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue(file.Info.Metadata["type"].ToString());
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = file.Info.Filename };
            }

            // var filter = Builders<GridFSFileInfo>.Filter.Eq("_id", ObjectId.Parse(id));
            // var info = await (await _global.bucket.FindAsync(filter)).FirstOrDefaultAsync();
            // if (info == null)
            // {
            //     result = new HttpResponseMessage(HttpStatusCode.NotFound);
            // }

            return result;

        }

        [SkipActionFilter]
        [HttpGet("LinkInfo")]
        public IActionResult GetSiteMetadata([FromQuery] string url)
        {
            var webGet = new HtmlWeb();
            var document = webGet.Load(url);
            var metaTags = document.DocumentNode.SelectNodes("//meta");
            MetadataInfo metaInfo = new MetadataInfo();
            if (metaTags != null)
            {
                int matchCount = 0;
                foreach (var tag in metaTags)
                {
                    var tagName = tag.Attributes["name"];
                    var tagContent = tag.Attributes["content"];
                    var tagProperty = tag.Attributes["property"];

                    if (tagName != null && tagContent != null)
                    {
                        switch (tagName.Value.ToLower())
                        {
                            case "title":
                                metaInfo.Title = tagContent.Value;
                                matchCount++;
                                break;
                            case "description":
                                metaInfo.Description = tagContent.Value;
                                matchCount++;
                                break;
                            case "twitter:image":
                                metaInfo.Image.Url = string.IsNullOrEmpty(metaInfo.Image.Url) ? tagContent.Value : metaInfo.Image.Url;
                                matchCount++;
                                break;
                        }
                    }
                    else if (tagProperty != null && tagContent != null)
                    {
                        switch (tagProperty.Value.ToLower())
                        {
                            case "og:title":
                                metaInfo.Title = string.IsNullOrEmpty(metaInfo.Title) ? tagContent.Value : metaInfo.Title;
                                matchCount++;
                                break;
                            case "og:description":
                                metaInfo.Description = string.IsNullOrEmpty(metaInfo.Description) ? tagContent.Value : metaInfo.Description;
                                matchCount++;
                                break;
                            case "og:image":
                                metaInfo.Image.Url = string.IsNullOrEmpty(metaInfo.Image.Url) ? tagContent.Value : metaInfo.Image.Url;
                                matchCount++;
                                break;
                        }
                    }
                }
            }
            var response = new { success = 1, meta = metaInfo };
            return Ok(response);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> DeleteImage(string id)
        {
            await _context.DeleteFile(id);
            return Ok();
        }
    }
}
