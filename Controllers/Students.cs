using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skooltrak_api.Filter;
using skooltrak_api.Models;
using skooltrak_api.Services;

namespace skooltrak_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StudentsController : ControllerBase
    {
        private readonly IStudentsService _context;

        public StudentsController(IStudentsService context)
        {
            _context = context;

        }

        [HttpGet]
        public async Task<IEnumerable<StudentSummary>> Get()
        {
            return await _context.Get((UserInfo)base.RouteData.Values["User"]);
        }

        [HttpGet("Inactive")]
        public async Task<IEnumerable<Student>> GetInactive()
        {
            return await _context.GetInactiveStudents();
        }

        [HttpGet("All")]
        [AdminRequired]
        public async Task<IEnumerable<Student>> Getall()
        {
            return await _context.GetAllStudents((UserInfo)base.RouteData.Values["User"]);
        }

        [HttpGet("List")]
        public async Task<IEnumerable<Student>> GetStudents()
        {
            return await _context.GetStudents();
        }

        [HttpGet("{id:length(24)}/Courses")]
        public async Task<IEnumerable<Course>> GetCourses([FromRoute] string id, [FromQuery] string periodId)
        {
            return await _context.GetCourses(id, periodId);
        }

        [HttpGet("{id:length(24)}/ParentCourses")]
        public IEnumerable<ParentSubject> GetParentSubjects([FromRoute] string id, [FromQuery] string periodId)
        {
            return _context.GetParentSubject(id, periodId);
        }

        [HttpGet("{id:length(24)}/Forums")]
        public async Task<IEnumerable<Forum>> GetForums([FromRoute] string id)
        {
            return await _context.GetForums(id);
        }

        [HttpGet("{id:length(24)}/Performance")]
        public async Task<IEnumerable<StudentPerformance>> GetPerformances([FromRoute] string id)
        {
            return await _context.GetPerformance(id);
        }

        [HttpGet("{id:length(24)}/Schedule")]
        public async Task<IEnumerable<ClassDay>> GetSchedule([FromRoute] string id)
        {
            return await _context.GetSchedule(id);
        }

        [HttpGet("Temporary")]
        public async Task<IEnumerable<Student>> GetTemporary()
        {
            return await _context.GetTemporaryStudents();
        }

        [HttpGet("SetPlans")]
        public async Task<IActionResult> SetPlans()
        {
            await _context.SetPlans();
            return NoContent();
        }

        [HttpGet("count")]
        public async Task<long> GetCount()
        {
            return await _context.GetCount();
        }


        [HttpGet("{id:length(24)}/Payments")]
        public async Task<IEnumerable<Payment>> GetPayments([FromRoute] string id)
        {
            return await _context.GetPayments(id);
        }

        [HttpGet("{id:length(24)}/Activity")]
        public async Task<IEnumerable<Activity>> GetActivities([FromRoute] string id)
        {
            return await _context.GetActivities(id, (UserInfo)base.RouteData.Values["User"]);
        }

        [HttpGet("ByDocument/{id}")]
        public async Task<ActionResult<Student>> GetStudentByDocument([FromRoute] string id)
        {
            var student = await _context.GetStudentByDocument(id);

            if (student == null)
            {
                return NotFound();
            }
            return Ok(student);
        }

        [HttpGet("ResetGroups")]
        public async Task<IActionResult> ResetGroups()
        {
            await _context.FixStudentsGroup();
            return Ok();
        }

        [HttpGet("ByPlan")]
        public IActionResult GetByPlan()
        {
            return Ok(_context.GetCountByPlan());
        }

        [HttpGet("{id:length(24)}/Charges")]
        public async Task<IEnumerable<Charge>> GetCharges([FromRoute] string id)
        {
            return await _context.GetCharges(id);
        }

        [HttpGet("{id:length(24)}/Grades")]
        public async Task<IEnumerable<StudentGrade>> GetGrades([FromRoute] string id)
        {
            return await _context.GetGrades(id);
        }

        [HttpGet("{id:length(24)}/Grades/{courseId:length(24)}/Course")]
        public async Task<IEnumerable<StudentGrade>> GetCourseGrades([FromRoute] string id, [FromRoute] string courseId, [FromQuery] string periodId)
        {
            return await _context.GetGradesByCourse(id, courseId, periodId);
        }

        [HttpGet("{id:length(24)}/Attendance")]
        public async Task<IEnumerable<StudentAttendance>> GetAttendances([FromRoute] string id)
        {
            return await _context.GetAttendances(id);
        }

        [HttpGet("{id:length(24)}/Score")]
        public async Task<IActionResult> GetPeriodScore([FromRoute] string id, [FromQuery] string periodId)
        {
            if (periodId != null && periodId != "undefined")
            {
                return Ok(_context.GetPeriodScore(id, periodId));
            }

            return Ok(await _context.GetCurrentScore(id));
        }

        [HttpGet("{id:length(24)}/Assignments")]
        public async Task<IEnumerable<Assignment>> GetAssignments([FromRoute] string id, [FromQuery] string dateFrom, [FromQuery] string DateTo)
        {
            return await _context.GetAssignments(id, dateFrom, DateTo);
        }

        [HttpGet("{id:length(24)}/Quizes")]
        public async Task<IEnumerable<QuizResult>> GetQuizes([FromRoute] string id)
        {
            return await _context.GetQuizes(id);
        }

        [HttpGet("{id:length(24)}/ExamResults")]
        public async Task<IEnumerable<ExamResult>> GetResults([FromRoute] string id)
        {
            return await _context.GetExamResults(id);
        }

        [HttpGet("{id:length(24)}/Exams")]
        public async Task<IEnumerable<ExamResult>> GetExams([FromRoute] string id)
        {
            return await _context.GetExams(id);
        }

        [HttpGet("{id:length(24)}/QuizResults")]
        public async Task<IEnumerable<QuizResult>> GetQuizResults([FromRoute] string id)
        {
            return await _context.GetResults(id);
        }

        [HttpGet("{id:length(24)}/Teachers")]
        public async Task<IEnumerable<Teacher>> GetTeachers([FromRoute] string id)
        {
            return await _context.GetTeachers(id);
        }

        [HttpGet("{id:length(24)}/Set_Charges")]
        public IActionResult SetCharges([FromRoute] string id)
        {
            _context.SetCharges(id);
            return NoContent();
        }

        [HttpGet("SetCharges")]
        public async Task<IActionResult> SetAllCharges()
        {
            await _context.SetAllCharges();
            return NoContent();
        }

        [HttpPost("{id:length(24)}/GradeSummary")]
        public async Task<GradeSummary> GetGradeSummary([FromRoute] string id, [FromBody] Period period)
        {
            return await _context.GetGradeSummary(id, period);
        }

        [HttpGet("{id:length(24)}/Archives/{year:length(4)}")]
        public async Task<IEnumerable<ArchiveGrade>> GetArchiveGrades([FromRoute] string id, [FromRoute] string year)
        {
            return await _context.GetArchiveGrades(id, Int32.Parse(year));
        }

        [HttpGet("{id:length(24)}/Archives")]
        public async Task<IEnumerable<int>> GetArchiveYears([FromRoute] string id)
        {
            return await _context.GetArchiveYears(id);
        }

        [HttpGet("{id:length(24)}/Skills")]
        public async Task<List<StudentSkill>> GetSkills([FromRoute] string id)
        {
            return await _context.GetSkills(id);
        }

        [HttpPost("{id:length(24)}/Skills")]
        public async Task<IActionResult> SetSkill([FromRoute] string id, [FromBody] StudentSkill skill)
        {
            await _context.SetSkill(id, skill);
            return NoContent();
        }

        [HttpGet("{id:length(24)}/Evaluations")]
        public async Task<List<PreScholarEvaluation>> GetEvaluations([FromRoute] string id)
        {
            return await _context.GetEvaluations(id);
        }

        [HttpPost("{id:length(24)}/Evaluations")]
        public async Task<IActionResult> SetEvaluation([FromRoute] string id, [FromBody] PreScholarEvaluation evaluation)
        {
            await _context.SetEvaluation(id, evaluation);
            return NoContent();
        }

        [HttpGet("{id:length(24)}", Name = "GetStudent")]
        public async Task<ActionResult<Student>> Get([FromRoute] string id)
        {
            var student = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

            if (student == null)
            {
                return NotFound();
            }

            return Ok(student);
        }

        [SkipActionFilter]
        [HttpPost("ByDocument")]
        public async Task<ActionResult<Student>> GetStudentByDocument([FromBody] DocumentValidation document)
        {
            var student = await _context.GetStudentByDocument(document.DocumentId);
            if (student == null)
            {
                return NotFound();
            }

            return Ok(student);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Student student)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!String.IsNullOrEmpty(student.DocumentId) && !(await _context.ValidDocumentId(student.DocumentId)))
            {
                return new ContentResult
                {
                    Content = "Duplicate document",
                    StatusCode = 409
                };
            }

            await _context.Create(student, (UserInfo)base.RouteData.Values["User"]);
            return CreatedAtRoute("GetStudent", new { id = student.Id }, student);
        }

        [HttpPost("CreateUsers")]
        public async Task<IActionResult> CreateUsers([FromBody] object value)
        {
            await _context.CreateUsers();
            return Ok();
        }

        [HttpPost("ValidateDocument")]
        public async Task<bool> ValidateDocumentId([FromBody] DocumentValidation validation)
        {
            if (validation.StudentId == null || validation.StudentId.Length == 0)
            {
                return await _context.ValidDocumentId(validation.DocumentId);
            }

            return await _context.ValidDocumentId(validation.StudentId, validation.DocumentId);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> Update([FromRoute] string id, [FromBody] Student studentIn)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (studentIn.Id != id)
            {
                return BadRequest();
            }

            var student = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

            if (student == null)
            {
                return NotFound();
            }

            await _context.Update(id, studentIn, (UserInfo)base.RouteData.Values["User"]);

            return NoContent();
        }

        [HttpGet("{id:length(24)}/Documents")]
        public async Task<IEnumerable<Document>> GetDocuments([FromRoute] string id)
        {
            return await _context.GetDocuments(id);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> Delete([FromRoute] string id)
        {
            var student = await _context.Get(id, (UserInfo)base.RouteData.Values["User"]);

            if (student == null)
            {
                return NotFound();
            }

            await _context.Remove(id, (UserInfo)base.RouteData.Values["User"]);
            return NoContent();
        }

        [HttpGet("SetSection")]
        public async Task<IActionResult> SetSection()
        {
            await _context.SetSection();
            return NoContent();
        }

        public class DocumentValidation
        {
            public string StudentId { get; set; }
            public string DocumentId { get; set; }
        }
    }
}